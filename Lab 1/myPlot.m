function void = myPlot(input_data)

    plot(input_data(:,1), input_data(:,2))

end