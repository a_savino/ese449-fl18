figure;

p1 = subplot(2,1,2);
plot(Potentiometer(:,1), Potentiometer(:,2));
title('Potentiometer');
xlabel('time');
ylabel('degrees');

% p2 = subplot(4,1,3);
% plot(Tachometer(:,1), Tachometer(:,2));
% title('Tachometer');
% xlabel('time');
% ylabel('revolutions / sec.');

p3 = subplot(2,1,1);
plot(Voltage(:,1), Voltage(:,2));
title('Input Voltage');
xlabel('time');
ylabel('Voltage');

% p4 = subplot(4,1,4);
% plot(encoder_reading(:,1), encoder_reading(:,2));
% title('Encoder Reading');
% xlabel('time');
% ylabel('Position');


