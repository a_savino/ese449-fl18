% Load saved figures
p1=hgload('adjusting_amp_0.fig');
p2=hgload('adjusting_frequency.fig');
p3=hgload('adjusting_amp_2.fig');

% Prepare subplots
figure
h(1)=subplot(1,3,1);
h(2)=subplot(1,3,2);
h(3)=subplot(1,3,3);
title(h(2), 'Adjusting Amplitude: 0, 1, 2 (1hz. frequency)');

% Paste figures on the subplots
copyobj(allchild(get(p1,'CurrentAxes')),h(1));
ylim(h(1), [0 .5])
xlabel(h(1), 'A=0')

copyobj(allchild(get(p2,'CurrentAxes')),h(2));
ylim([0 2.5])
xlabel(h(2), 'A=1')

copyobj(allchild(get(p3,'CurrentAxes')),h(3));
ylim([0 2.5])
xlabel(h(3), 'A=2')


