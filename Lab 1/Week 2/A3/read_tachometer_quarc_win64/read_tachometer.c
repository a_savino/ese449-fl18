/*
 * read_tachometer.c
 *
 * Code generation for model "read_tachometer".
 *
 * Model version              : 1.3
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Sep 10 16:23:16 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "read_tachometer.h"
#include "read_tachometer_private.h"
#include "read_tachometer_dt.h"

/* Block signals (auto storage) */
BlockIO_read_tachometer read_tachometer_B;

/* Block states (auto storage) */
D_Work_read_tachometer read_tachometer_DWork;

/* Real-time model */
RT_MODEL_read_tachometer read_tachometer_M_;
RT_MODEL_read_tachometer *const read_tachometer_M = &read_tachometer_M_;

/* Model step function */
void read_tachometer_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_HILReadAnalogTimebase_o2;

  /* S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

  /* S-Function Block: read_tachometer/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_read_analog
      (read_tachometer_DWork.HILReadAnalogTimebase_Task, 1,
       &read_tachometer_DWork.HILReadAnalogTimebase_Buffer[0]);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
    }

    rtb_HILReadAnalogTimebase_o1 =
      read_tachometer_DWork.HILReadAnalogTimebase_Buffer[0];
    rtb_HILReadAnalogTimebase_o2 =
      read_tachometer_DWork.HILReadAnalogTimebase_Buffer[1];
  }

  /* Gain: '<Root>/Potentiometer Categoration ' */
  read_tachometer_B.PotentiometerCategoration =
    read_tachometer_P.PotentiometerCategoration_Gain *
    rtb_HILReadAnalogTimebase_o1;

  /* Gain: '<Root>/Tachometer Calibration ' */
  read_tachometer_B.TachometerCalibration =
    read_tachometer_P.TachometerCalibration_Gain * rtb_HILReadAnalogTimebase_o2;

  /* Gain: '<S1>/Slider Gain' incorporates:
   *  SignalGenerator: '<Root>/Signal Generator'
   */
  read_tachometer_B.SliderGain = sin(6.2831853071795862 *
    read_tachometer_M->Timing.t[0] * read_tachometer_P.SignalGenerator_Frequency)
    * read_tachometer_P.SignalGenerator_Amplitude *
    read_tachometer_P.SliderGain_Gain;

  /* Gain: '<Root>/Motor Cali brator' */
  read_tachometer_B.MotorCalibrator = read_tachometer_P.MotorCalibrator_Gain *
    read_tachometer_B.SliderGain;

  /* S-Function (hil_write_block): '<Root>/HIL Write' */

  /* S-Function Block: read_tachometer/HIL Write (hil_write_block) */
  {
    t_error result;
    result = hil_write(read_tachometer_DWork.HILInitialize_Card,
                       &read_tachometer_P.HILWrite_AnalogChannels, 1U,
                       NULL, 0U,
                       NULL, 0U,
                       NULL, 0U,
                       &read_tachometer_B.MotorCalibrator,
                       NULL,
                       NULL,
                       NULL
                       );
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
    }
  }

  /* External mode */
  rtExtModeUploadCheckTrigger(2);

  {                                    /* Sample time: [0.0s, 0.0s] */
    rtExtModeUpload(0, read_tachometer_M->Timing.t[0]);
  }

  {                                    /* Sample time: [0.002s, 0.0s] */
    rtExtModeUpload(1, (((read_tachometer_M->Timing.clockTick1+
                          read_tachometer_M->Timing.clockTickH1* 4294967296.0)) *
                        0.002));
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.0s, 0.0s] */
    if ((rtmGetTFinal(read_tachometer_M)!=-1) &&
        !((rtmGetTFinal(read_tachometer_M)-read_tachometer_M->Timing.t[0]) >
          read_tachometer_M->Timing.t[0] * (DBL_EPSILON))) {
      rtmSetErrorStatus(read_tachometer_M, "Simulation finished");
    }

    if (rtmGetStopRequested(read_tachometer_M)) {
      rtmSetErrorStatus(read_tachometer_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++read_tachometer_M->Timing.clockTick0)) {
    ++read_tachometer_M->Timing.clockTickH0;
  }

  read_tachometer_M->Timing.t[0] = read_tachometer_M->Timing.clockTick0 *
    read_tachometer_M->Timing.stepSize0 + read_tachometer_M->Timing.clockTickH0 *
    read_tachometer_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.002s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The resolution of this integer timer is 0.002, which is the step size
     * of the task. Size of "clockTick1" ensures timer will not overflow during the
     * application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    read_tachometer_M->Timing.clockTick1++;
    if (!read_tachometer_M->Timing.clockTick1) {
      read_tachometer_M->Timing.clockTickH1++;
    }
  }
}

/* Model initialize function */
void read_tachometer_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)read_tachometer_M, 0,
                sizeof(RT_MODEL_read_tachometer));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&read_tachometer_M->solverInfo,
                          &read_tachometer_M->Timing.simTimeStep);
    rtsiSetTPtr(&read_tachometer_M->solverInfo, &rtmGetTPtr(read_tachometer_M));
    rtsiSetStepSizePtr(&read_tachometer_M->solverInfo,
                       &read_tachometer_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&read_tachometer_M->solverInfo, (&rtmGetErrorStatus
      (read_tachometer_M)));
    rtsiSetRTModelPtr(&read_tachometer_M->solverInfo, read_tachometer_M);
  }

  rtsiSetSimTimeStep(&read_tachometer_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&read_tachometer_M->solverInfo,"FixedStepDiscrete");
  rtmSetTPtr(read_tachometer_M, &read_tachometer_M->Timing.tArray[0]);
  rtmSetTFinal(read_tachometer_M, -1);
  read_tachometer_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  read_tachometer_M->Sizes.checksums[0] = (427895501U);
  read_tachometer_M->Sizes.checksums[1] = (3285041450U);
  read_tachometer_M->Sizes.checksums[2] = (2883323222U);
  read_tachometer_M->Sizes.checksums[3] = (2351377563U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    read_tachometer_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(read_tachometer_M->extModeInfo,
      &read_tachometer_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(read_tachometer_M->extModeInfo,
                        read_tachometer_M->Sizes.checksums);
    rteiSetTPtr(read_tachometer_M->extModeInfo, rtmGetTPtr(read_tachometer_M));
  }

  /* block I/O */
  (void) memset(((void *) &read_tachometer_B), 0,
                sizeof(BlockIO_read_tachometer));

  /* states (dwork) */
  (void) memset((void *)&read_tachometer_DWork, 0,
                sizeof(D_Work_read_tachometer));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    read_tachometer_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: read_tachometer/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &read_tachometer_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options
      (read_tachometer_DWork.HILInitialize_Card,
       "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(read_tachometer_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
      return;
    }

    if ((read_tachometer_P.HILInitialize_AIPStart && !is_switching) ||
        (read_tachometer_P.HILInitialize_AIPEnter && is_switching)) {
      read_tachometer_DWork.HILInitialize_AIMinimums[0] =
        read_tachometer_P.HILInitialize_AILow;
      read_tachometer_DWork.HILInitialize_AIMinimums[1] =
        read_tachometer_P.HILInitialize_AILow;
      read_tachometer_DWork.HILInitialize_AIMaximums[0] =
        read_tachometer_P.HILInitialize_AIHigh;
      read_tachometer_DWork.HILInitialize_AIMaximums[1] =
        read_tachometer_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges
        (read_tachometer_DWork.HILInitialize_Card,
         read_tachometer_P.HILInitialize_AIChannels, 2U,
         &read_tachometer_DWork.HILInitialize_AIMinimums[0],
         &read_tachometer_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
        return;
      }
    }

    if ((read_tachometer_P.HILInitialize_AOPStart && !is_switching) ||
        (read_tachometer_P.HILInitialize_AOPEnter && is_switching)) {
      read_tachometer_DWork.HILInitialize_AOMinimums[0] =
        read_tachometer_P.HILInitialize_AOLow;
      read_tachometer_DWork.HILInitialize_AOMinimums[1] =
        read_tachometer_P.HILInitialize_AOLow;
      read_tachometer_DWork.HILInitialize_AOMaximums[0] =
        read_tachometer_P.HILInitialize_AOHigh;
      read_tachometer_DWork.HILInitialize_AOMaximums[1] =
        read_tachometer_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges
        (read_tachometer_DWork.HILInitialize_Card,
         read_tachometer_P.HILInitialize_AOChannels, 2U,
         &read_tachometer_DWork.HILInitialize_AOMinimums[0],
         &read_tachometer_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
        return;
      }
    }

    if ((read_tachometer_P.HILInitialize_AOStart && !is_switching) ||
        (read_tachometer_P.HILInitialize_AOEnter && is_switching)) {
      read_tachometer_DWork.HILInitialize_AOVoltages[0] =
        read_tachometer_P.HILInitialize_AOInitial;
      read_tachometer_DWork.HILInitialize_AOVoltages[1] =
        read_tachometer_P.HILInitialize_AOInitial;
      result = hil_write_analog(read_tachometer_DWork.HILInitialize_Card,
        read_tachometer_P.HILInitialize_AOChannels, 2U,
        &read_tachometer_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
        return;
      }
    }

    if (read_tachometer_P.HILInitialize_AOReset) {
      read_tachometer_DWork.HILInitialize_AOVoltages[0] =
        read_tachometer_P.HILInitialize_AOWatchdog;
      read_tachometer_DWork.HILInitialize_AOVoltages[1] =
        read_tachometer_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (read_tachometer_DWork.HILInitialize_Card,
         read_tachometer_P.HILInitialize_AOChannels, 2U,
         &read_tachometer_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
        return;
      }
    }

    if ((read_tachometer_P.HILInitialize_EIPStart && !is_switching) ||
        (read_tachometer_P.HILInitialize_EIPEnter && is_switching)) {
      read_tachometer_DWork.HILInitialize_QuadratureModes[0] =
        read_tachometer_P.HILInitialize_EIQuadrature;
      read_tachometer_DWork.HILInitialize_QuadratureModes[1] =
        read_tachometer_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (read_tachometer_DWork.HILInitialize_Card,
         read_tachometer_P.HILInitialize_EIChannels, 2U,
         (t_encoder_quadrature_mode *)
         &read_tachometer_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
        return;
      }
    }

    if ((read_tachometer_P.HILInitialize_EIStart && !is_switching) ||
        (read_tachometer_P.HILInitialize_EIEnter && is_switching)) {
      read_tachometer_DWork.HILInitialize_InitialEICounts[0] =
        read_tachometer_P.HILInitialize_EIInitial;
      read_tachometer_DWork.HILInitialize_InitialEICounts[1] =
        read_tachometer_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(read_tachometer_DWork.HILInitialize_Card,
        read_tachometer_P.HILInitialize_EIChannels, 2U,
        &read_tachometer_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

  /* S-Function Block: read_tachometer/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader
      (read_tachometer_DWork.HILInitialize_Card,
       read_tachometer_P.HILReadAnalogTimebase_SamplesIn,
       read_tachometer_P.HILReadAnalogTimebase_Channels, 2,
       &read_tachometer_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
    }
  }
}

/* Model terminate function */
void read_tachometer_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: read_tachometer/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(read_tachometer_DWork.HILInitialize_Card);
    hil_monitor_stop_all(read_tachometer_DWork.HILInitialize_Card);
    is_switching = false;
    if ((read_tachometer_P.HILInitialize_AOTerminate && !is_switching) ||
        (read_tachometer_P.HILInitialize_AOExit && is_switching)) {
      read_tachometer_DWork.HILInitialize_AOVoltages[0] =
        read_tachometer_P.HILInitialize_AOFinal;
      read_tachometer_DWork.HILInitialize_AOVoltages[1] =
        read_tachometer_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(read_tachometer_DWork.HILInitialize_Card,
        read_tachometer_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &read_tachometer_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_tachometer_M, _rt_error_message);
      }
    }

    hil_task_delete_all(read_tachometer_DWork.HILInitialize_Card);
    hil_monitor_delete_all(read_tachometer_DWork.HILInitialize_Card);
    hil_close(read_tachometer_DWork.HILInitialize_Card);
    read_tachometer_DWork.HILInitialize_Card = NULL;
  }
}
