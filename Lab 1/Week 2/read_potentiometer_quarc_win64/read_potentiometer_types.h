/*
 * read_potentiometer_types.h
 *
 * Code generation for model "read_potentiometer".
 *
 * Model version              : 1.2
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Sep 10 14:47:58 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_read_potentiometer_types_h_
#define RTW_HEADER_read_potentiometer_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_read_potentiometer_ Parameters_read_potentiometer;

/* Forward declaration for rtModel */
typedef struct tag_RTM_read_potentiometer RT_MODEL_read_potentiometer;

#endif                                 /* RTW_HEADER_read_potentiometer_types_h_ */
