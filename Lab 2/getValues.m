function Results = getValues(index, Results, Tach, encoder)
    Results(1, index) = max(abs(Tach(:,2)));
    Results(2, index) = max(abs(encoder(:,2)));
end