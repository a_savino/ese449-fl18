clf
plot(simulation(:,1), simulation(:,[2,3]));
xlabel('Time (sec.)');
ylabel('Velocity (rad/sec)');
title('Simulated Test (Nominal Values)');
