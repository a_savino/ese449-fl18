/*
 * read_encoder_types.h
 *
 * Code generation for model "read_encoder".
 *
 * Model version              : 1.8
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Sep 17 16:27:35 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_read_encoder_types_h_
#define RTW_HEADER_read_encoder_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_read_encoder_ Parameters_read_encoder;

/* Forward declaration for rtModel */
typedef struct tag_RTM_read_encoder RT_MODEL_read_encoder;

#endif                                 /* RTW_HEADER_read_encoder_types_h_ */
