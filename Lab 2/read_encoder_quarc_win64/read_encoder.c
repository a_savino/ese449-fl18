/*
 * read_encoder.c
 *
 * Code generation for model "read_encoder".
 *
 * Model version              : 1.8
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Sep 17 16:27:35 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "read_encoder.h"
#include "read_encoder_private.h"
#include "read_encoder_dt.h"

/* Block signals (auto storage) */
BlockIO_read_encoder read_encoder_B;

/* Continuous states */
ContinuousStates_read_encoder read_encoder_X;

/* Block states (auto storage) */
D_Work_read_encoder read_encoder_DWork;

/* Real-time model */
RT_MODEL_read_encoder read_encoder_M_;
RT_MODEL_read_encoder *const read_encoder_M = &read_encoder_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 3;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  read_encoder_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void read_encoder_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_HILReadEncoder;
  real_T currentTime;
  if (rtmIsMajorTimeStep(read_encoder_M)) {
    /* set solver stop time */
    if (!(read_encoder_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&read_encoder_M->solverInfo,
                            ((read_encoder_M->Timing.clockTickH0 + 1) *
        read_encoder_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&read_encoder_M->solverInfo,
                            ((read_encoder_M->Timing.clockTick0 + 1) *
        read_encoder_M->Timing.stepSize0 + read_encoder_M->Timing.clockTickH0 *
        read_encoder_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(read_encoder_M)) {
    read_encoder_M->Timing.t[0] = rtsiGetT(&read_encoder_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(read_encoder_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

    /* S-Function Block: read_encoder/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog
        (read_encoder_DWork.HILReadAnalogTimebase_Task, 1,
         &read_encoder_DWork.HILReadAnalogTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
      }

      rtb_HILReadAnalogTimebase_o1 =
        read_encoder_DWork.HILReadAnalogTimebase_Buffer[0];
      rtb_HILReadEncoder = read_encoder_DWork.HILReadAnalogTimebase_Buffer[1];
    }

    /* Gain: '<Root>/Potentiometer Categoration ' */
    read_encoder_B.PotentiometerCategoration =
      read_encoder_P.PotentiometerCategoration_Gain *
      rtb_HILReadAnalogTimebase_o1;

    /* Gain: '<Root>/Tachometer Categoration' */
    read_encoder_B.TachometerCategoration =
      read_encoder_P.TachometerCategoration_Gain * rtb_HILReadEncoder;
  }

  /* TransferFcn: '<Root>/Transfer Fcn' */
  read_encoder_B.TransferFcn = read_encoder_P.TransferFcn_C*
    read_encoder_X.TransferFcn_CSTATE;
  if (rtmIsMajorTimeStep(read_encoder_M)) {
  }

  /* Step: '<Root>/Step1' */
  currentTime = read_encoder_M->Timing.t[0];
  if (currentTime < read_encoder_P.Step1_Time) {
    currentTime = read_encoder_P.Step1_Y0;
  } else {
    currentTime = read_encoder_P.Step1_YFinal;
  }

  /* End of Step: '<Root>/Step1' */

  /* Gain: '<Root>/Amplification ' */
  read_encoder_B.Amplification = read_encoder_P.Amplification_Gain * currentTime;

  /* Step: '<Root>/Step' */
  currentTime = read_encoder_M->Timing.t[0];

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant'
   *  SignalGenerator: '<Root>/Signal Generator'
   *  Step: '<Root>/Step'
   */
  if (read_encoder_P.Constant_Value >= read_encoder_P.Switch_Threshold) {
    currentTime = sin(6.2831853071795862 * read_encoder_M->Timing.t[0] *
                      read_encoder_P.SignalGenerator_Frequency) *
      read_encoder_P.SignalGenerator_Amplitude;
  } else if (currentTime < read_encoder_P.Step_Time) {
    /* Step: '<Root>/Step' */
    currentTime = read_encoder_P.Step_Y0;
  } else {
    currentTime = read_encoder_P.Step_YFinal;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Gain: '<S1>/Slider Gain' */
  read_encoder_B.SliderGain = read_encoder_P.SliderGain_Gain * currentTime;

  /* Gain: '<Root>/Motor Calibration' */
  read_encoder_B.MotorCalibration = read_encoder_P.MotorCalibration_Gain *
    read_encoder_B.SliderGain;
  if (rtmIsMajorTimeStep(read_encoder_M)) {
    /* S-Function (hil_write_block): '<Root>/HIL Write' */

    /* S-Function Block: read_encoder/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(read_encoder_DWork.HILInitialize_Card,
                         &read_encoder_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &read_encoder_B.MotorCalibration,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
      }
    }
  }

  /* TransferFcn: '<Root>/Low Pass Filter' */
  read_encoder_B.LowPassFilter = read_encoder_P.LowPassFilter_C[0]*
    read_encoder_X.LowPassFilter_CSTATE[0]
    + read_encoder_P.LowPassFilter_C[1]*read_encoder_X.LowPassFilter_CSTATE[1];
  if (rtmIsMajorTimeStep(read_encoder_M)) {
    /* S-Function (hil_read_encoder_block): '<Root>/HIL Read Encoder' */

    /* S-Function Block: read_encoder/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(read_encoder_DWork.HILInitialize_Card,
        &read_encoder_P.HILReadEncoder_Channels, 1,
        &read_encoder_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder = read_encoder_DWork.HILReadEncoder_Buffer;
      }
    }

    /* Gain: '<Root>/Encoder Categoration ' */
    read_encoder_B.EncoderCategoration = read_encoder_P.EncoderCategoration_Gain
      * rtb_HILReadEncoder;
  }

  if (rtmIsMajorTimeStep(read_encoder_M)) {
    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, read_encoder_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(read_encoder_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((read_encoder_M->Timing.clockTick1+
                            read_encoder_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(read_encoder_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(read_encoder_M)!=-1) &&
          !((rtmGetTFinal(read_encoder_M)-(((read_encoder_M->Timing.clockTick1+
               read_encoder_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((read_encoder_M->Timing.clockTick1+
               read_encoder_M->Timing.clockTickH1* 4294967296.0)) * 0.002) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(read_encoder_M, "Simulation finished");
      }

      if (rtmGetStopRequested(read_encoder_M)) {
        rtmSetErrorStatus(read_encoder_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&read_encoder_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++read_encoder_M->Timing.clockTick0)) {
      ++read_encoder_M->Timing.clockTickH0;
    }

    read_encoder_M->Timing.t[0] = rtsiGetSolverStopTime
      (&read_encoder_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      read_encoder_M->Timing.clockTick1++;
      if (!read_encoder_M->Timing.clockTick1) {
        read_encoder_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void read_encoder_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn' */
  {
    ((StateDerivatives_read_encoder *) read_encoder_M->ModelData.derivs)
      ->TransferFcn_CSTATE = read_encoder_B.Amplification;
    ((StateDerivatives_read_encoder *) read_encoder_M->ModelData.derivs)
      ->TransferFcn_CSTATE += (read_encoder_P.TransferFcn_A)*
      read_encoder_X.TransferFcn_CSTATE;
  }

  /* Derivatives for TransferFcn: '<Root>/Low Pass Filter' */
  {
    ((StateDerivatives_read_encoder *) read_encoder_M->ModelData.derivs)
      ->LowPassFilter_CSTATE[0] = read_encoder_B.EncoderCategoration;
    ((StateDerivatives_read_encoder *) read_encoder_M->ModelData.derivs)
      ->LowPassFilter_CSTATE[0] += (read_encoder_P.LowPassFilter_A[0])*
      read_encoder_X.LowPassFilter_CSTATE[0]
      + (read_encoder_P.LowPassFilter_A[1])*read_encoder_X.LowPassFilter_CSTATE
      [1];
    ((StateDerivatives_read_encoder *) read_encoder_M->ModelData.derivs)
      ->LowPassFilter_CSTATE[1]= read_encoder_X.LowPassFilter_CSTATE[0];
  }
}

/* Model initialize function */
void read_encoder_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)read_encoder_M, 0,
                sizeof(RT_MODEL_read_encoder));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&read_encoder_M->solverInfo,
                          &read_encoder_M->Timing.simTimeStep);
    rtsiSetTPtr(&read_encoder_M->solverInfo, &rtmGetTPtr(read_encoder_M));
    rtsiSetStepSizePtr(&read_encoder_M->solverInfo,
                       &read_encoder_M->Timing.stepSize0);
    rtsiSetdXPtr(&read_encoder_M->solverInfo, &read_encoder_M->ModelData.derivs);
    rtsiSetContStatesPtr(&read_encoder_M->solverInfo,
                         &read_encoder_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&read_encoder_M->solverInfo,
      &read_encoder_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&read_encoder_M->solverInfo, (&rtmGetErrorStatus
      (read_encoder_M)));
    rtsiSetRTModelPtr(&read_encoder_M->solverInfo, read_encoder_M);
  }

  rtsiSetSimTimeStep(&read_encoder_M->solverInfo, MAJOR_TIME_STEP);
  read_encoder_M->ModelData.intgData.f[0] = read_encoder_M->ModelData.odeF[0];
  read_encoder_M->ModelData.contStates = ((real_T *) &read_encoder_X);
  rtsiSetSolverData(&read_encoder_M->solverInfo, (void *)
                    &read_encoder_M->ModelData.intgData);
  rtsiSetSolverName(&read_encoder_M->solverInfo,"ode1");
  rtmSetTPtr(read_encoder_M, &read_encoder_M->Timing.tArray[0]);
  rtmSetTFinal(read_encoder_M, 5.0);
  read_encoder_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  read_encoder_M->Sizes.checksums[0] = (2684193068U);
  read_encoder_M->Sizes.checksums[1] = (2264008082U);
  read_encoder_M->Sizes.checksums[2] = (436472258U);
  read_encoder_M->Sizes.checksums[3] = (1726634755U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    read_encoder_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(read_encoder_M->extModeInfo,
      &read_encoder_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(read_encoder_M->extModeInfo,
                        read_encoder_M->Sizes.checksums);
    rteiSetTPtr(read_encoder_M->extModeInfo, rtmGetTPtr(read_encoder_M));
  }

  /* block I/O */
  (void) memset(((void *) &read_encoder_B), 0,
                sizeof(BlockIO_read_encoder));

  /* states (continuous) */
  {
    (void) memset((void *)&read_encoder_X, 0,
                  sizeof(ContinuousStates_read_encoder));
  }

  /* states (dwork) */
  (void) memset((void *)&read_encoder_DWork, 0,
                sizeof(D_Work_read_encoder));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    read_encoder_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: read_encoder/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &read_encoder_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_encoder_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options(read_encoder_DWork.HILInitialize_Card,
      "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_encoder_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(read_encoder_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_encoder_M, _rt_error_message);
      return;
    }

    if ((read_encoder_P.HILInitialize_AIPStart && !is_switching) ||
        (read_encoder_P.HILInitialize_AIPEnter && is_switching)) {
      read_encoder_DWork.HILInitialize_AIMinimums[0] =
        read_encoder_P.HILInitialize_AILow;
      read_encoder_DWork.HILInitialize_AIMinimums[1] =
        read_encoder_P.HILInitialize_AILow;
      read_encoder_DWork.HILInitialize_AIMaximums[0] =
        read_encoder_P.HILInitialize_AIHigh;
      read_encoder_DWork.HILInitialize_AIMaximums[1] =
        read_encoder_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(read_encoder_DWork.HILInitialize_Card,
        read_encoder_P.HILInitialize_AIChannels, 2U,
        &read_encoder_DWork.HILInitialize_AIMinimums[0],
        &read_encoder_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
        return;
      }
    }

    if ((read_encoder_P.HILInitialize_AOPStart && !is_switching) ||
        (read_encoder_P.HILInitialize_AOPEnter && is_switching)) {
      read_encoder_DWork.HILInitialize_AOMinimums[0] =
        read_encoder_P.HILInitialize_AOLow;
      read_encoder_DWork.HILInitialize_AOMinimums[1] =
        read_encoder_P.HILInitialize_AOLow;
      read_encoder_DWork.HILInitialize_AOMaximums[0] =
        read_encoder_P.HILInitialize_AOHigh;
      read_encoder_DWork.HILInitialize_AOMaximums[1] =
        read_encoder_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges
        (read_encoder_DWork.HILInitialize_Card,
         read_encoder_P.HILInitialize_AOChannels, 2U,
         &read_encoder_DWork.HILInitialize_AOMinimums[0],
         &read_encoder_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
        return;
      }
    }

    if ((read_encoder_P.HILInitialize_AOStart && !is_switching) ||
        (read_encoder_P.HILInitialize_AOEnter && is_switching)) {
      read_encoder_DWork.HILInitialize_AOVoltages[0] =
        read_encoder_P.HILInitialize_AOInitial;
      read_encoder_DWork.HILInitialize_AOVoltages[1] =
        read_encoder_P.HILInitialize_AOInitial;
      result = hil_write_analog(read_encoder_DWork.HILInitialize_Card,
        read_encoder_P.HILInitialize_AOChannels, 2U,
        &read_encoder_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
        return;
      }
    }

    if (read_encoder_P.HILInitialize_AOReset) {
      read_encoder_DWork.HILInitialize_AOVoltages[0] =
        read_encoder_P.HILInitialize_AOWatchdog;
      read_encoder_DWork.HILInitialize_AOVoltages[1] =
        read_encoder_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (read_encoder_DWork.HILInitialize_Card,
         read_encoder_P.HILInitialize_AOChannels, 2U,
         &read_encoder_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
        return;
      }
    }

    if ((read_encoder_P.HILInitialize_EIPStart && !is_switching) ||
        (read_encoder_P.HILInitialize_EIPEnter && is_switching)) {
      read_encoder_DWork.HILInitialize_QuadratureModes[0] =
        read_encoder_P.HILInitialize_EIQuadrature;
      read_encoder_DWork.HILInitialize_QuadratureModes[1] =
        read_encoder_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (read_encoder_DWork.HILInitialize_Card,
         read_encoder_P.HILInitialize_EIChannels, 2U, (t_encoder_quadrature_mode
          *) &read_encoder_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
        return;
      }
    }

    if ((read_encoder_P.HILInitialize_EIStart && !is_switching) ||
        (read_encoder_P.HILInitialize_EIEnter && is_switching)) {
      read_encoder_DWork.HILInitialize_InitialEICounts[0] =
        read_encoder_P.HILInitialize_EIInitial;
      read_encoder_DWork.HILInitialize_InitialEICounts[1] =
        read_encoder_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(read_encoder_DWork.HILInitialize_Card,
        read_encoder_P.HILInitialize_EIChannels, 2U,
        &read_encoder_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

  /* S-Function Block: read_encoder/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader(read_encoder_DWork.HILInitialize_Card,
      read_encoder_P.HILReadAnalogTimebase_SamplesIn,
      read_encoder_P.HILReadAnalogTimebase_Channels, 2,
      &read_encoder_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(read_encoder_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn' */
  read_encoder_X.TransferFcn_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/Low Pass Filter' */
  read_encoder_X.LowPassFilter_CSTATE[0] = 0.0;
  read_encoder_X.LowPassFilter_CSTATE[1] = 0.0;
}

/* Model terminate function */
void read_encoder_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: read_encoder/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(read_encoder_DWork.HILInitialize_Card);
    hil_monitor_stop_all(read_encoder_DWork.HILInitialize_Card);
    is_switching = false;
    if ((read_encoder_P.HILInitialize_AOTerminate && !is_switching) ||
        (read_encoder_P.HILInitialize_AOExit && is_switching)) {
      read_encoder_DWork.HILInitialize_AOVoltages[0] =
        read_encoder_P.HILInitialize_AOFinal;
      read_encoder_DWork.HILInitialize_AOVoltages[1] =
        read_encoder_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(read_encoder_DWork.HILInitialize_Card,
        read_encoder_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &read_encoder_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(read_encoder_M, _rt_error_message);
      }
    }

    hil_task_delete_all(read_encoder_DWork.HILInitialize_Card);
    hil_monitor_delete_all(read_encoder_DWork.HILInitialize_Card);
    hil_close(read_encoder_DWork.HILInitialize_Card);
    read_encoder_DWork.HILInitialize_Card = NULL;
  }
}
