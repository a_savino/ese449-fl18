/*
 * PhaseLead_data.c
 *
 * Code generation for model "PhaseLead".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Nov 05 15:54:21 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "PhaseLead.h"
#include "PhaseLead_private.h"

/* Block parameters (auto storage) */
Parameters_PhaseLead PhaseLead_P = {
  0.0,                                 /* Expression: set_other_outputs_at_start
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: set_other_outputs_at_switch_in
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: set_other_outputs_at_terminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: set_other_outputs_at_switch_out
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  10.0,                                /* Expression: analog_input_maximums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  -10.0,                               /* Expression: analog_input_minimums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  10.0,                                /* Expression: analog_output_maximums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  -10.0,                               /* Expression: analog_output_minimums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: initial_analog_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: final_analog_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: watchdog_analog_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  50.0,                                /* Expression: pwm_frequency
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: initial_pwm_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: final_pwm_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0.0,                                 /* Expression: watchdog_pwm_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Signal Generator1'
                                        */
  0.4,                                 /* Expression: 0.4
                                        * Referenced by: '<Root>/Signal Generator1'
                                        */

  /*  Computed Parameter: LowPassFilter_A
   * Referenced by: '<S1>/Low Pass Filter'
   */
  { -282.74333882308139, -24674.011002723397 },

  /*  Computed Parameter: LowPassFilter_C
   * Referenced by: '<S1>/Low Pass Filter'
   */
  { 24674.011002723397, 0.0 },
  -0.0015,                             /* Expression: -0.0015
                                        * Referenced by: '<S1>/Encoder Calibration (deg//count)'
                                        */
  0.997,                               /* Expression: 0.997
                                        * Referenced by: '<S1>/Tachometer Calibration (krpm//V)'
                                        */
  35.2,                                /* Expression: 352/10
                                        * Referenced by: '<S1>/Potentiometer calibration  (deg//V)'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/Gain'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Integrator1'
                                        */
  54.0,                                /* Expression: 54
                                        * Referenced by: '<Root>/Kc Gain1'
                                        */
  -170.9,                              /* Computed Parameter: PhaseLead1_A
                                        * Referenced by: '<Root>/Phase Lead1'
                                        */
  -891.92710000000022,                 /* Computed Parameter: PhaseLead1_C
                                        * Referenced by: '<Root>/Phase Lead1'
                                        */
  6.219,                               /* Computed Parameter: PhaseLead1_D
                                        * Referenced by: '<Root>/Phase Lead1'
                                        */
  10.0,                                /* Expression: 10
                                        * Referenced by: '<Root>/Saturation1'
                                        */
  -10.0,                               /* Expression: -10
                                        * Referenced by: '<Root>/Saturation1'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<S1>/Motor Calibration (V//V)'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_CKChannels
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_DOWatchdog
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_EIInitial
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_POModes
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILReadAnalogTimebase_Clock
                                        * Referenced by: '<S1>/HIL Read Analog Timebase'
                                        */

  /*  Computed Parameter: HILInitialize1_AIChannels
   * Referenced by: '<Root>/HIL Initialize1'
   */
  { 0U, 1U },

  /*  Computed Parameter: HILInitialize1_AOChannels
   * Referenced by: '<Root>/HIL Initialize1'
   */
  { 0U, 1U },

  /*  Computed Parameter: HILInitialize1_EIChannels
   * Referenced by: '<Root>/HIL Initialize1'
   */
  { 0U, 1U },
  4U,                                  /* Computed Parameter: HILInitialize1_EIQuadrature
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */

  /*  Computed Parameter: HILReadAnalogTimebase_Channels
   * Referenced by: '<S1>/HIL Read Analog Timebase'
   */
  { 0U, 1U },
  500U,                                /* Computed Parameter: HILReadAnalogTimebase_SamplesIn
                                        * Referenced by: '<S1>/HIL Read Analog Timebase'
                                        */
  0U,                                  /* Computed Parameter: HILReadEncoder_Channels
                                        * Referenced by: '<S1>/HIL Read Encoder'
                                        */
  0U,                                  /* Computed Parameter: HILWrite_AnalogChannels
                                        * Referenced by: '<S1>/HIL Write'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_Active
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_CKPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_CKPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_CKStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_CKEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_AIPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_AIPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_AOPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_AOPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_AOStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_AOEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_AOTerminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_AOExit
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_AOReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_DOPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_DOPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_DOStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_DOEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_DOTerminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_DOExit
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_DOReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_EIPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_EIPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_EIStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_EIEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_POPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_POPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_POStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_POEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_POTerminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_POExit
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_POReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  0,                                   /* Computed Parameter: HILInitialize1_OOReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_DOInitial
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILInitialize1_DOFinal
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  1,                                   /* Computed Parameter: HILReadAnalogTimebase_Active
                                        * Referenced by: '<S1>/HIL Read Analog Timebase'
                                        */
  1,                                   /* Computed Parameter: HILReadEncoder_Active
                                        * Referenced by: '<S1>/HIL Read Encoder'
                                        */
  0                                    /* Computed Parameter: HILWrite_Active
                                        * Referenced by: '<S1>/HIL Write'
                                        */
};
