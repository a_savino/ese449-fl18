/*
 * bb_validation_types.h
 *
 * Code generation for model "bb_validation".
 *
 * Model version              : 1.26
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Wed Nov 28 16:53:43 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_bb_validation_types_h_
#define RTW_HEADER_bb_validation_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_bb_validation_ Parameters_bb_validation;

/* Forward declaration for rtModel */
typedef struct tag_RTM_bb_validation RT_MODEL_bb_validation;

#endif                                 /* RTW_HEADER_bb_validation_types_h_ */
