var = LagPI_Square_Tuned;

plot(var(:,1), var(:,3), var(:,1), var(:,2));

xlabel('time (sec.)')
ylabel('voltage')
legend('input', 'output')
title('PV+Lag Tuned Controller with Sawtooth Input')
ylim([-1.25 1.25])

first_peak = var([1:500], 2);
[out, I] = max(abs(first_peak));

tp = var(I,1)
OS =  out / abs(var(I,3))
SSE = var(586,3) - var(586,2) 