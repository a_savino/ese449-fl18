  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 4;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (PI_controller_P)
    ;%
      section.nData     = 31;
      section.data(31)  = dumData; %prealloc
      
	  ;% PI_controller_P.HILInitialize1_OOStart
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_P.HILInitialize1_OOEnter
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% PI_controller_P.HILInitialize1_OOTerminate
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% PI_controller_P.HILInitialize1_OOExit
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% PI_controller_P.HILInitialize1_AIHigh
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% PI_controller_P.HILInitialize1_AILow
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% PI_controller_P.HILInitialize1_AOHigh
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% PI_controller_P.HILInitialize1_AOLow
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% PI_controller_P.HILInitialize1_AOInitial
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% PI_controller_P.HILInitialize1_AOFinal
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
	  ;% PI_controller_P.HILInitialize1_AOWatchdog
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 10;
	
	  ;% PI_controller_P.HILInitialize1_POFrequency
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 11;
	
	  ;% PI_controller_P.HILInitialize1_POInitial
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 12;
	
	  ;% PI_controller_P.HILInitialize1_POFinal
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 13;
	
	  ;% PI_controller_P.HILInitialize1_POWatchdog
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 14;
	
	  ;% PI_controller_P.SignalGenerator1_Amplitude
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 15;
	
	  ;% PI_controller_P.SignalGenerator1_Frequency
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 16;
	
	  ;% PI_controller_P.LowPassFilter1_A
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 17;
	
	  ;% PI_controller_P.LowPassFilter1_C
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 19;
	
	  ;% PI_controller_P.TachometerCalibrationkrpmV_Gain
	  section.data(20).logicalSrcIdx = 21;
	  section.data(20).dtTransOffset = 21;
	
	  ;% PI_controller_P.PotentiometercalibrationdegV_Ga
	  section.data(21).logicalSrcIdx = 22;
	  section.data(21).dtTransOffset = 22;
	
	  ;% PI_controller_P.HighPassFilter_A
	  section.data(22).logicalSrcIdx = 23;
	  section.data(22).dtTransOffset = 23;
	
	  ;% PI_controller_P.HighPassFilter_C
	  section.data(23).logicalSrcIdx = 24;
	  section.data(23).dtTransOffset = 25;
	
	  ;% PI_controller_P.Integrator_IC
	  section.data(24).logicalSrcIdx = 27;
	  section.data(24).dtTransOffset = 27;
	
	  ;% PI_controller_P.SliderGain_Gain
	  section.data(25).logicalSrcIdx = 28;
	  section.data(25).dtTransOffset = 28;
	
	  ;% PI_controller_P.SliderGain_Gain_a
	  section.data(26).logicalSrcIdx = 29;
	  section.data(26).dtTransOffset = 29;
	
	  ;% PI_controller_P.Saturation1_UpperSat
	  section.data(27).logicalSrcIdx = 30;
	  section.data(27).dtTransOffset = 30;
	
	  ;% PI_controller_P.Saturation1_LowerSat
	  section.data(28).logicalSrcIdx = 31;
	  section.data(28).dtTransOffset = 31;
	
	  ;% PI_controller_P.SliderGain_Gain_l
	  section.data(29).logicalSrcIdx = 32;
	  section.data(29).dtTransOffset = 32;
	
	  ;% PI_controller_P.EncoderCalibrationdegcount_Gain
	  section.data(30).logicalSrcIdx = 33;
	  section.data(30).dtTransOffset = 33;
	
	  ;% PI_controller_P.MotorCalibrationVV_Gain
	  section.data(31).logicalSrcIdx = 34;
	  section.data(31).dtTransOffset = 34;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% PI_controller_P.HILInitialize1_CKChannels
	  section.data(1).logicalSrcIdx = 35;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_P.HILInitialize1_DOWatchdog
	  section.data(2).logicalSrcIdx = 36;
	  section.data(2).dtTransOffset = 1;
	
	  ;% PI_controller_P.HILInitialize1_EIInitial
	  section.data(3).logicalSrcIdx = 37;
	  section.data(3).dtTransOffset = 2;
	
	  ;% PI_controller_P.HILInitialize1_POModes
	  section.data(4).logicalSrcIdx = 38;
	  section.data(4).dtTransOffset = 3;
	
	  ;% PI_controller_P.HILReadAnalogTimebase_Clock
	  section.data(5).logicalSrcIdx = 39;
	  section.data(5).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% PI_controller_P.HILInitialize1_AIChannels
	  section.data(1).logicalSrcIdx = 40;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_P.HILInitialize1_AOChannels
	  section.data(2).logicalSrcIdx = 41;
	  section.data(2).dtTransOffset = 2;
	
	  ;% PI_controller_P.HILInitialize1_EIChannels
	  section.data(3).logicalSrcIdx = 42;
	  section.data(3).dtTransOffset = 4;
	
	  ;% PI_controller_P.HILInitialize1_EIQuadrature
	  section.data(4).logicalSrcIdx = 43;
	  section.data(4).dtTransOffset = 6;
	
	  ;% PI_controller_P.HILReadAnalogTimebase_Channels
	  section.data(5).logicalSrcIdx = 44;
	  section.data(5).dtTransOffset = 7;
	
	  ;% PI_controller_P.HILReadAnalogTimebase_SamplesIn
	  section.data(6).logicalSrcIdx = 45;
	  section.data(6).dtTransOffset = 9;
	
	  ;% PI_controller_P.HILReadEncoder_Channels
	  section.data(7).logicalSrcIdx = 46;
	  section.data(7).dtTransOffset = 10;
	
	  ;% PI_controller_P.HILWrite_AnalogChannels
	  section.data(8).logicalSrcIdx = 47;
	  section.data(8).dtTransOffset = 11;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 38;
      section.data(38)  = dumData; %prealloc
      
	  ;% PI_controller_P.HILInitialize1_Active
	  section.data(1).logicalSrcIdx = 48;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_P.HILInitialize1_CKPStart
	  section.data(2).logicalSrcIdx = 49;
	  section.data(2).dtTransOffset = 1;
	
	  ;% PI_controller_P.HILInitialize1_CKPEnter
	  section.data(3).logicalSrcIdx = 50;
	  section.data(3).dtTransOffset = 2;
	
	  ;% PI_controller_P.HILInitialize1_CKStart
	  section.data(4).logicalSrcIdx = 51;
	  section.data(4).dtTransOffset = 3;
	
	  ;% PI_controller_P.HILInitialize1_CKEnter
	  section.data(5).logicalSrcIdx = 52;
	  section.data(5).dtTransOffset = 4;
	
	  ;% PI_controller_P.HILInitialize1_AIPStart
	  section.data(6).logicalSrcIdx = 53;
	  section.data(6).dtTransOffset = 5;
	
	  ;% PI_controller_P.HILInitialize1_AIPEnter
	  section.data(7).logicalSrcIdx = 54;
	  section.data(7).dtTransOffset = 6;
	
	  ;% PI_controller_P.HILInitialize1_AOPStart
	  section.data(8).logicalSrcIdx = 55;
	  section.data(8).dtTransOffset = 7;
	
	  ;% PI_controller_P.HILInitialize1_AOPEnter
	  section.data(9).logicalSrcIdx = 56;
	  section.data(9).dtTransOffset = 8;
	
	  ;% PI_controller_P.HILInitialize1_AOStart
	  section.data(10).logicalSrcIdx = 57;
	  section.data(10).dtTransOffset = 9;
	
	  ;% PI_controller_P.HILInitialize1_AOEnter
	  section.data(11).logicalSrcIdx = 58;
	  section.data(11).dtTransOffset = 10;
	
	  ;% PI_controller_P.HILInitialize1_AOTerminate
	  section.data(12).logicalSrcIdx = 59;
	  section.data(12).dtTransOffset = 11;
	
	  ;% PI_controller_P.HILInitialize1_AOExit
	  section.data(13).logicalSrcIdx = 60;
	  section.data(13).dtTransOffset = 12;
	
	  ;% PI_controller_P.HILInitialize1_AOReset
	  section.data(14).logicalSrcIdx = 61;
	  section.data(14).dtTransOffset = 13;
	
	  ;% PI_controller_P.HILInitialize1_DOPStart
	  section.data(15).logicalSrcIdx = 62;
	  section.data(15).dtTransOffset = 14;
	
	  ;% PI_controller_P.HILInitialize1_DOPEnter
	  section.data(16).logicalSrcIdx = 63;
	  section.data(16).dtTransOffset = 15;
	
	  ;% PI_controller_P.HILInitialize1_DOStart
	  section.data(17).logicalSrcIdx = 64;
	  section.data(17).dtTransOffset = 16;
	
	  ;% PI_controller_P.HILInitialize1_DOEnter
	  section.data(18).logicalSrcIdx = 65;
	  section.data(18).dtTransOffset = 17;
	
	  ;% PI_controller_P.HILInitialize1_DOTerminate
	  section.data(19).logicalSrcIdx = 66;
	  section.data(19).dtTransOffset = 18;
	
	  ;% PI_controller_P.HILInitialize1_DOExit
	  section.data(20).logicalSrcIdx = 67;
	  section.data(20).dtTransOffset = 19;
	
	  ;% PI_controller_P.HILInitialize1_DOReset
	  section.data(21).logicalSrcIdx = 68;
	  section.data(21).dtTransOffset = 20;
	
	  ;% PI_controller_P.HILInitialize1_EIPStart
	  section.data(22).logicalSrcIdx = 69;
	  section.data(22).dtTransOffset = 21;
	
	  ;% PI_controller_P.HILInitialize1_EIPEnter
	  section.data(23).logicalSrcIdx = 70;
	  section.data(23).dtTransOffset = 22;
	
	  ;% PI_controller_P.HILInitialize1_EIStart
	  section.data(24).logicalSrcIdx = 71;
	  section.data(24).dtTransOffset = 23;
	
	  ;% PI_controller_P.HILInitialize1_EIEnter
	  section.data(25).logicalSrcIdx = 72;
	  section.data(25).dtTransOffset = 24;
	
	  ;% PI_controller_P.HILInitialize1_POPStart
	  section.data(26).logicalSrcIdx = 73;
	  section.data(26).dtTransOffset = 25;
	
	  ;% PI_controller_P.HILInitialize1_POPEnter
	  section.data(27).logicalSrcIdx = 74;
	  section.data(27).dtTransOffset = 26;
	
	  ;% PI_controller_P.HILInitialize1_POStart
	  section.data(28).logicalSrcIdx = 75;
	  section.data(28).dtTransOffset = 27;
	
	  ;% PI_controller_P.HILInitialize1_POEnter
	  section.data(29).logicalSrcIdx = 76;
	  section.data(29).dtTransOffset = 28;
	
	  ;% PI_controller_P.HILInitialize1_POTerminate
	  section.data(30).logicalSrcIdx = 77;
	  section.data(30).dtTransOffset = 29;
	
	  ;% PI_controller_P.HILInitialize1_POExit
	  section.data(31).logicalSrcIdx = 78;
	  section.data(31).dtTransOffset = 30;
	
	  ;% PI_controller_P.HILInitialize1_POReset
	  section.data(32).logicalSrcIdx = 79;
	  section.data(32).dtTransOffset = 31;
	
	  ;% PI_controller_P.HILInitialize1_OOReset
	  section.data(33).logicalSrcIdx = 80;
	  section.data(33).dtTransOffset = 32;
	
	  ;% PI_controller_P.HILInitialize1_DOInitial
	  section.data(34).logicalSrcIdx = 81;
	  section.data(34).dtTransOffset = 33;
	
	  ;% PI_controller_P.HILInitialize1_DOFinal
	  section.data(35).logicalSrcIdx = 82;
	  section.data(35).dtTransOffset = 34;
	
	  ;% PI_controller_P.HILReadAnalogTimebase_Active
	  section.data(36).logicalSrcIdx = 83;
	  section.data(36).dtTransOffset = 35;
	
	  ;% PI_controller_P.HILReadEncoder_Active
	  section.data(37).logicalSrcIdx = 84;
	  section.data(37).dtTransOffset = 36;
	
	  ;% PI_controller_P.HILWrite_Active
	  section.data(38).logicalSrcIdx = 85;
	  section.data(38).dtTransOffset = 37;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 1;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (PI_controller_B)
    ;%
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% PI_controller_B.SignalGenerator1
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_B.LowPassFilter1
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% PI_controller_B.TachometerCalibrationkrpmV
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% PI_controller_B.PotentiometercalibrationdegV
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% PI_controller_B.HighPassFilter
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% PI_controller_B.SliderGain
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% PI_controller_B.EncoderCalibrationdegcount
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% PI_controller_B.MotorCalibrationVV
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 5;
    sectIdxOffset = 1;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (PI_controller_DWork)
    ;%
      section.nData     = 7;
      section.data(7)  = dumData; %prealloc
      
	  ;% PI_controller_DWork.HILInitialize1_AIMinimums
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_DWork.HILInitialize1_AIMaximums
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 2;
	
	  ;% PI_controller_DWork.HILInitialize1_AOMinimums
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 4;
	
	  ;% PI_controller_DWork.HILInitialize1_AOMaximums
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 6;
	
	  ;% PI_controller_DWork.HILInitialize1_AOVoltages
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 8;
	
	  ;% PI_controller_DWork.HILInitialize1_FilterFrequency
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 10;
	
	  ;% PI_controller_DWork.HILReadAnalogTimebase_Buffer
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 12;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% PI_controller_DWork.HILReadAnalogTimebase_Task
	  section.data(1).logicalSrcIdx = 7;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% PI_controller_DWork.HILInitialize1_Card
	  section.data(1).logicalSrcIdx = 8;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% PI_controller_DWork.Output_PWORK.LoggedData
	  section.data(1).logicalSrcIdx = 9;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_DWork.W_Irpm_PWORK.LoggedData
	  section.data(2).logicalSrcIdx = 10;
	  section.data(2).dtTransOffset = 1;
	
	  ;% PI_controller_DWork.theta_Ideg_PWORK.LoggedData
	  section.data(3).logicalSrcIdx = 11;
	  section.data(3).dtTransOffset = 2;
	
	  ;% PI_controller_DWork.theta_Ideg1_PWORK.LoggedData
	  section.data(4).logicalSrcIdx = 12;
	  section.data(4).dtTransOffset = 3;
	
	  ;% PI_controller_DWork.HILReadEncoder_PWORK
	  section.data(5).logicalSrcIdx = 13;
	  section.data(5).dtTransOffset = 4;
	
	  ;% PI_controller_DWork.HILWrite_PWORK
	  section.data(6).logicalSrcIdx = 14;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% PI_controller_DWork.HILInitialize1_ClockModes
	  section.data(1).logicalSrcIdx = 15;
	  section.data(1).dtTransOffset = 0;
	
	  ;% PI_controller_DWork.HILInitialize1_QuadratureModes
	  section.data(2).logicalSrcIdx = 16;
	  section.data(2).dtTransOffset = 1;
	
	  ;% PI_controller_DWork.HILInitialize1_InitialEICounts
	  section.data(3).logicalSrcIdx = 17;
	  section.data(3).dtTransOffset = 3;
	
	  ;% PI_controller_DWork.HILReadEncoder_Buffer
	  section.data(4).logicalSrcIdx = 18;
	  section.data(4).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 773355888;
  targMap.checksum1 = 623729290;
  targMap.checksum2 = 3979139984;
  targMap.checksum3 = 2262029688;

