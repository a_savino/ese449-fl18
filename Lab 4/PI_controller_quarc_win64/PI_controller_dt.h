/*
 * PI_controller_dt.h
 *
 * Code generation for model "PI_controller".
 *
 * Model version              : 1.6
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Fri Nov 09 13:47:27 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(t_task),
  sizeof(t_card)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "t_task",
  "t_card"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&PI_controller_B.SignalGenerator1), 0, 0, 8 }
  ,

  { (char_T *)(&PI_controller_DWork.HILInitialize1_AIMinimums[0]), 0, 0, 14 },

  { (char_T *)(&PI_controller_DWork.HILReadAnalogTimebase_Task), 14, 0, 1 },

  { (char_T *)(&PI_controller_DWork.HILInitialize1_Card), 15, 0, 1 },

  { (char_T *)(&PI_controller_DWork.Output_PWORK.LoggedData), 11, 0, 6 },

  { (char_T *)(&PI_controller_DWork.HILInitialize1_ClockModes), 6, 0, 6 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  6U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&PI_controller_P.HILInitialize1_OOStart), 0, 0, 35 },

  { (char_T *)(&PI_controller_P.HILInitialize1_CKChannels), 6, 0, 5 },

  { (char_T *)(&PI_controller_P.HILInitialize1_AIChannels[0]), 7, 0, 12 },

  { (char_T *)(&PI_controller_P.HILInitialize1_Active), 8, 0, 38 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  4U,
  rtPTransitions
};

/* [EOF] PI_controller_dt.h */
