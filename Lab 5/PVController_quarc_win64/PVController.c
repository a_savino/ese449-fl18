/*
 * PVController.c
 *
 * Code generation for model "PVController".
 *
 * Model version              : 1.16
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Fri Nov 09 13:48:47 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "PVController.h"
#include "PVController_private.h"
#include "PVController_dt.h"

/* Block signals (auto storage) */
BlockIO_PVController PVController_B;

/* Continuous states */
ContinuousStates_PVController PVController_X;

/* Block states (auto storage) */
D_Work_PVController PVController_DWork;

/* Real-time model */
RT_MODEL_PVController PVController_M_;
RT_MODEL_PVController *const PVController_M = &PVController_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 2;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  PVController_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void PVController_step(void)
{
  /* local block i/o variables */
  real_T rtb_LowPassFilter;
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_HILReadAnalogTimebase_o2;
  real_T rtb_HILReadEncoder;
  real_T rtb_Sum2;
  real_T temp;
  if (rtmIsMajorTimeStep(PVController_M)) {
    /* set solver stop time */
    if (!(PVController_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&PVController_M->solverInfo,
                            ((PVController_M->Timing.clockTickH0 + 1) *
        PVController_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&PVController_M->solverInfo,
                            ((PVController_M->Timing.clockTick0 + 1) *
        PVController_M->Timing.stepSize0 + PVController_M->Timing.clockTickH0 *
        PVController_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(PVController_M)) {
    PVController_M->Timing.t[0] = rtsiGetT(&PVController_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(PVController_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

    /* S-Function Block: PVController/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog
        (PVController_DWork.HILReadAnalogTimebase_Task, 1,
         &PVController_DWork.HILReadAnalogTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
      }

      rtb_HILReadAnalogTimebase_o1 =
        PVController_DWork.HILReadAnalogTimebase_Buffer[0];
      rtb_HILReadAnalogTimebase_o2 =
        PVController_DWork.HILReadAnalogTimebase_Buffer[1];
    }
  }

  /* SignalGenerator: '<Root>/Signal Generator' */
  temp = PVController_P.SignalGenerator_Frequency * PVController_M->Timing.t[0];
  PVController_B.SignalGenerator = (1.0 - (temp - floor(temp)) * 2.0) *
    PVController_P.SignalGenerator_Amplitude;
  if (rtmIsMajorTimeStep(PVController_M)) {
    /* S-Function (hil_read_encoder_block): '<S1>/HIL Read Encoder' */

    /* S-Function Block: PVController/Subsystem/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(PVController_DWork.HILInitialize_Card,
        &PVController_P.HILReadEncoder_Channels, 1,
        &PVController_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder = PVController_DWork.HILReadEncoder_Buffer;
      }
    }

    /* Gain: '<S1>/Encoder Calibration (deg//count)' */
    PVController_B.EncoderCalibrationdegcount =
      PVController_P.EncoderCalibrationdegcount_Gain * rtb_HILReadEncoder;

    /* Gain: '<S1>/Tachometer Calibration (krpm//V)' */
    PVController_B.TachometerCalibrationkrpmV =
      PVController_P.TachometerCalibrationkrpmV_Gain *
      rtb_HILReadAnalogTimebase_o2;

    /* Gain: '<S1>/Potentiometer calibration  (deg//V)' */
    PVController_B.PotentiometercalibrationdegV =
      PVController_P.PotentiometercalibrationdegV_Ga *
      rtb_HILReadAnalogTimebase_o1;
  }

  /* TransferFcn: '<Root>/Low Pass Filter' */
  rtb_LowPassFilter = PVController_P.LowPassFilter_C[0]*
    PVController_X.LowPassFilter_CSTATE[0]
    + PVController_P.LowPassFilter_C[1]*PVController_X.LowPassFilter_CSTATE[1];

  /* Derivative: '<Root>/Derivative1' */
  {
    real_T t = PVController_M->Timing.t[0];
    real_T timeStampA = PVController_DWork.Derivative1_RWORK.TimeStampA;
    real_T timeStampB = PVController_DWork.Derivative1_RWORK.TimeStampB;
    real_T *lastU = &PVController_DWork.Derivative1_RWORK.LastUAtTimeA;
    if (timeStampA >= t && timeStampB >= t) {
      rtb_Sum2 = 0.0;
    } else {
      real_T deltaT;
      real_T lastTime = timeStampA;
      if (timeStampA < timeStampB) {
        if (timeStampB < t) {
          lastTime = timeStampB;
          lastU = &PVController_DWork.Derivative1_RWORK.LastUAtTimeB;
        }
      } else if (timeStampA >= t) {
        lastTime = timeStampB;
        lastU = &PVController_DWork.Derivative1_RWORK.LastUAtTimeB;
      }

      deltaT = t - lastTime;
      rtb_Sum2 = (rtb_LowPassFilter - *lastU++) / deltaT;
    }
  }

  /* Sum: '<Root>/Sum2' incorporates:
   *  Gain: '<Root>/Gain'
   *  Gain: '<Root>/Kp1'
   *  Gain: '<Root>/Kv2'
   *  Gain: '<Root>/Kv3'
   *  Sum: '<Root>/Sum1'
   */
  rtb_Sum2 = (PVController_P.Gain_Gain * PVController_B.SignalGenerator -
              PVController_B.EncoderCalibrationdegcount) *
    PVController_P.Kp1_Gain + PVController_P.Kv3_Gain * rtb_Sum2 *
    PVController_P.Kv2_Gain;

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_Sum2 >= PVController_P.Saturation1_UpperSat) {
    temp = PVController_P.Saturation1_UpperSat;
  } else if (rtb_Sum2 <= PVController_P.Saturation1_LowerSat) {
    temp = PVController_P.Saturation1_LowerSat;
  } else {
    temp = rtb_Sum2;
  }

  /* Gain: '<S1>/Motor Calibration (V//V)' incorporates:
   *  Saturate: '<Root>/Saturation1'
   */
  PVController_B.MotorCalibrationVV = PVController_P.MotorCalibrationVV_Gain *
    temp;
  if (rtmIsMajorTimeStep(PVController_M)) {
    /* S-Function (hil_write_block): '<S1>/HIL Write' */

    /* S-Function Block: PVController/Subsystem/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(PVController_DWork.HILInitialize_Card,
                         &PVController_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &PVController_B.MotorCalibrationVV,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
      }
    }
  }

  if (rtmIsMajorTimeStep(PVController_M)) {
    /* Update for Derivative: '<Root>/Derivative1' */
    {
      real_T timeStampA = PVController_DWork.Derivative1_RWORK.TimeStampA;
      real_T timeStampB = PVController_DWork.Derivative1_RWORK.TimeStampB;
      real_T* lastTime = &PVController_DWork.Derivative1_RWORK.TimeStampA;
      real_T* lastU = &PVController_DWork.Derivative1_RWORK.LastUAtTimeA;
      if (timeStampA != rtInf) {
        if (timeStampB == rtInf) {
          lastTime = &PVController_DWork.Derivative1_RWORK.TimeStampB;
          lastU = &PVController_DWork.Derivative1_RWORK.LastUAtTimeB;
        } else if (timeStampA >= timeStampB) {
          lastTime = &PVController_DWork.Derivative1_RWORK.TimeStampB;
          lastU = &PVController_DWork.Derivative1_RWORK.LastUAtTimeB;
        }
      }

      *lastTime = PVController_M->Timing.t[0];
      *lastU++ = rtb_LowPassFilter;
    }

    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, PVController_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(PVController_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((PVController_M->Timing.clockTick1+
                            PVController_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(PVController_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(PVController_M)!=-1) &&
          !((rtmGetTFinal(PVController_M)-(((PVController_M->Timing.clockTick1+
               PVController_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((PVController_M->Timing.clockTick1+
               PVController_M->Timing.clockTickH1* 4294967296.0)) * 0.002) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(PVController_M, "Simulation finished");
      }

      if (rtmGetStopRequested(PVController_M)) {
        rtmSetErrorStatus(PVController_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&PVController_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++PVController_M->Timing.clockTick0)) {
      ++PVController_M->Timing.clockTickH0;
    }

    PVController_M->Timing.t[0] = rtsiGetSolverStopTime
      (&PVController_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      PVController_M->Timing.clockTick1++;
      if (!PVController_M->Timing.clockTick1) {
        PVController_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void PVController_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Low Pass Filter' */
  {
    ((StateDerivatives_PVController *) PVController_M->ModelData.derivs)
      ->LowPassFilter_CSTATE[0] = PVController_B.EncoderCalibrationdegcount;
    ((StateDerivatives_PVController *) PVController_M->ModelData.derivs)
      ->LowPassFilter_CSTATE[0] += (PVController_P.LowPassFilter_A[0])*
      PVController_X.LowPassFilter_CSTATE[0]
      + (PVController_P.LowPassFilter_A[1])*PVController_X.LowPassFilter_CSTATE
      [1];
    ((StateDerivatives_PVController *) PVController_M->ModelData.derivs)
      ->LowPassFilter_CSTATE[1]= PVController_X.LowPassFilter_CSTATE[0];
  }
}

/* Model initialize function */
void PVController_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)PVController_M, 0,
                sizeof(RT_MODEL_PVController));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&PVController_M->solverInfo,
                          &PVController_M->Timing.simTimeStep);
    rtsiSetTPtr(&PVController_M->solverInfo, &rtmGetTPtr(PVController_M));
    rtsiSetStepSizePtr(&PVController_M->solverInfo,
                       &PVController_M->Timing.stepSize0);
    rtsiSetdXPtr(&PVController_M->solverInfo, &PVController_M->ModelData.derivs);
    rtsiSetContStatesPtr(&PVController_M->solverInfo,
                         &PVController_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&PVController_M->solverInfo,
      &PVController_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&PVController_M->solverInfo, (&rtmGetErrorStatus
      (PVController_M)));
    rtsiSetRTModelPtr(&PVController_M->solverInfo, PVController_M);
  }

  rtsiSetSimTimeStep(&PVController_M->solverInfo, MAJOR_TIME_STEP);
  PVController_M->ModelData.intgData.f[0] = PVController_M->ModelData.odeF[0];
  PVController_M->ModelData.contStates = ((real_T *) &PVController_X);
  rtsiSetSolverData(&PVController_M->solverInfo, (void *)
                    &PVController_M->ModelData.intgData);
  rtsiSetSolverName(&PVController_M->solverInfo,"ode1");
  rtmSetTPtr(PVController_M, &PVController_M->Timing.tArray[0]);
  rtmSetTFinal(PVController_M, 8.0);
  PVController_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  PVController_M->Sizes.checksums[0] = (1103358497U);
  PVController_M->Sizes.checksums[1] = (3278430575U);
  PVController_M->Sizes.checksums[2] = (2883785705U);
  PVController_M->Sizes.checksums[3] = (2083304603U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    PVController_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(PVController_M->extModeInfo,
      &PVController_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(PVController_M->extModeInfo,
                        PVController_M->Sizes.checksums);
    rteiSetTPtr(PVController_M->extModeInfo, rtmGetTPtr(PVController_M));
  }

  /* block I/O */
  (void) memset(((void *) &PVController_B), 0,
                sizeof(BlockIO_PVController));

  /* states (continuous) */
  {
    (void) memset((void *)&PVController_X, 0,
                  sizeof(ContinuousStates_PVController));
  }

  /* states (dwork) */
  (void) memset((void *)&PVController_DWork, 0,
                sizeof(D_Work_PVController));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    PVController_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: PVController/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &PVController_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PVController_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options(PVController_DWork.HILInitialize_Card,
      "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PVController_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(PVController_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PVController_M, _rt_error_message);
      return;
    }

    if ((PVController_P.HILInitialize_AIPStart && !is_switching) ||
        (PVController_P.HILInitialize_AIPEnter && is_switching)) {
      PVController_DWork.HILInitialize_AIMinimums[0] =
        PVController_P.HILInitialize_AILow;
      PVController_DWork.HILInitialize_AIMinimums[1] =
        PVController_P.HILInitialize_AILow;
      PVController_DWork.HILInitialize_AIMaximums[0] =
        PVController_P.HILInitialize_AIHigh;
      PVController_DWork.HILInitialize_AIMaximums[1] =
        PVController_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(PVController_DWork.HILInitialize_Card,
        PVController_P.HILInitialize_AIChannels, 2U,
        &PVController_DWork.HILInitialize_AIMinimums[0],
        &PVController_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
        return;
      }
    }

    if ((PVController_P.HILInitialize_AOPStart && !is_switching) ||
        (PVController_P.HILInitialize_AOPEnter && is_switching)) {
      PVController_DWork.HILInitialize_AOMinimums[0] =
        PVController_P.HILInitialize_AOLow;
      PVController_DWork.HILInitialize_AOMinimums[1] =
        PVController_P.HILInitialize_AOLow;
      PVController_DWork.HILInitialize_AOMaximums[0] =
        PVController_P.HILInitialize_AOHigh;
      PVController_DWork.HILInitialize_AOMaximums[1] =
        PVController_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges
        (PVController_DWork.HILInitialize_Card,
         PVController_P.HILInitialize_AOChannels, 2U,
         &PVController_DWork.HILInitialize_AOMinimums[0],
         &PVController_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
        return;
      }
    }

    if ((PVController_P.HILInitialize_AOStart && !is_switching) ||
        (PVController_P.HILInitialize_AOEnter && is_switching)) {
      PVController_DWork.HILInitialize_AOVoltages[0] =
        PVController_P.HILInitialize_AOInitial;
      PVController_DWork.HILInitialize_AOVoltages[1] =
        PVController_P.HILInitialize_AOInitial;
      result = hil_write_analog(PVController_DWork.HILInitialize_Card,
        PVController_P.HILInitialize_AOChannels, 2U,
        &PVController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
        return;
      }
    }

    if (PVController_P.HILInitialize_AOReset) {
      PVController_DWork.HILInitialize_AOVoltages[0] =
        PVController_P.HILInitialize_AOWatchdog;
      PVController_DWork.HILInitialize_AOVoltages[1] =
        PVController_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (PVController_DWork.HILInitialize_Card,
         PVController_P.HILInitialize_AOChannels, 2U,
         &PVController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
        return;
      }
    }

    if ((PVController_P.HILInitialize_EIPStart && !is_switching) ||
        (PVController_P.HILInitialize_EIPEnter && is_switching)) {
      PVController_DWork.HILInitialize_QuadratureModes[0] =
        PVController_P.HILInitialize_EIQuadrature;
      PVController_DWork.HILInitialize_QuadratureModes[1] =
        PVController_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (PVController_DWork.HILInitialize_Card,
         PVController_P.HILInitialize_EIChannels, 2U, (t_encoder_quadrature_mode
          *) &PVController_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
        return;
      }
    }

    if ((PVController_P.HILInitialize_EIStart && !is_switching) ||
        (PVController_P.HILInitialize_EIEnter && is_switching)) {
      PVController_DWork.HILInitialize_InitialEICounts[0] =
        PVController_P.HILInitialize_EIInitial;
      PVController_DWork.HILInitialize_InitialEICounts[1] =
        PVController_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(PVController_DWork.HILInitialize_Card,
        PVController_P.HILInitialize_EIChannels, 2U,
        &PVController_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

  /* S-Function Block: PVController/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader(PVController_DWork.HILInitialize_Card,
      PVController_P.HILReadAnalogTimebase_SamplesIn,
      PVController_P.HILReadAnalogTimebase_Channels, 2,
      &PVController_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PVController_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<Root>/Low Pass Filter' */
  PVController_X.LowPassFilter_CSTATE[0] = 0.0;
  PVController_X.LowPassFilter_CSTATE[1] = 0.0;

  /* InitializeConditions for Derivative: '<Root>/Derivative1' */
  PVController_DWork.Derivative1_RWORK.TimeStampA = rtInf;
  PVController_DWork.Derivative1_RWORK.TimeStampB = rtInf;
}

/* Model terminate function */
void PVController_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: PVController/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(PVController_DWork.HILInitialize_Card);
    hil_monitor_stop_all(PVController_DWork.HILInitialize_Card);
    is_switching = false;
    if ((PVController_P.HILInitialize_AOTerminate && !is_switching) ||
        (PVController_P.HILInitialize_AOExit && is_switching)) {
      PVController_DWork.HILInitialize_AOVoltages[0] =
        PVController_P.HILInitialize_AOFinal;
      PVController_DWork.HILInitialize_AOVoltages[1] =
        PVController_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(PVController_DWork.HILInitialize_Card,
        PVController_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &PVController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PVController_M, _rt_error_message);
      }
    }

    hil_task_delete_all(PVController_DWork.HILInitialize_Card);
    hil_monitor_delete_all(PVController_DWork.HILInitialize_Card);
    hil_close(PVController_DWork.HILInitialize_Card);
    PVController_DWork.HILInitialize_Card = NULL;
  }
}
