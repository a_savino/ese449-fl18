%% calculating Phase Lead controller for our system

K = 1.6268;
tao = .03;
P = tf(K, [tao 1]);
l = tf(1, [1 0]);
Pi = series(l,P);
[Gm, Pm,Wcg,Wcp] = margin(Pi);
bandW = bandwidth(feedback(Pi, 1));
Kc = 55.5;
Lp = Kc*Pi;
margin(Lp);

Phi_desired = 75;
Phi_measured = 33.7;
Phi_margin =  Phi_desired - Phi_measured + 5; 
Phi_m_rad = Phi_margin * pi/180;
a = (1+sin(Phi_m_rad))/(1-sin(Phi_m_rad));

om_max = 77.75;
p2 = om_max * sqrt(a);
z2 = p2/a;

PhaseLead = (p2/z2) * tf([1 z2],[1, p2]);
