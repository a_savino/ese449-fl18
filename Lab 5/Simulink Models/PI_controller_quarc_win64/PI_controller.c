/*
 * PI_controller.c
 *
 * Code generation for model "PI_controller".
 *
 * Model version              : 1.8
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Sat Nov 10 16:15:36 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "PI_controller.h"
#include "PI_controller_private.h"
#include "PI_controller_dt.h"

/* Block signals (auto storage) */
BlockIO_PI_controller PI_controller_B;

/* Continuous states */
ContinuousStates_PI_controller PI_controller_X;

/* Block states (auto storage) */
D_Work_PI_controller PI_controller_DWork;

/* Real-time model */
RT_MODEL_PI_controller PI_controller_M_;
RT_MODEL_PI_controller *const PI_controller_M = &PI_controller_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 5;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  PI_controller_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void PI_controller_step(void)
{
  /* local block i/o variables */
  real_T rtb_TransferFcn;
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_HILReadEncoder;
  real_T rtb_Saturation1;
  if (rtmIsMajorTimeStep(PI_controller_M)) {
    /* set solver stop time */
    if (!(PI_controller_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&PI_controller_M->solverInfo,
                            ((PI_controller_M->Timing.clockTickH0 + 1) *
        PI_controller_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&PI_controller_M->solverInfo,
                            ((PI_controller_M->Timing.clockTick0 + 1) *
        PI_controller_M->Timing.stepSize0 + PI_controller_M->Timing.clockTickH0 *
        PI_controller_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(PI_controller_M)) {
    PI_controller_M->Timing.t[0] = rtsiGetT(&PI_controller_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(PI_controller_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<S4>/HIL Read Analog Timebase' */

    /* S-Function Block: PI_controller/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog
        (PI_controller_DWork.HILReadAnalogTimebase_Task, 1,
         &PI_controller_DWork.HILReadAnalogTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
      }

      rtb_HILReadAnalogTimebase_o1 =
        PI_controller_DWork.HILReadAnalogTimebase_Buffer[0];
      rtb_HILReadEncoder = PI_controller_DWork.HILReadAnalogTimebase_Buffer[1];
    }
  }

  /* SignalGenerator: '<Root>/Signal Generator1' */
  rtb_Saturation1 = PI_controller_P.SignalGenerator1_Frequency *
    PI_controller_M->Timing.t[0];
  PI_controller_B.SignalGenerator1 = (1.0 - (rtb_Saturation1 - floor
    (rtb_Saturation1)) * 2.0) * PI_controller_P.SignalGenerator1_Amplitude;
  if (rtmIsMajorTimeStep(PI_controller_M)) {
    /* Gain: '<S4>/Tachometer Calibration (krpm//V)' */
    PI_controller_B.TachometerCalibrationkrpmV =
      PI_controller_P.TachometerCalibrationkrpmV_Gain * rtb_HILReadEncoder;

    /* Gain: '<S4>/Potentiometer calibration  (deg//V)' */
    PI_controller_B.PotentiometercalibrationdegV =
      PI_controller_P.PotentiometercalibrationdegV_Ga *
      rtb_HILReadAnalogTimebase_o1;
  }

  /* TransferFcn: '<S4>/High Pass Filter ' */
  PI_controller_B.HighPassFilter = PI_controller_P.HighPassFilter_C[0]*
    PI_controller_X.HighPassFilter_CSTATE[0]
    + PI_controller_P.HighPassFilter_C[1]*PI_controller_X.HighPassFilter_CSTATE
    [1];
  if (rtmIsMajorTimeStep(PI_controller_M)) {
  }

  /* Sum: '<Root>/Sum2' incorporates:
   *  Gain: '<S1>/Slider Gain'
   *  Gain: '<S3>/Slider Gain'
   *  Integrator: '<Root>/Integrator'
   *  Sum: '<Root>/Sum1'
   */
  rtb_Saturation1 = (PI_controller_P.SliderGain_Gain *
                     PI_controller_B.SignalGenerator1 -
                     PI_controller_B.TachometerCalibrationkrpmV) *
    PI_controller_P.SliderGain_Gain_a + PI_controller_X.Integrator_CSTATE;

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_Saturation1 >= PI_controller_P.Saturation1_UpperSat) {
    rtb_Saturation1 = PI_controller_P.Saturation1_UpperSat;
  } else {
    if (rtb_Saturation1 <= PI_controller_P.Saturation1_LowerSat) {
      rtb_Saturation1 = PI_controller_P.Saturation1_LowerSat;
    }
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Gain: '<S2>/Slider Gain' incorporates:
   *  Sum: '<Root>/Sum'
   */
  PI_controller_B.SliderGain = (PI_controller_B.SignalGenerator1 -
    PI_controller_B.TachometerCalibrationkrpmV) *
    PI_controller_P.SliderGain_Gain_l;
  if (rtmIsMajorTimeStep(PI_controller_M)) {
    /* S-Function (hil_read_encoder_block): '<S4>/HIL Read Encoder' */

    /* S-Function Block: PI_controller/Subsystem/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(PI_controller_DWork.HILInitialize1_Card,
        &PI_controller_P.HILReadEncoder_Channels, 1,
        &PI_controller_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder = PI_controller_DWork.HILReadEncoder_Buffer;
      }
    }

    /* Gain: '<S4>/Encoder Calibration (deg//count)' */
    PI_controller_B.EncoderCalibrationdegcount =
      PI_controller_P.EncoderCalibrationdegcount_Gain * rtb_HILReadEncoder;
  }

  /* Gain: '<S4>/Motor Calibration (V//V)' */
  PI_controller_B.MotorCalibrationVV = PI_controller_P.MotorCalibrationVV_Gain *
    rtb_Saturation1;
  if (rtmIsMajorTimeStep(PI_controller_M)) {
    /* S-Function (hil_write_block): '<S4>/HIL Write' */

    /* S-Function Block: PI_controller/Subsystem/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(PI_controller_DWork.HILInitialize1_Card,
                         &PI_controller_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &PI_controller_B.MotorCalibrationVV,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
      }
    }
  }

  /* TransferFcn: '<Root>/Transfer Fcn' */
  rtb_TransferFcn = PI_controller_P.TransferFcn_C[0]*
    PI_controller_X.TransferFcn_CSTATE[0]
    + PI_controller_P.TransferFcn_C[1]*PI_controller_X.TransferFcn_CSTATE[1];
  if (rtmIsMajorTimeStep(PI_controller_M)) {
    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, PI_controller_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(PI_controller_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((PI_controller_M->Timing.clockTick1+
                            PI_controller_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(PI_controller_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(PI_controller_M)!=-1) &&
          !((rtmGetTFinal(PI_controller_M)-(((PI_controller_M->Timing.clockTick1
               +PI_controller_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((PI_controller_M->Timing.clockTick1+
               PI_controller_M->Timing.clockTickH1* 4294967296.0)) * 0.002) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(PI_controller_M, "Simulation finished");
      }

      if (rtmGetStopRequested(PI_controller_M)) {
        rtmSetErrorStatus(PI_controller_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&PI_controller_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++PI_controller_M->Timing.clockTick0)) {
      ++PI_controller_M->Timing.clockTickH0;
    }

    PI_controller_M->Timing.t[0] = rtsiGetSolverStopTime
      (&PI_controller_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      PI_controller_M->Timing.clockTick1++;
      if (!PI_controller_M->Timing.clockTick1) {
        PI_controller_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void PI_controller_derivatives(void)
{
  /* Derivatives for TransferFcn: '<S4>/High Pass Filter ' */
  {
    ((StateDerivatives_PI_controller *) PI_controller_M->ModelData.derivs)
      ->HighPassFilter_CSTATE[0] = PI_controller_B.EncoderCalibrationdegcount;
    ((StateDerivatives_PI_controller *) PI_controller_M->ModelData.derivs)
      ->HighPassFilter_CSTATE[0] += (PI_controller_P.HighPassFilter_A[0])*
      PI_controller_X.HighPassFilter_CSTATE[0]
      + (PI_controller_P.HighPassFilter_A[1])*
      PI_controller_X.HighPassFilter_CSTATE[1];
    ((StateDerivatives_PI_controller *) PI_controller_M->ModelData.derivs)
      ->HighPassFilter_CSTATE[1]= PI_controller_X.HighPassFilter_CSTATE[0];
  }

  /* Derivatives for Integrator: '<Root>/Integrator' */
  ((StateDerivatives_PI_controller *) PI_controller_M->ModelData.derivs)
    ->Integrator_CSTATE = PI_controller_B.SliderGain;

  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn' */
  {
    ((StateDerivatives_PI_controller *) PI_controller_M->ModelData.derivs)
      ->TransferFcn_CSTATE[0] = 0.0;
    ((StateDerivatives_PI_controller *) PI_controller_M->ModelData.derivs)
      ->TransferFcn_CSTATE[0] += (PI_controller_P.TransferFcn_A[0])*
      PI_controller_X.TransferFcn_CSTATE[0]
      + (PI_controller_P.TransferFcn_A[1])*PI_controller_X.TransferFcn_CSTATE[1];
    ((StateDerivatives_PI_controller *) PI_controller_M->ModelData.derivs)
      ->TransferFcn_CSTATE[1]= PI_controller_X.TransferFcn_CSTATE[0];
  }
}

/* Model initialize function */
void PI_controller_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)PI_controller_M, 0,
                sizeof(RT_MODEL_PI_controller));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&PI_controller_M->solverInfo,
                          &PI_controller_M->Timing.simTimeStep);
    rtsiSetTPtr(&PI_controller_M->solverInfo, &rtmGetTPtr(PI_controller_M));
    rtsiSetStepSizePtr(&PI_controller_M->solverInfo,
                       &PI_controller_M->Timing.stepSize0);
    rtsiSetdXPtr(&PI_controller_M->solverInfo,
                 &PI_controller_M->ModelData.derivs);
    rtsiSetContStatesPtr(&PI_controller_M->solverInfo,
                         &PI_controller_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&PI_controller_M->solverInfo,
      &PI_controller_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&PI_controller_M->solverInfo, (&rtmGetErrorStatus
      (PI_controller_M)));
    rtsiSetRTModelPtr(&PI_controller_M->solverInfo, PI_controller_M);
  }

  rtsiSetSimTimeStep(&PI_controller_M->solverInfo, MAJOR_TIME_STEP);
  PI_controller_M->ModelData.intgData.f[0] = PI_controller_M->ModelData.odeF[0];
  PI_controller_M->ModelData.contStates = ((real_T *) &PI_controller_X);
  rtsiSetSolverData(&PI_controller_M->solverInfo, (void *)
                    &PI_controller_M->ModelData.intgData);
  rtsiSetSolverName(&PI_controller_M->solverInfo,"ode1");
  rtmSetTPtr(PI_controller_M, &PI_controller_M->Timing.tArray[0]);
  rtmSetTFinal(PI_controller_M, 4.5);
  PI_controller_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  PI_controller_M->Sizes.checksums[0] = (446938821U);
  PI_controller_M->Sizes.checksums[1] = (3245208511U);
  PI_controller_M->Sizes.checksums[2] = (2558732762U);
  PI_controller_M->Sizes.checksums[3] = (1227610510U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    PI_controller_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(PI_controller_M->extModeInfo,
      &PI_controller_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(PI_controller_M->extModeInfo,
                        PI_controller_M->Sizes.checksums);
    rteiSetTPtr(PI_controller_M->extModeInfo, rtmGetTPtr(PI_controller_M));
  }

  /* block I/O */
  (void) memset(((void *) &PI_controller_B), 0,
                sizeof(BlockIO_PI_controller));

  /* states (continuous) */
  {
    (void) memset((void *)&PI_controller_X, 0,
                  sizeof(ContinuousStates_PI_controller));
  }

  /* states (dwork) */
  (void) memset((void *)&PI_controller_DWork, 0,
                sizeof(D_Work_PI_controller));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    PI_controller_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize1' */

  /* S-Function Block: PI_controller/HIL Initialize1 (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &PI_controller_DWork.HILInitialize1_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PI_controller_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options
      (PI_controller_DWork.HILInitialize1_Card,
       "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PI_controller_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(PI_controller_DWork.HILInitialize1_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PI_controller_M, _rt_error_message);
      return;
    }

    if ((PI_controller_P.HILInitialize1_AIPStart && !is_switching) ||
        (PI_controller_P.HILInitialize1_AIPEnter && is_switching)) {
      PI_controller_DWork.HILInitialize1_AIMinimums[0] =
        PI_controller_P.HILInitialize1_AILow;
      PI_controller_DWork.HILInitialize1_AIMinimums[1] =
        PI_controller_P.HILInitialize1_AILow;
      PI_controller_DWork.HILInitialize1_AIMaximums[0] =
        PI_controller_P.HILInitialize1_AIHigh;
      PI_controller_DWork.HILInitialize1_AIMaximums[1] =
        PI_controller_P.HILInitialize1_AIHigh;
      result = hil_set_analog_input_ranges
        (PI_controller_DWork.HILInitialize1_Card,
         PI_controller_P.HILInitialize1_AIChannels, 2U,
         &PI_controller_DWork.HILInitialize1_AIMinimums[0],
         &PI_controller_DWork.HILInitialize1_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
        return;
      }
    }

    if ((PI_controller_P.HILInitialize1_AOPStart && !is_switching) ||
        (PI_controller_P.HILInitialize1_AOPEnter && is_switching)) {
      PI_controller_DWork.HILInitialize1_AOMinimums[0] =
        PI_controller_P.HILInitialize1_AOLow;
      PI_controller_DWork.HILInitialize1_AOMinimums[1] =
        PI_controller_P.HILInitialize1_AOLow;
      PI_controller_DWork.HILInitialize1_AOMaximums[0] =
        PI_controller_P.HILInitialize1_AOHigh;
      PI_controller_DWork.HILInitialize1_AOMaximums[1] =
        PI_controller_P.HILInitialize1_AOHigh;
      result = hil_set_analog_output_ranges
        (PI_controller_DWork.HILInitialize1_Card,
         PI_controller_P.HILInitialize1_AOChannels, 2U,
         &PI_controller_DWork.HILInitialize1_AOMinimums[0],
         &PI_controller_DWork.HILInitialize1_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
        return;
      }
    }

    if ((PI_controller_P.HILInitialize1_AOStart && !is_switching) ||
        (PI_controller_P.HILInitialize1_AOEnter && is_switching)) {
      PI_controller_DWork.HILInitialize1_AOVoltages[0] =
        PI_controller_P.HILInitialize1_AOInitial;
      PI_controller_DWork.HILInitialize1_AOVoltages[1] =
        PI_controller_P.HILInitialize1_AOInitial;
      result = hil_write_analog(PI_controller_DWork.HILInitialize1_Card,
        PI_controller_P.HILInitialize1_AOChannels, 2U,
        &PI_controller_DWork.HILInitialize1_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
        return;
      }
    }

    if (PI_controller_P.HILInitialize1_AOReset) {
      PI_controller_DWork.HILInitialize1_AOVoltages[0] =
        PI_controller_P.HILInitialize1_AOWatchdog;
      PI_controller_DWork.HILInitialize1_AOVoltages[1] =
        PI_controller_P.HILInitialize1_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (PI_controller_DWork.HILInitialize1_Card,
         PI_controller_P.HILInitialize1_AOChannels, 2U,
         &PI_controller_DWork.HILInitialize1_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
        return;
      }
    }

    if ((PI_controller_P.HILInitialize1_EIPStart && !is_switching) ||
        (PI_controller_P.HILInitialize1_EIPEnter && is_switching)) {
      PI_controller_DWork.HILInitialize1_QuadratureModes[0] =
        PI_controller_P.HILInitialize1_EIQuadrature;
      PI_controller_DWork.HILInitialize1_QuadratureModes[1] =
        PI_controller_P.HILInitialize1_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (PI_controller_DWork.HILInitialize1_Card,
         PI_controller_P.HILInitialize1_EIChannels, 2U,
         (t_encoder_quadrature_mode *)
         &PI_controller_DWork.HILInitialize1_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
        return;
      }
    }

    if ((PI_controller_P.HILInitialize1_EIStart && !is_switching) ||
        (PI_controller_P.HILInitialize1_EIEnter && is_switching)) {
      PI_controller_DWork.HILInitialize1_InitialEICounts[0] =
        PI_controller_P.HILInitialize1_EIInitial;
      PI_controller_DWork.HILInitialize1_InitialEICounts[1] =
        PI_controller_P.HILInitialize1_EIInitial;
      result = hil_set_encoder_counts(PI_controller_DWork.HILInitialize1_Card,
        PI_controller_P.HILInitialize1_EIChannels, 2U,
        &PI_controller_DWork.HILInitialize1_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<S4>/HIL Read Analog Timebase' */

  /* S-Function Block: PI_controller/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader
      (PI_controller_DWork.HILInitialize1_Card,
       PI_controller_P.HILReadAnalogTimebase_SamplesIn,
       PI_controller_P.HILReadAnalogTimebase_Channels, 2,
       &PI_controller_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PI_controller_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<S4>/High Pass Filter ' */
  PI_controller_X.HighPassFilter_CSTATE[0] = 0.0;
  PI_controller_X.HighPassFilter_CSTATE[1] = 0.0;

  /* InitializeConditions for Integrator: '<Root>/Integrator' */
  PI_controller_X.Integrator_CSTATE = PI_controller_P.Integrator_IC;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn' */
  PI_controller_X.TransferFcn_CSTATE[0] = 0.0;
  PI_controller_X.TransferFcn_CSTATE[1] = 0.0;
}

/* Model terminate function */
void PI_controller_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize1' */

  /* S-Function Block: PI_controller/HIL Initialize1 (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(PI_controller_DWork.HILInitialize1_Card);
    hil_monitor_stop_all(PI_controller_DWork.HILInitialize1_Card);
    is_switching = false;
    if ((PI_controller_P.HILInitialize1_AOTerminate && !is_switching) ||
        (PI_controller_P.HILInitialize1_AOExit && is_switching)) {
      PI_controller_DWork.HILInitialize1_AOVoltages[0] =
        PI_controller_P.HILInitialize1_AOFinal;
      PI_controller_DWork.HILInitialize1_AOVoltages[1] =
        PI_controller_P.HILInitialize1_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(PI_controller_DWork.HILInitialize1_Card,
        PI_controller_P.HILInitialize1_AOChannels, num_final_analog_outputs,
        &PI_controller_DWork.HILInitialize1_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PI_controller_M, _rt_error_message);
      }
    }

    hil_task_delete_all(PI_controller_DWork.HILInitialize1_Card);
    hil_monitor_delete_all(PI_controller_DWork.HILInitialize1_Card);
    hil_close(PI_controller_DWork.HILInitialize1_Card);
    PI_controller_DWork.HILInitialize1_Card = NULL;
  }
}
