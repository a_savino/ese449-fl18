/*
 * PhaseLead.c
 *
 * Code generation for model "PhaseLead".
 *
 * Model version              : 1.16
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Sat Nov 10 16:18:03 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "PhaseLead.h"
#include "PhaseLead_private.h"
#include "PhaseLead_dt.h"

/* Block signals (auto storage) */
BlockIO_PhaseLead PhaseLead_B;

/* Continuous states */
ContinuousStates_PhaseLead PhaseLead_X;

/* Block states (auto storage) */
D_Work_PhaseLead PhaseLead_DWork;

/* Real-time model */
RT_MODEL_PhaseLead PhaseLead_M_;
RT_MODEL_PhaseLead *const PhaseLead_M = &PhaseLead_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 6;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  PhaseLead_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void PhaseLead_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_PhaseLead1;
  real_T rtb_HILReadEncoder;
  real_T rtb_Gain;
  real_T rtb_Saturation1;
  if (rtmIsMajorTimeStep(PhaseLead_M)) {
    /* set solver stop time */
    if (!(PhaseLead_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&PhaseLead_M->solverInfo,
                            ((PhaseLead_M->Timing.clockTickH0 + 1) *
        PhaseLead_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&PhaseLead_M->solverInfo,
                            ((PhaseLead_M->Timing.clockTick0 + 1) *
        PhaseLead_M->Timing.stepSize0 + PhaseLead_M->Timing.clockTickH0 *
        PhaseLead_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(PhaseLead_M)) {
    PhaseLead_M->Timing.t[0] = rtsiGetT(&PhaseLead_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(PhaseLead_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<S2>/HIL Read Analog Timebase' */

    /* S-Function Block: PhaseLead/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog(PhaseLead_DWork.HILReadAnalogTimebase_Task,
        1, &PhaseLead_DWork.HILReadAnalogTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
      }

      rtb_HILReadAnalogTimebase_o1 =
        PhaseLead_DWork.HILReadAnalogTimebase_Buffer[0];
      rtb_HILReadEncoder = PhaseLead_DWork.HILReadAnalogTimebase_Buffer[1];
    }
  }

  /* SignalGenerator: '<Root>/Signal Generator1' */
  rtb_Gain = PhaseLead_P.SignalGenerator1_Frequency * PhaseLead_M->Timing.t[0];
  if (rtb_Gain - floor(rtb_Gain) >= 0.5) {
    PhaseLead_B.SignalGenerator1 = PhaseLead_P.SignalGenerator1_Amplitude;
  } else {
    PhaseLead_B.SignalGenerator1 = -PhaseLead_P.SignalGenerator1_Amplitude;
  }

  /* End of SignalGenerator: '<Root>/Signal Generator1' */

  /* TransferFcn: '<Root>/Transfer Fcn' */
  PhaseLead_B.TransferFcn = PhaseLead_P.TransferFcn_C[0]*
    PhaseLead_X.TransferFcn_CSTATE[0]
    + PhaseLead_P.TransferFcn_C[1]*PhaseLead_X.TransferFcn_CSTATE[1];
  if (rtmIsMajorTimeStep(PhaseLead_M)) {
    /* Gain: '<S2>/Tachometer Calibration (krpm//V)' */
    PhaseLead_B.TachometerCalibrationkrpmV =
      PhaseLead_P.TachometerCalibrationkrpmV_Gain * rtb_HILReadEncoder;

    /* Gain: '<S2>/Potentiometer calibration  (deg//V)' */
    PhaseLead_B.PotentiometercalibrationdegV =
      PhaseLead_P.PotentiometercalibrationdegV_Ga * rtb_HILReadAnalogTimebase_o1;
  }

  /* TransferFcn: '<S2>/High Pass Filter ' */
  PhaseLead_B.HighPassFilter = PhaseLead_P.HighPassFilter_C[0]*
    PhaseLead_X.HighPassFilter_CSTATE[0]
    + PhaseLead_P.HighPassFilter_C[1]*PhaseLead_X.HighPassFilter_CSTATE[1];
  if (rtmIsMajorTimeStep(PhaseLead_M)) {
  }

  /* Gain: '<Root>/Gain' */
  rtb_Gain = PhaseLead_P.Gain_Gain * PhaseLead_B.SignalGenerator1;

  /* Integrator: '<Root>/Integrator1' */
  PhaseLead_B.Integrator1 = PhaseLead_X.Integrator1_CSTATE;

  /* TransferFcn: '<Root>/Phase Lead1' */
  rtb_PhaseLead1 = PhaseLead_P.PhaseLead1_D*PhaseLead_B.Integrator1;
  rtb_PhaseLead1 += PhaseLead_P.PhaseLead1_C*PhaseLead_X.PhaseLead1_CSTATE;

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_PhaseLead1 >= PhaseLead_P.Saturation1_UpperSat) {
    rtb_Saturation1 = PhaseLead_P.Saturation1_UpperSat;
  } else if (rtb_PhaseLead1 <= PhaseLead_P.Saturation1_LowerSat) {
    rtb_Saturation1 = PhaseLead_P.Saturation1_LowerSat;
  } else {
    rtb_Saturation1 = rtb_PhaseLead1;
  }

  /* End of Saturate: '<Root>/Saturation1' */

  /* Gain: '<S1>/Slider Gain' incorporates:
   *  Sum: '<Root>/Sum2'
   */
  PhaseLead_B.SliderGain = (rtb_Gain - PhaseLead_B.HighPassFilter) *
    PhaseLead_P.SliderGain_Gain;
  if (rtmIsMajorTimeStep(PhaseLead_M)) {
    /* S-Function (hil_read_encoder_block): '<S2>/HIL Read Encoder' */

    /* S-Function Block: PhaseLead/Subsystem/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(PhaseLead_DWork.HILInitialize1_Card,
        &PhaseLead_P.HILReadEncoder_Channels, 1,
        &PhaseLead_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder = PhaseLead_DWork.HILReadEncoder_Buffer;
      }
    }

    /* Gain: '<S2>/Encoder Calibration (deg//count)' */
    PhaseLead_B.EncoderCalibrationdegcount =
      PhaseLead_P.EncoderCalibrationdegcount_Gain * rtb_HILReadEncoder;
  }

  /* Gain: '<S2>/Motor Calibration (V//V)' */
  PhaseLead_B.MotorCalibrationVV = PhaseLead_P.MotorCalibrationVV_Gain *
    rtb_Saturation1;
  if (rtmIsMajorTimeStep(PhaseLead_M)) {
    /* S-Function (hil_write_block): '<S2>/HIL Write' */

    /* S-Function Block: PhaseLead/Subsystem/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(PhaseLead_DWork.HILInitialize1_Card,
                         &PhaseLead_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &PhaseLead_B.MotorCalibrationVV,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
      }
    }
  }

  if (rtmIsMajorTimeStep(PhaseLead_M)) {
    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, PhaseLead_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(PhaseLead_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((PhaseLead_M->Timing.clockTick1+
                            PhaseLead_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(PhaseLead_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(PhaseLead_M)!=-1) &&
          !((rtmGetTFinal(PhaseLead_M)-(((PhaseLead_M->Timing.clockTick1+
               PhaseLead_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((PhaseLead_M->Timing.clockTick1+PhaseLead_M->Timing.clockTickH1*
               4294967296.0)) * 0.002) * (DBL_EPSILON))) {
        rtmSetErrorStatus(PhaseLead_M, "Simulation finished");
      }

      if (rtmGetStopRequested(PhaseLead_M)) {
        rtmSetErrorStatus(PhaseLead_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&PhaseLead_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++PhaseLead_M->Timing.clockTick0)) {
      ++PhaseLead_M->Timing.clockTickH0;
    }

    PhaseLead_M->Timing.t[0] = rtsiGetSolverStopTime(&PhaseLead_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      PhaseLead_M->Timing.clockTick1++;
      if (!PhaseLead_M->Timing.clockTick1) {
        PhaseLead_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void PhaseLead_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn' */
  {
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->TransferFcn_CSTATE[0] = PhaseLead_B.HighPassFilter;
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->TransferFcn_CSTATE[0] += (PhaseLead_P.TransferFcn_A[0])*
      PhaseLead_X.TransferFcn_CSTATE[0]
      + (PhaseLead_P.TransferFcn_A[1])*PhaseLead_X.TransferFcn_CSTATE[1];
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->TransferFcn_CSTATE[1]= PhaseLead_X.TransferFcn_CSTATE[0];
  }

  /* Derivatives for TransferFcn: '<S2>/High Pass Filter ' */
  {
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->HighPassFilter_CSTATE[0] = PhaseLead_B.EncoderCalibrationdegcount;
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->HighPassFilter_CSTATE[0] += (PhaseLead_P.HighPassFilter_A[0])*
      PhaseLead_X.HighPassFilter_CSTATE[0]
      + (PhaseLead_P.HighPassFilter_A[1])*PhaseLead_X.HighPassFilter_CSTATE[1];
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->HighPassFilter_CSTATE[1]= PhaseLead_X.HighPassFilter_CSTATE[0];
  }

  /* Derivatives for Integrator: '<Root>/Integrator1' */
  ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
    ->Integrator1_CSTATE = PhaseLead_B.SliderGain;

  /* Derivatives for TransferFcn: '<Root>/Phase Lead1' */
  {
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->PhaseLead1_CSTATE = PhaseLead_B.Integrator1;
    ((StateDerivatives_PhaseLead *) PhaseLead_M->ModelData.derivs)
      ->PhaseLead1_CSTATE += (PhaseLead_P.PhaseLead1_A)*
      PhaseLead_X.PhaseLead1_CSTATE;
  }
}

/* Model initialize function */
void PhaseLead_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)PhaseLead_M, 0,
                sizeof(RT_MODEL_PhaseLead));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&PhaseLead_M->solverInfo,
                          &PhaseLead_M->Timing.simTimeStep);
    rtsiSetTPtr(&PhaseLead_M->solverInfo, &rtmGetTPtr(PhaseLead_M));
    rtsiSetStepSizePtr(&PhaseLead_M->solverInfo, &PhaseLead_M->Timing.stepSize0);
    rtsiSetdXPtr(&PhaseLead_M->solverInfo, &PhaseLead_M->ModelData.derivs);
    rtsiSetContStatesPtr(&PhaseLead_M->solverInfo,
                         &PhaseLead_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&PhaseLead_M->solverInfo,
      &PhaseLead_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&PhaseLead_M->solverInfo, (&rtmGetErrorStatus
      (PhaseLead_M)));
    rtsiSetRTModelPtr(&PhaseLead_M->solverInfo, PhaseLead_M);
  }

  rtsiSetSimTimeStep(&PhaseLead_M->solverInfo, MAJOR_TIME_STEP);
  PhaseLead_M->ModelData.intgData.f[0] = PhaseLead_M->ModelData.odeF[0];
  PhaseLead_M->ModelData.contStates = ((real_T *) &PhaseLead_X);
  rtsiSetSolverData(&PhaseLead_M->solverInfo, (void *)
                    &PhaseLead_M->ModelData.intgData);
  rtsiSetSolverName(&PhaseLead_M->solverInfo,"ode1");
  rtmSetTPtr(PhaseLead_M, &PhaseLead_M->Timing.tArray[0]);
  rtmSetTFinal(PhaseLead_M, 4.5);
  PhaseLead_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  PhaseLead_M->Sizes.checksums[0] = (1948646745U);
  PhaseLead_M->Sizes.checksums[1] = (3250974592U);
  PhaseLead_M->Sizes.checksums[2] = (2423124759U);
  PhaseLead_M->Sizes.checksums[3] = (847036241U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    PhaseLead_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(PhaseLead_M->extModeInfo,
      &PhaseLead_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(PhaseLead_M->extModeInfo, PhaseLead_M->Sizes.checksums);
    rteiSetTPtr(PhaseLead_M->extModeInfo, rtmGetTPtr(PhaseLead_M));
  }

  /* block I/O */
  (void) memset(((void *) &PhaseLead_B), 0,
                sizeof(BlockIO_PhaseLead));

  /* states (continuous) */
  {
    (void) memset((void *)&PhaseLead_X, 0,
                  sizeof(ContinuousStates_PhaseLead));
  }

  /* states (dwork) */
  (void) memset((void *)&PhaseLead_DWork, 0,
                sizeof(D_Work_PhaseLead));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    PhaseLead_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize1' */

  /* S-Function Block: PhaseLead/HIL Initialize1 (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &PhaseLead_DWork.HILInitialize1_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options(PhaseLead_DWork.HILInitialize1_Card,
      "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(PhaseLead_DWork.HILInitialize1_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
      return;
    }

    if ((PhaseLead_P.HILInitialize1_AIPStart && !is_switching) ||
        (PhaseLead_P.HILInitialize1_AIPEnter && is_switching)) {
      PhaseLead_DWork.HILInitialize1_AIMinimums[0] =
        PhaseLead_P.HILInitialize1_AILow;
      PhaseLead_DWork.HILInitialize1_AIMinimums[1] =
        PhaseLead_P.HILInitialize1_AILow;
      PhaseLead_DWork.HILInitialize1_AIMaximums[0] =
        PhaseLead_P.HILInitialize1_AIHigh;
      PhaseLead_DWork.HILInitialize1_AIMaximums[1] =
        PhaseLead_P.HILInitialize1_AIHigh;
      result = hil_set_analog_input_ranges(PhaseLead_DWork.HILInitialize1_Card,
        PhaseLead_P.HILInitialize1_AIChannels, 2U,
        &PhaseLead_DWork.HILInitialize1_AIMinimums[0],
        &PhaseLead_DWork.HILInitialize1_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
        return;
      }
    }

    if ((PhaseLead_P.HILInitialize1_AOPStart && !is_switching) ||
        (PhaseLead_P.HILInitialize1_AOPEnter && is_switching)) {
      PhaseLead_DWork.HILInitialize1_AOMinimums[0] =
        PhaseLead_P.HILInitialize1_AOLow;
      PhaseLead_DWork.HILInitialize1_AOMinimums[1] =
        PhaseLead_P.HILInitialize1_AOLow;
      PhaseLead_DWork.HILInitialize1_AOMaximums[0] =
        PhaseLead_P.HILInitialize1_AOHigh;
      PhaseLead_DWork.HILInitialize1_AOMaximums[1] =
        PhaseLead_P.HILInitialize1_AOHigh;
      result = hil_set_analog_output_ranges(PhaseLead_DWork.HILInitialize1_Card,
        PhaseLead_P.HILInitialize1_AOChannels, 2U,
        &PhaseLead_DWork.HILInitialize1_AOMinimums[0],
        &PhaseLead_DWork.HILInitialize1_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
        return;
      }
    }

    if ((PhaseLead_P.HILInitialize1_AOStart && !is_switching) ||
        (PhaseLead_P.HILInitialize1_AOEnter && is_switching)) {
      PhaseLead_DWork.HILInitialize1_AOVoltages[0] =
        PhaseLead_P.HILInitialize1_AOInitial;
      PhaseLead_DWork.HILInitialize1_AOVoltages[1] =
        PhaseLead_P.HILInitialize1_AOInitial;
      result = hil_write_analog(PhaseLead_DWork.HILInitialize1_Card,
        PhaseLead_P.HILInitialize1_AOChannels, 2U,
        &PhaseLead_DWork.HILInitialize1_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
        return;
      }
    }

    if (PhaseLead_P.HILInitialize1_AOReset) {
      PhaseLead_DWork.HILInitialize1_AOVoltages[0] =
        PhaseLead_P.HILInitialize1_AOWatchdog;
      PhaseLead_DWork.HILInitialize1_AOVoltages[1] =
        PhaseLead_P.HILInitialize1_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (PhaseLead_DWork.HILInitialize1_Card,
         PhaseLead_P.HILInitialize1_AOChannels, 2U,
         &PhaseLead_DWork.HILInitialize1_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
        return;
      }
    }

    if ((PhaseLead_P.HILInitialize1_EIPStart && !is_switching) ||
        (PhaseLead_P.HILInitialize1_EIPEnter && is_switching)) {
      PhaseLead_DWork.HILInitialize1_QuadratureModes[0] =
        PhaseLead_P.HILInitialize1_EIQuadrature;
      PhaseLead_DWork.HILInitialize1_QuadratureModes[1] =
        PhaseLead_P.HILInitialize1_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (PhaseLead_DWork.HILInitialize1_Card,
         PhaseLead_P.HILInitialize1_EIChannels, 2U, (t_encoder_quadrature_mode *)
         &PhaseLead_DWork.HILInitialize1_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
        return;
      }
    }

    if ((PhaseLead_P.HILInitialize1_EIStart && !is_switching) ||
        (PhaseLead_P.HILInitialize1_EIEnter && is_switching)) {
      PhaseLead_DWork.HILInitialize1_InitialEICounts[0] =
        PhaseLead_P.HILInitialize1_EIInitial;
      PhaseLead_DWork.HILInitialize1_InitialEICounts[1] =
        PhaseLead_P.HILInitialize1_EIInitial;
      result = hil_set_encoder_counts(PhaseLead_DWork.HILInitialize1_Card,
        PhaseLead_P.HILInitialize1_EIChannels, 2U,
        &PhaseLead_DWork.HILInitialize1_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<S2>/HIL Read Analog Timebase' */

  /* S-Function Block: PhaseLead/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader(PhaseLead_DWork.HILInitialize1_Card,
      PhaseLead_P.HILReadAnalogTimebase_SamplesIn,
      PhaseLead_P.HILReadAnalogTimebase_Channels, 2,
      &PhaseLead_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn' */
  PhaseLead_X.TransferFcn_CSTATE[0] = 0.0;
  PhaseLead_X.TransferFcn_CSTATE[1] = 0.0;

  /* InitializeConditions for TransferFcn: '<S2>/High Pass Filter ' */
  PhaseLead_X.HighPassFilter_CSTATE[0] = 0.0;
  PhaseLead_X.HighPassFilter_CSTATE[1] = 0.0;

  /* InitializeConditions for Integrator: '<Root>/Integrator1' */
  PhaseLead_X.Integrator1_CSTATE = PhaseLead_P.Integrator1_IC;

  /* InitializeConditions for TransferFcn: '<Root>/Phase Lead1' */
  PhaseLead_X.PhaseLead1_CSTATE = 0.0;
}

/* Model terminate function */
void PhaseLead_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize1' */

  /* S-Function Block: PhaseLead/HIL Initialize1 (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(PhaseLead_DWork.HILInitialize1_Card);
    hil_monitor_stop_all(PhaseLead_DWork.HILInitialize1_Card);
    is_switching = false;
    if ((PhaseLead_P.HILInitialize1_AOTerminate && !is_switching) ||
        (PhaseLead_P.HILInitialize1_AOExit && is_switching)) {
      PhaseLead_DWork.HILInitialize1_AOVoltages[0] =
        PhaseLead_P.HILInitialize1_AOFinal;
      PhaseLead_DWork.HILInitialize1_AOVoltages[1] =
        PhaseLead_P.HILInitialize1_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(PhaseLead_DWork.HILInitialize1_Card,
        PhaseLead_P.HILInitialize1_AOChannels, num_final_analog_outputs,
        &PhaseLead_DWork.HILInitialize1_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PhaseLead_M, _rt_error_message);
      }
    }

    hil_task_delete_all(PhaseLead_DWork.HILInitialize1_Card);
    hil_monitor_delete_all(PhaseLead_DWork.HILInitialize1_Card);
    hil_close(PhaseLead_DWork.HILInitialize1_Card);
    PhaseLead_DWork.HILInitialize1_Card = NULL;
  }
}
