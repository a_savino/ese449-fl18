/*
 * PhaseLead.h
 *
 * Code generation for model "PhaseLead".
 *
 * Model version              : 1.15
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Sat Nov 10 16:09:46 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_PhaseLead_h_
#define RTW_HEADER_PhaseLead_h_
#ifndef PhaseLead_COMMON_INCLUDES_
# define PhaseLead_COMMON_INCLUDES_
#include <float.h>
#include <math.h>
#include <string.h>
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "hil.h"
#include "quanser_messages.h"
#include "quanser_extern.h"
#endif                                 /* PhaseLead_COMMON_INCLUDES_ */

#include "PhaseLead_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T SignalGenerator1;             /* '<Root>/Signal Generator1' */
  real_T TachometerCalibrationkrpmV;   /* '<S2>/Tachometer Calibration (krpm//V)' */
  real_T PotentiometercalibrationdegV; /* '<S2>/Potentiometer calibration  (deg//V)' */
  real_T HighPassFilter;               /* '<S2>/High Pass Filter ' */
  real_T Integrator1;                  /* '<Root>/Integrator1' */
  real_T SliderGain;                   /* '<S1>/Slider Gain' */
  real_T EncoderCalibrationdegcount;   /* '<S2>/Encoder Calibration (deg//count)' */
  real_T MotorCalibrationVV;           /* '<S2>/Motor Calibration (V//V)' */
} BlockIO_PhaseLead;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T HILInitialize1_AIMinimums[2]; /* '<Root>/HIL Initialize1' */
  real_T HILInitialize1_AIMaximums[2]; /* '<Root>/HIL Initialize1' */
  real_T HILInitialize1_AOMinimums[2]; /* '<Root>/HIL Initialize1' */
  real_T HILInitialize1_AOMaximums[2]; /* '<Root>/HIL Initialize1' */
  real_T HILInitialize1_AOVoltages[2]; /* '<Root>/HIL Initialize1' */
  real_T HILInitialize1_FilterFrequency[2];/* '<Root>/HIL Initialize1' */
  real_T HILReadAnalogTimebase_Buffer[2];/* '<S2>/HIL Read Analog Timebase' */
  t_task HILReadAnalogTimebase_Task;   /* '<S2>/HIL Read Analog Timebase' */
  t_card HILInitialize1_Card;          /* '<Root>/HIL Initialize1' */
  struct {
    void *LoggedData;
  } Output_PWORK;                      /* '<Root>/Output' */

  struct {
    void *LoggedData;
  } W_Irpm_PWORK;                      /* '<Root>/W_I (rpm)' */

  struct {
    void *LoggedData;
  } theta_Ideg_PWORK;                  /* '<Root>/theta_I (deg)' */

  struct {
    void *LoggedData;
  } theta_Ideg1_PWORK;                 /* '<Root>/theta_I (deg)1' */

  void *HILReadEncoder_PWORK;          /* '<S2>/HIL Read Encoder' */
  void *HILWrite_PWORK;                /* '<S2>/HIL Write' */
  int32_T HILInitialize1_ClockModes;   /* '<Root>/HIL Initialize1' */
  int32_T HILInitialize1_QuadratureModes[2];/* '<Root>/HIL Initialize1' */
  int32_T HILInitialize1_InitialEICounts[2];/* '<Root>/HIL Initialize1' */
  int32_T HILReadEncoder_Buffer;       /* '<S2>/HIL Read Encoder' */
} D_Work_PhaseLead;

/* Continuous states (auto storage) */
typedef struct {
  real_T HighPassFilter_CSTATE[2];     /* '<S2>/High Pass Filter ' */
  real_T Integrator1_CSTATE;           /* '<Root>/Integrator1' */
  real_T PhaseLead1_CSTATE;            /* '<Root>/Phase Lead1' */
} ContinuousStates_PhaseLead;

/* State derivatives (auto storage) */
typedef struct {
  real_T HighPassFilter_CSTATE[2];     /* '<S2>/High Pass Filter ' */
  real_T Integrator1_CSTATE;           /* '<Root>/Integrator1' */
  real_T PhaseLead1_CSTATE;            /* '<Root>/Phase Lead1' */
} StateDerivatives_PhaseLead;

/* State disabled  */
typedef struct {
  boolean_T HighPassFilter_CSTATE[2];  /* '<S2>/High Pass Filter ' */
  boolean_T Integrator1_CSTATE;        /* '<Root>/Integrator1' */
  boolean_T PhaseLead1_CSTATE;         /* '<Root>/Phase Lead1' */
} StateDisabled_PhaseLead;

#ifndef ODE1_INTG
#define ODE1_INTG

/* ODE1 Integration Data */
typedef struct {
  real_T *f[1];                        /* derivatives */
} ODE1_IntgData;

#endif

/* Parameters (auto storage) */
struct Parameters_PhaseLead_ {
  real_T HILInitialize1_OOStart;       /* Expression: set_other_outputs_at_start
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_OOEnter;       /* Expression: set_other_outputs_at_switch_in
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_OOTerminate;   /* Expression: set_other_outputs_at_terminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_OOExit;        /* Expression: set_other_outputs_at_switch_out
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_AIHigh;        /* Expression: analog_input_maximums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_AILow;         /* Expression: analog_input_minimums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_AOHigh;        /* Expression: analog_output_maximums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_AOLow;         /* Expression: analog_output_minimums
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_AOInitial;     /* Expression: initial_analog_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_AOFinal;       /* Expression: final_analog_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_AOWatchdog;    /* Expression: watchdog_analog_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_POFrequency;   /* Expression: pwm_frequency
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_POInitial;     /* Expression: initial_pwm_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_POFinal;       /* Expression: final_pwm_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T HILInitialize1_POWatchdog;    /* Expression: watchdog_pwm_outputs
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  real_T SignalGenerator1_Amplitude;   /* Expression: 1
                                        * Referenced by: '<Root>/Signal Generator1'
                                        */
  real_T SignalGenerator1_Frequency;   /* Expression: 0.4
                                        * Referenced by: '<Root>/Signal Generator1'
                                        */
  real_T TachometerCalibrationkrpmV_Gain;/* Expression: 0.997
                                          * Referenced by: '<S2>/Tachometer Calibration (krpm//V)'
                                          */
  real_T PotentiometercalibrationdegV_Ga;/* Expression: 352/10
                                          * Referenced by: '<S2>/Potentiometer calibration  (deg//V)'
                                          */
  real_T HighPassFilter_A[2];          /* Computed Parameter: HighPassFilter_A
                                        * Referenced by: '<S2>/High Pass Filter '
                                        */
  real_T HighPassFilter_C[2];          /* Computed Parameter: HighPassFilter_C
                                        * Referenced by: '<S2>/High Pass Filter '
                                        */
  real_T Gain_Gain;                    /* Expression: 1
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T Integrator1_IC;               /* Expression: 0
                                        * Referenced by: '<Root>/Integrator1'
                                        */
  real_T PhaseLead1_A;                 /* Computed Parameter: PhaseLead1_A
                                        * Referenced by: '<Root>/Phase Lead1'
                                        */
  real_T PhaseLead1_C;                 /* Computed Parameter: PhaseLead1_C
                                        * Referenced by: '<Root>/Phase Lead1'
                                        */
  real_T PhaseLead1_D;                 /* Computed Parameter: PhaseLead1_D
                                        * Referenced by: '<Root>/Phase Lead1'
                                        */
  real_T Saturation1_UpperSat;         /* Expression: 10
                                        * Referenced by: '<Root>/Saturation1'
                                        */
  real_T Saturation1_LowerSat;         /* Expression: -10
                                        * Referenced by: '<Root>/Saturation1'
                                        */
  real_T SliderGain_Gain;              /* Expression: gain
                                        * Referenced by: '<S1>/Slider Gain'
                                        */
  real_T EncoderCalibrationdegcount_Gain;/* Expression: -0.0015
                                          * Referenced by: '<S2>/Encoder Calibration (deg//count)'
                                          */
  real_T MotorCalibrationVV_Gain;      /* Expression: 1
                                        * Referenced by: '<S2>/Motor Calibration (V//V)'
                                        */
  int32_T HILInitialize1_CKChannels;   /* Computed Parameter: HILInitialize1_CKChannels
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  int32_T HILInitialize1_DOWatchdog;   /* Computed Parameter: HILInitialize1_DOWatchdog
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  int32_T HILInitialize1_EIInitial;    /* Computed Parameter: HILInitialize1_EIInitial
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  int32_T HILInitialize1_POModes;      /* Computed Parameter: HILInitialize1_POModes
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  int32_T HILReadAnalogTimebase_Clock; /* Computed Parameter: HILReadAnalogTimebase_Clock
                                        * Referenced by: '<S2>/HIL Read Analog Timebase'
                                        */
  uint32_T HILInitialize1_AIChannels[2];/* Computed Parameter: HILInitialize1_AIChannels
                                         * Referenced by: '<Root>/HIL Initialize1'
                                         */
  uint32_T HILInitialize1_AOChannels[2];/* Computed Parameter: HILInitialize1_AOChannels
                                         * Referenced by: '<Root>/HIL Initialize1'
                                         */
  uint32_T HILInitialize1_EIChannels[2];/* Computed Parameter: HILInitialize1_EIChannels
                                         * Referenced by: '<Root>/HIL Initialize1'
                                         */
  uint32_T HILInitialize1_EIQuadrature;/* Computed Parameter: HILInitialize1_EIQuadrature
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  uint32_T HILReadAnalogTimebase_Channels[2];/* Computed Parameter: HILReadAnalogTimebase_Channels
                                              * Referenced by: '<S2>/HIL Read Analog Timebase'
                                              */
  uint32_T HILReadAnalogTimebase_SamplesIn;/* Computed Parameter: HILReadAnalogTimebase_SamplesIn
                                            * Referenced by: '<S2>/HIL Read Analog Timebase'
                                            */
  uint32_T HILReadEncoder_Channels;    /* Computed Parameter: HILReadEncoder_Channels
                                        * Referenced by: '<S2>/HIL Read Encoder'
                                        */
  uint32_T HILWrite_AnalogChannels;    /* Computed Parameter: HILWrite_AnalogChannels
                                        * Referenced by: '<S2>/HIL Write'
                                        */
  boolean_T HILInitialize1_Active;     /* Computed Parameter: HILInitialize1_Active
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_CKPStart;   /* Computed Parameter: HILInitialize1_CKPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_CKPEnter;   /* Computed Parameter: HILInitialize1_CKPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_CKStart;    /* Computed Parameter: HILInitialize1_CKStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_CKEnter;    /* Computed Parameter: HILInitialize1_CKEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AIPStart;   /* Computed Parameter: HILInitialize1_AIPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AIPEnter;   /* Computed Parameter: HILInitialize1_AIPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AOPStart;   /* Computed Parameter: HILInitialize1_AOPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AOPEnter;   /* Computed Parameter: HILInitialize1_AOPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AOStart;    /* Computed Parameter: HILInitialize1_AOStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AOEnter;    /* Computed Parameter: HILInitialize1_AOEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AOTerminate;/* Computed Parameter: HILInitialize1_AOTerminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AOExit;     /* Computed Parameter: HILInitialize1_AOExit
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_AOReset;    /* Computed Parameter: HILInitialize1_AOReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOPStart;   /* Computed Parameter: HILInitialize1_DOPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOPEnter;   /* Computed Parameter: HILInitialize1_DOPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOStart;    /* Computed Parameter: HILInitialize1_DOStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOEnter;    /* Computed Parameter: HILInitialize1_DOEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOTerminate;/* Computed Parameter: HILInitialize1_DOTerminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOExit;     /* Computed Parameter: HILInitialize1_DOExit
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOReset;    /* Computed Parameter: HILInitialize1_DOReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_EIPStart;   /* Computed Parameter: HILInitialize1_EIPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_EIPEnter;   /* Computed Parameter: HILInitialize1_EIPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_EIStart;    /* Computed Parameter: HILInitialize1_EIStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_EIEnter;    /* Computed Parameter: HILInitialize1_EIEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_POPStart;   /* Computed Parameter: HILInitialize1_POPStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_POPEnter;   /* Computed Parameter: HILInitialize1_POPEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_POStart;    /* Computed Parameter: HILInitialize1_POStart
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_POEnter;    /* Computed Parameter: HILInitialize1_POEnter
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_POTerminate;/* Computed Parameter: HILInitialize1_POTerminate
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_POExit;     /* Computed Parameter: HILInitialize1_POExit
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_POReset;    /* Computed Parameter: HILInitialize1_POReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_OOReset;    /* Computed Parameter: HILInitialize1_OOReset
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOInitial;  /* Computed Parameter: HILInitialize1_DOInitial
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_DOFinal;    /* Computed Parameter: HILInitialize1_DOFinal
                                        * Referenced by: '<Root>/HIL Initialize1'
                                        */
  boolean_T HILReadAnalogTimebase_Active;/* Computed Parameter: HILReadAnalogTimebase_Active
                                          * Referenced by: '<S2>/HIL Read Analog Timebase'
                                          */
  boolean_T HILReadEncoder_Active;     /* Computed Parameter: HILReadEncoder_Active
                                        * Referenced by: '<S2>/HIL Read Encoder'
                                        */
  boolean_T HILWrite_Active;           /* Computed Parameter: HILWrite_Active
                                        * Referenced by: '<S2>/HIL Write'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_PhaseLead {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;
  RTWSolverInfo solverInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    real_T *contStates;
    real_T *derivs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    real_T odeF[1][4];
    ODE1_IntgData intgData;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
    int_T numContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T tFinal;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block parameters (auto storage) */
extern Parameters_PhaseLead PhaseLead_P;

/* Block signals (auto storage) */
extern BlockIO_PhaseLead PhaseLead_B;

/* Continuous states (auto storage) */
extern ContinuousStates_PhaseLead PhaseLead_X;

/* Block states (auto storage) */
extern D_Work_PhaseLead PhaseLead_DWork;

/* Model entry point functions */
extern void PhaseLead_initialize(void);
extern void PhaseLead_step(void);
extern void PhaseLead_terminate(void);

/* Real-time Model object */
extern RT_MODEL_PhaseLead *const PhaseLead_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'PhaseLead'
 * '<S1>'   : 'PhaseLead/Slider Gain'
 * '<S2>'   : 'PhaseLead/Subsystem'
 */
#endif                                 /* RTW_HEADER_PhaseLead_h_ */
