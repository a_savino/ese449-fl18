/*
 * PhaseLead_types.h
 *
 * Code generation for model "PhaseLead".
 *
 * Model version              : 1.15
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Sat Nov 10 16:09:46 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_PhaseLead_types_h_
#define RTW_HEADER_PhaseLead_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_PhaseLead_ Parameters_PhaseLead;

/* Forward declaration for rtModel */
typedef struct tag_RTM_PhaseLead RT_MODEL_PhaseLead;

#endif                                 /* RTW_HEADER_PhaseLead_types_h_ */
