/*
 * PVController_types.h
 *
 * Code generation for model "PVController".
 *
 * Model version              : 1.14
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Wed Oct 10 15:56:31 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_PVController_types_h_
#define RTW_HEADER_PVController_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_PVController_ Parameters_PVController;

/* Forward declaration for rtModel */
typedef struct tag_RTM_PVController RT_MODEL_PVController;

#endif                                 /* RTW_HEADER_PVController_types_h_ */
