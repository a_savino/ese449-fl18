wn = 21.7;
tau = .03;
K = 1.6263;
z = .69;

%% calculate PV controller constants
Kp = wn^2 * tau / K;
Kv = (2*z*wn*tau -1) /K;

%% calculate PIV controller constants
V_max = 10;
ti = 1;
R_slope = 3.36;
ess = R_slope * (1 + K*Kv)/(K*Kp);
Ki = (V_max - Kp*ess ) / (ti * ess);
