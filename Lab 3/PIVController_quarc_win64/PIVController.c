/*
 * PIVController.c
 *
 * Code generation for model "PIVController".
 *
 * Model version              : 1.13
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Wed Oct 10 16:24:07 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "PIVController.h"
#include "PIVController_private.h"
#include "PIVController_dt.h"

/* Block signals (auto storage) */
BlockIO_PIVController PIVController_B;

/* Continuous states */
ContinuousStates_PIVController PIVController_X;

/* Block states (auto storage) */
D_Work_PIVController PIVController_DWork;

/* Real-time model */
RT_MODEL_PIVController PIVController_M_;
RT_MODEL_PIVController *const PIVController_M = &PIVController_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 1;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  PIVController_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void PIVController_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_HILReadAnalogTimebase_o2;
  real_T rtb_HILReadEncoder;
  real_T rtb_Derivative1;
  real_T rtb_Sum3;
  real_T rtb_Kp1;
  real_T rtb_Kv;
  if (rtmIsMajorTimeStep(PIVController_M)) {
    /* set solver stop time */
    if (!(PIVController_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&PIVController_M->solverInfo,
                            ((PIVController_M->Timing.clockTickH0 + 1) *
        PIVController_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&PIVController_M->solverInfo,
                            ((PIVController_M->Timing.clockTick0 + 1) *
        PIVController_M->Timing.stepSize0 + PIVController_M->Timing.clockTickH0 *
        PIVController_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(PIVController_M)) {
    PIVController_M->Timing.t[0] = rtsiGetT(&PIVController_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(PIVController_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

    /* S-Function Block: PIVController/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog
        (PIVController_DWork.HILReadAnalogTimebase_Task, 1,
         &PIVController_DWork.HILReadAnalogTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
      }

      rtb_HILReadAnalogTimebase_o1 =
        PIVController_DWork.HILReadAnalogTimebase_Buffer[0];
      rtb_HILReadAnalogTimebase_o2 =
        PIVController_DWork.HILReadAnalogTimebase_Buffer[1];
    }
  }

  /* SignalGenerator: '<Root>/Signal Generator' */
  rtb_Kp1 = PIVController_P.SignalGenerator_Frequency *
    PIVController_M->Timing.t[0];
  PIVController_B.SignalGenerator = (1.0 - (rtb_Kp1 - floor(rtb_Kp1)) * 2.0) *
    PIVController_P.SignalGenerator_Amplitude;
  if (rtmIsMajorTimeStep(PIVController_M)) {
    /* S-Function (hil_read_encoder_block): '<S1>/HIL Read Encoder' */

    /* S-Function Block: PIVController/Subsystem/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(PIVController_DWork.HILInitialize_Card,
        &PIVController_P.HILReadEncoder_Channels, 1,
        &PIVController_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder = PIVController_DWork.HILReadEncoder_Buffer;
      }
    }

    /* Gain: '<S1>/Encoder Calibration (deg//count)' */
    PIVController_B.EncoderCalibrationdegcount =
      PIVController_P.EncoderCalibrationdegcount_Gain * rtb_HILReadEncoder;

    /* Gain: '<S1>/Tachometer Calibration (krpm//V)' */
    PIVController_B.TachometerCalibrationkrpmV =
      PIVController_P.TachometerCalibrationkrpmV_Gain *
      rtb_HILReadAnalogTimebase_o2;

    /* Gain: '<S1>/Potentiometer calibration  (deg//V)' */
    PIVController_B.PotentiometercalibrationdegV =
      PIVController_P.PotentiometercalibrationdegV_Ga *
      rtb_HILReadAnalogTimebase_o1;
  }

  /* Derivative: '<Root>/Derivative1' */
  {
    real_T t = PIVController_M->Timing.t[0];
    real_T timeStampA = PIVController_DWork.Derivative1_RWORK.TimeStampA;
    real_T timeStampB = PIVController_DWork.Derivative1_RWORK.TimeStampB;
    real_T *lastU = &PIVController_DWork.Derivative1_RWORK.LastUAtTimeA;
    if (timeStampA >= t && timeStampB >= t) {
      rtb_Derivative1 = 0.0;
    } else {
      real_T deltaT;
      real_T lastTime = timeStampA;
      if (timeStampA < timeStampB) {
        if (timeStampB < t) {
          lastTime = timeStampB;
          lastU = &PIVController_DWork.Derivative1_RWORK.LastUAtTimeB;
        }
      } else if (timeStampA >= t) {
        lastTime = timeStampB;
        lastU = &PIVController_DWork.Derivative1_RWORK.LastUAtTimeB;
      }

      deltaT = t - lastTime;
      rtb_Derivative1 = (PIVController_B.EncoderCalibrationdegcount - *lastU++) /
        deltaT;
    }
  }

  /* Sum: '<Root>/Sum1' incorporates:
   *  Gain: '<Root>/Gain'
   */
  rtb_Sum3 = PIVController_P.Gain_Gain * PIVController_B.SignalGenerator -
    PIVController_B.EncoderCalibrationdegcount;

  /* Gain: '<Root>/Ki' */
  PIVController_B.Ki = PIVController_P.Ki_Gain * rtb_Sum3;

  /* Gain: '<Root>/Kp1' */
  rtb_Kp1 = PIVController_P.Kp1_Gain * rtb_Sum3;

  /* Gain: '<Root>/Kv' incorporates:
   *  Gain: '<Root>/Kv3'
   */
  rtb_Kv = PIVController_P.Kv3_Gain * rtb_Derivative1 * PIVController_P.Kv_Gain;

  /* TransferFcn: '<Root>/Transfer Fcn' */
  rtb_Sum3 = PIVController_P.TransferFcn_C*PIVController_X.TransferFcn_CSTATE;

  /* Sum: '<Root>/Sum3' */
  rtb_Sum3 = (rtb_Kp1 + rtb_Sum3) + rtb_Kv;

  /* Saturate: '<Root>/Saturation1' */
  if (rtb_Sum3 >= PIVController_P.Saturation1_UpperSat) {
    rtb_Kp1 = PIVController_P.Saturation1_UpperSat;
  } else if (rtb_Sum3 <= PIVController_P.Saturation1_LowerSat) {
    rtb_Kp1 = PIVController_P.Saturation1_LowerSat;
  } else {
    rtb_Kp1 = rtb_Sum3;
  }

  /* Gain: '<S1>/Motor Calibration (V//V)' incorporates:
   *  Saturate: '<Root>/Saturation1'
   */
  PIVController_B.MotorCalibrationVV = PIVController_P.MotorCalibrationVV_Gain *
    rtb_Kp1;
  if (rtmIsMajorTimeStep(PIVController_M)) {
    /* S-Function (hil_write_block): '<S1>/HIL Write' */

    /* S-Function Block: PIVController/Subsystem/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(PIVController_DWork.HILInitialize_Card,
                         &PIVController_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &PIVController_B.MotorCalibrationVV,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
      }
    }
  }

  if (rtmIsMajorTimeStep(PIVController_M)) {
    /* Update for Derivative: '<Root>/Derivative1' */
    {
      real_T timeStampA = PIVController_DWork.Derivative1_RWORK.TimeStampA;
      real_T timeStampB = PIVController_DWork.Derivative1_RWORK.TimeStampB;
      real_T* lastTime = &PIVController_DWork.Derivative1_RWORK.TimeStampA;
      real_T* lastU = &PIVController_DWork.Derivative1_RWORK.LastUAtTimeA;
      if (timeStampA != rtInf) {
        if (timeStampB == rtInf) {
          lastTime = &PIVController_DWork.Derivative1_RWORK.TimeStampB;
          lastU = &PIVController_DWork.Derivative1_RWORK.LastUAtTimeB;
        } else if (timeStampA >= timeStampB) {
          lastTime = &PIVController_DWork.Derivative1_RWORK.TimeStampB;
          lastU = &PIVController_DWork.Derivative1_RWORK.LastUAtTimeB;
        }
      }

      *lastTime = PIVController_M->Timing.t[0];
      *lastU++ = PIVController_B.EncoderCalibrationdegcount;
    }

    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, PIVController_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(PIVController_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((PIVController_M->Timing.clockTick1+
                            PIVController_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(PIVController_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(PIVController_M)!=-1) &&
          !((rtmGetTFinal(PIVController_M)-(((PIVController_M->Timing.clockTick1
               +PIVController_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((PIVController_M->Timing.clockTick1+
               PIVController_M->Timing.clockTickH1* 4294967296.0)) * 0.002) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(PIVController_M, "Simulation finished");
      }

      if (rtmGetStopRequested(PIVController_M)) {
        rtmSetErrorStatus(PIVController_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&PIVController_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++PIVController_M->Timing.clockTick0)) {
      ++PIVController_M->Timing.clockTickH0;
    }

    PIVController_M->Timing.t[0] = rtsiGetSolverStopTime
      (&PIVController_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      PIVController_M->Timing.clockTick1++;
      if (!PIVController_M->Timing.clockTick1) {
        PIVController_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void PIVController_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn' */
  {
    ((StateDerivatives_PIVController *) PIVController_M->ModelData.derivs)
      ->TransferFcn_CSTATE = PIVController_B.Ki;
    ((StateDerivatives_PIVController *) PIVController_M->ModelData.derivs)
      ->TransferFcn_CSTATE += (PIVController_P.TransferFcn_A)*
      PIVController_X.TransferFcn_CSTATE;
  }
}

/* Model initialize function */
void PIVController_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)PIVController_M, 0,
                sizeof(RT_MODEL_PIVController));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&PIVController_M->solverInfo,
                          &PIVController_M->Timing.simTimeStep);
    rtsiSetTPtr(&PIVController_M->solverInfo, &rtmGetTPtr(PIVController_M));
    rtsiSetStepSizePtr(&PIVController_M->solverInfo,
                       &PIVController_M->Timing.stepSize0);
    rtsiSetdXPtr(&PIVController_M->solverInfo,
                 &PIVController_M->ModelData.derivs);
    rtsiSetContStatesPtr(&PIVController_M->solverInfo,
                         &PIVController_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&PIVController_M->solverInfo,
      &PIVController_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&PIVController_M->solverInfo, (&rtmGetErrorStatus
      (PIVController_M)));
    rtsiSetRTModelPtr(&PIVController_M->solverInfo, PIVController_M);
  }

  rtsiSetSimTimeStep(&PIVController_M->solverInfo, MAJOR_TIME_STEP);
  PIVController_M->ModelData.intgData.f[0] = PIVController_M->ModelData.odeF[0];
  PIVController_M->ModelData.contStates = ((real_T *) &PIVController_X);
  rtsiSetSolverData(&PIVController_M->solverInfo, (void *)
                    &PIVController_M->ModelData.intgData);
  rtsiSetSolverName(&PIVController_M->solverInfo,"ode1");
  rtmSetTPtr(PIVController_M, &PIVController_M->Timing.tArray[0]);
  rtmSetTFinal(PIVController_M, 8.0);
  PIVController_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  PIVController_M->Sizes.checksums[0] = (760815482U);
  PIVController_M->Sizes.checksums[1] = (3980539443U);
  PIVController_M->Sizes.checksums[2] = (2192607583U);
  PIVController_M->Sizes.checksums[3] = (12587911U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    PIVController_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(PIVController_M->extModeInfo,
      &PIVController_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(PIVController_M->extModeInfo,
                        PIVController_M->Sizes.checksums);
    rteiSetTPtr(PIVController_M->extModeInfo, rtmGetTPtr(PIVController_M));
  }

  /* block I/O */
  (void) memset(((void *) &PIVController_B), 0,
                sizeof(BlockIO_PIVController));

  /* states (continuous) */
  {
    (void) memset((void *)&PIVController_X, 0,
                  sizeof(ContinuousStates_PIVController));
  }

  /* states (dwork) */
  (void) memset((void *)&PIVController_DWork, 0,
                sizeof(D_Work_PIVController));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    PIVController_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: PIVController/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &PIVController_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PIVController_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options
      (PIVController_DWork.HILInitialize_Card,
       "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PIVController_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(PIVController_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PIVController_M, _rt_error_message);
      return;
    }

    if ((PIVController_P.HILInitialize_AIPStart && !is_switching) ||
        (PIVController_P.HILInitialize_AIPEnter && is_switching)) {
      PIVController_DWork.HILInitialize_AIMinimums[0] =
        PIVController_P.HILInitialize_AILow;
      PIVController_DWork.HILInitialize_AIMinimums[1] =
        PIVController_P.HILInitialize_AILow;
      PIVController_DWork.HILInitialize_AIMaximums[0] =
        PIVController_P.HILInitialize_AIHigh;
      PIVController_DWork.HILInitialize_AIMaximums[1] =
        PIVController_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges
        (PIVController_DWork.HILInitialize_Card,
         PIVController_P.HILInitialize_AIChannels, 2U,
         &PIVController_DWork.HILInitialize_AIMinimums[0],
         &PIVController_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
        return;
      }
    }

    if ((PIVController_P.HILInitialize_AOPStart && !is_switching) ||
        (PIVController_P.HILInitialize_AOPEnter && is_switching)) {
      PIVController_DWork.HILInitialize_AOMinimums[0] =
        PIVController_P.HILInitialize_AOLow;
      PIVController_DWork.HILInitialize_AOMinimums[1] =
        PIVController_P.HILInitialize_AOLow;
      PIVController_DWork.HILInitialize_AOMaximums[0] =
        PIVController_P.HILInitialize_AOHigh;
      PIVController_DWork.HILInitialize_AOMaximums[1] =
        PIVController_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges
        (PIVController_DWork.HILInitialize_Card,
         PIVController_P.HILInitialize_AOChannels, 2U,
         &PIVController_DWork.HILInitialize_AOMinimums[0],
         &PIVController_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
        return;
      }
    }

    if ((PIVController_P.HILInitialize_AOStart && !is_switching) ||
        (PIVController_P.HILInitialize_AOEnter && is_switching)) {
      PIVController_DWork.HILInitialize_AOVoltages[0] =
        PIVController_P.HILInitialize_AOInitial;
      PIVController_DWork.HILInitialize_AOVoltages[1] =
        PIVController_P.HILInitialize_AOInitial;
      result = hil_write_analog(PIVController_DWork.HILInitialize_Card,
        PIVController_P.HILInitialize_AOChannels, 2U,
        &PIVController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
        return;
      }
    }

    if (PIVController_P.HILInitialize_AOReset) {
      PIVController_DWork.HILInitialize_AOVoltages[0] =
        PIVController_P.HILInitialize_AOWatchdog;
      PIVController_DWork.HILInitialize_AOVoltages[1] =
        PIVController_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (PIVController_DWork.HILInitialize_Card,
         PIVController_P.HILInitialize_AOChannels, 2U,
         &PIVController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
        return;
      }
    }

    if ((PIVController_P.HILInitialize_EIPStart && !is_switching) ||
        (PIVController_P.HILInitialize_EIPEnter && is_switching)) {
      PIVController_DWork.HILInitialize_QuadratureModes[0] =
        PIVController_P.HILInitialize_EIQuadrature;
      PIVController_DWork.HILInitialize_QuadratureModes[1] =
        PIVController_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (PIVController_DWork.HILInitialize_Card,
         PIVController_P.HILInitialize_EIChannels, 2U,
         (t_encoder_quadrature_mode *)
         &PIVController_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
        return;
      }
    }

    if ((PIVController_P.HILInitialize_EIStart && !is_switching) ||
        (PIVController_P.HILInitialize_EIEnter && is_switching)) {
      PIVController_DWork.HILInitialize_InitialEICounts[0] =
        PIVController_P.HILInitialize_EIInitial;
      PIVController_DWork.HILInitialize_InitialEICounts[1] =
        PIVController_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(PIVController_DWork.HILInitialize_Card,
        PIVController_P.HILInitialize_EIChannels, 2U,
        &PIVController_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

  /* S-Function Block: PIVController/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader
      (PIVController_DWork.HILInitialize_Card,
       PIVController_P.HILReadAnalogTimebase_SamplesIn,
       PIVController_P.HILReadAnalogTimebase_Channels, 2,
       &PIVController_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(PIVController_M, _rt_error_message);
    }
  }

  /* InitializeConditions for Derivative: '<Root>/Derivative1' */
  PIVController_DWork.Derivative1_RWORK.TimeStampA = rtInf;
  PIVController_DWork.Derivative1_RWORK.TimeStampB = rtInf;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn' */
  PIVController_X.TransferFcn_CSTATE = 0.0;
}

/* Model terminate function */
void PIVController_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: PIVController/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(PIVController_DWork.HILInitialize_Card);
    hil_monitor_stop_all(PIVController_DWork.HILInitialize_Card);
    is_switching = false;
    if ((PIVController_P.HILInitialize_AOTerminate && !is_switching) ||
        (PIVController_P.HILInitialize_AOExit && is_switching)) {
      PIVController_DWork.HILInitialize_AOVoltages[0] =
        PIVController_P.HILInitialize_AOFinal;
      PIVController_DWork.HILInitialize_AOVoltages[1] =
        PIVController_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(PIVController_DWork.HILInitialize_Card,
        PIVController_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &PIVController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(PIVController_M, _rt_error_message);
      }
    }

    hil_task_delete_all(PIVController_DWork.HILInitialize_Card);
    hil_monitor_delete_all(PIVController_DWork.HILInitialize_Card);
    hil_close(PIVController_DWork.HILInitialize_Card);
    PIVController_DWork.HILInitialize_Card = NULL;
  }
}
