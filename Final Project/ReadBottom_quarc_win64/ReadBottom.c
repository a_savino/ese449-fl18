/*
 * ReadBottom.c
 *
 * Code generation for model "ReadBottom".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Dec 03 15:06:03 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "ReadBottom.h"
#include "ReadBottom_private.h"
#include "ReadBottom_dt.h"

/* Block signals (auto storage) */
BlockIO_ReadBottom ReadBottom_B;

/* Block states (auto storage) */
D_Work_ReadBottom ReadBottom_DWork;

/* Real-time model */
RT_MODEL_ReadBottom ReadBottom_M_;
RT_MODEL_ReadBottom *const ReadBottom_M = &ReadBottom_M_;

/* Model step function */
void ReadBottom_step(void)
{
  /* S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

  /* S-Function Block: ReadBottom/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_read_analog(ReadBottom_DWork.HILReadAnalogTimebase_Task, 1,
      &ReadBottom_DWork.HILReadAnalogTimebase_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
    }

    ReadBottom_B.HILReadAnalogTimebase =
      ReadBottom_DWork.HILReadAnalogTimebase_Buffer;
  }

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.002s, 0.0s] */
    rtExtModeUpload(0, ReadBottom_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.002s, 0.0s] */
    if ((rtmGetTFinal(ReadBottom_M)!=-1) &&
        !((rtmGetTFinal(ReadBottom_M)-ReadBottom_M->Timing.taskTime0) >
          ReadBottom_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(ReadBottom_M, "Simulation finished");
    }

    if (rtmGetStopRequested(ReadBottom_M)) {
      rtmSetErrorStatus(ReadBottom_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++ReadBottom_M->Timing.clockTick0)) {
    ++ReadBottom_M->Timing.clockTickH0;
  }

  ReadBottom_M->Timing.taskTime0 = ReadBottom_M->Timing.clockTick0 *
    ReadBottom_M->Timing.stepSize0 + ReadBottom_M->Timing.clockTickH0 *
    ReadBottom_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void ReadBottom_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)ReadBottom_M, 0,
                sizeof(RT_MODEL_ReadBottom));
  rtmSetTFinal(ReadBottom_M, -1);
  ReadBottom_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  ReadBottom_M->Sizes.checksums[0] = (2792019655U);
  ReadBottom_M->Sizes.checksums[1] = (174510745U);
  ReadBottom_M->Sizes.checksums[2] = (3703397103U);
  ReadBottom_M->Sizes.checksums[3] = (2145983514U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    ReadBottom_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(ReadBottom_M->extModeInfo,
      &ReadBottom_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(ReadBottom_M->extModeInfo, ReadBottom_M->Sizes.checksums);
    rteiSetTPtr(ReadBottom_M->extModeInfo, rtmGetTPtr(ReadBottom_M));
  }

  /* block I/O */
  (void) memset(((void *) &ReadBottom_B), 0,
                sizeof(BlockIO_ReadBottom));

  /* states (dwork) */
  (void) memset((void *)&ReadBottom_DWork, 0,
                sizeof(D_Work_ReadBottom));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    ReadBottom_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: ReadBottom/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &ReadBottom_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options(ReadBottom_DWork.HILInitialize_Card,
      "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(ReadBottom_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
      return;
    }

    if ((ReadBottom_P.HILInitialize_AIPStart && !is_switching) ||
        (ReadBottom_P.HILInitialize_AIPEnter && is_switching)) {
      ReadBottom_DWork.HILInitialize_AIMinimums[0] =
        ReadBottom_P.HILInitialize_AILow;
      ReadBottom_DWork.HILInitialize_AIMinimums[1] =
        ReadBottom_P.HILInitialize_AILow;
      ReadBottom_DWork.HILInitialize_AIMaximums[0] =
        ReadBottom_P.HILInitialize_AIHigh;
      ReadBottom_DWork.HILInitialize_AIMaximums[1] =
        ReadBottom_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(ReadBottom_DWork.HILInitialize_Card,
        ReadBottom_P.HILInitialize_AIChannels, 2U,
        &ReadBottom_DWork.HILInitialize_AIMinimums[0],
        &ReadBottom_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
        return;
      }
    }

    if ((ReadBottom_P.HILInitialize_AOPStart && !is_switching) ||
        (ReadBottom_P.HILInitialize_AOPEnter && is_switching)) {
      ReadBottom_DWork.HILInitialize_AOMinimums[0] =
        ReadBottom_P.HILInitialize_AOLow;
      ReadBottom_DWork.HILInitialize_AOMinimums[1] =
        ReadBottom_P.HILInitialize_AOLow;
      ReadBottom_DWork.HILInitialize_AOMaximums[0] =
        ReadBottom_P.HILInitialize_AOHigh;
      ReadBottom_DWork.HILInitialize_AOMaximums[1] =
        ReadBottom_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(ReadBottom_DWork.HILInitialize_Card,
        ReadBottom_P.HILInitialize_AOChannels, 2U,
        &ReadBottom_DWork.HILInitialize_AOMinimums[0],
        &ReadBottom_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
        return;
      }
    }

    if ((ReadBottom_P.HILInitialize_AOStart && !is_switching) ||
        (ReadBottom_P.HILInitialize_AOEnter && is_switching)) {
      ReadBottom_DWork.HILInitialize_AOVoltages[0] =
        ReadBottom_P.HILInitialize_AOInitial;
      ReadBottom_DWork.HILInitialize_AOVoltages[1] =
        ReadBottom_P.HILInitialize_AOInitial;
      result = hil_write_analog(ReadBottom_DWork.HILInitialize_Card,
        ReadBottom_P.HILInitialize_AOChannels, 2U,
        &ReadBottom_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
        return;
      }
    }

    if (ReadBottom_P.HILInitialize_AOReset) {
      ReadBottom_DWork.HILInitialize_AOVoltages[0] =
        ReadBottom_P.HILInitialize_AOWatchdog;
      ReadBottom_DWork.HILInitialize_AOVoltages[1] =
        ReadBottom_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (ReadBottom_DWork.HILInitialize_Card,
         ReadBottom_P.HILInitialize_AOChannels, 2U,
         &ReadBottom_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
        return;
      }
    }

    if ((ReadBottom_P.HILInitialize_EIPStart && !is_switching) ||
        (ReadBottom_P.HILInitialize_EIPEnter && is_switching)) {
      ReadBottom_DWork.HILInitialize_QuadratureModes[0] =
        ReadBottom_P.HILInitialize_EIQuadrature;
      ReadBottom_DWork.HILInitialize_QuadratureModes[1] =
        ReadBottom_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (ReadBottom_DWork.HILInitialize_Card,
         ReadBottom_P.HILInitialize_EIChannels, 2U, (t_encoder_quadrature_mode *)
         &ReadBottom_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
        return;
      }
    }

    if ((ReadBottom_P.HILInitialize_EIStart && !is_switching) ||
        (ReadBottom_P.HILInitialize_EIEnter && is_switching)) {
      ReadBottom_DWork.HILInitialize_InitialEICounts[0] =
        ReadBottom_P.HILInitialize_EIInitial;
      ReadBottom_DWork.HILInitialize_InitialEICounts[1] =
        ReadBottom_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(ReadBottom_DWork.HILInitialize_Card,
        ReadBottom_P.HILInitialize_EIChannels, 2U,
        &ReadBottom_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

  /* S-Function Block: ReadBottom/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader(ReadBottom_DWork.HILInitialize_Card,
      ReadBottom_P.HILReadAnalogTimebase_SamplesIn,
      &ReadBottom_P.HILReadAnalogTimebase_Channels, 1,
      &ReadBottom_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
    }
  }
}

/* Model terminate function */
void ReadBottom_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: ReadBottom/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(ReadBottom_DWork.HILInitialize_Card);
    hil_monitor_stop_all(ReadBottom_DWork.HILInitialize_Card);
    is_switching = false;
    if ((ReadBottom_P.HILInitialize_AOTerminate && !is_switching) ||
        (ReadBottom_P.HILInitialize_AOExit && is_switching)) {
      ReadBottom_DWork.HILInitialize_AOVoltages[0] =
        ReadBottom_P.HILInitialize_AOFinal;
      ReadBottom_DWork.HILInitialize_AOVoltages[1] =
        ReadBottom_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(ReadBottom_DWork.HILInitialize_Card,
        ReadBottom_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &ReadBottom_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(ReadBottom_M, _rt_error_message);
      }
    }

    hil_task_delete_all(ReadBottom_DWork.HILInitialize_Card);
    hil_monitor_delete_all(ReadBottom_DWork.HILInitialize_Card);
    hil_close(ReadBottom_DWork.HILInitialize_Card);
    ReadBottom_DWork.HILInitialize_Card = NULL;
  }
}
