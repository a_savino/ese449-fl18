/*
 * ReadBottom_types.h
 *
 * Code generation for model "ReadBottom".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Dec 03 15:06:03 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_ReadBottom_types_h_
#define RTW_HEADER_ReadBottom_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_ReadBottom_ Parameters_ReadBottom;

/* Forward declaration for rtModel */
typedef struct tag_RTM_ReadBottom RT_MODEL_ReadBottom;

#endif                                 /* RTW_HEADER_ReadBottom_types_h_ */
