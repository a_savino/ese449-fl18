clf
'Uncompensated System'
numg=1.6263;
deng=[0.03 1 0];
'P(s)'
P=tf(numg, deng);
lag = tf([1, 30],[1 26.78]);
K_lag = 8.61;
PI = tf([1 0.1],[1 0])
sys = P*lag*K_lag*PI;
rlocus(sys)
Zeta = .69;
sgrid(Zeta,0)
ylim([-15, 15])
[K,p] = rlocfind(sys)

figure(2);

Pc = feedback(series(sys,K),1);
step(Pc);
grid;