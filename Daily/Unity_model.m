t = 0:0.1:10;
alpha=1;
ramp=alpha*t;
Hs_0 = tf(1.5, [0.025 1]);
Hs_1 = tf(1.5, [0.025 1 0]);
model_0 = feedback(Hs_0,1);
model_1 = feedback(Hs_1,1);

% type 0 with ramp (inf. error)
[y,t]=lsim(model_0,ramp,t);
plot(t,y);
hold on;
plot(t,ramp);

% type 1 with ramp (constant error)
[y,t]=lsim(model_1,ramp,t);
plot(t,y);

figure;
step = ones(1, 101);

%type 0 with step
[y,t]=lsim(model_0,step, t);
plot(t,y);
hold on;
%plot(t,1);
[y,t]=lsim(model_1,step,t);
plot(t,y);