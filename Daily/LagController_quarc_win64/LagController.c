/*
 * LagController.c
 *
 * Code generation for model "LagController".
 *
 * Model version              : 1.14
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Sat Oct 20 14:48:45 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "LagController.h"
#include "LagController_private.h"
#include "LagController_dt.h"

/* Block signals (auto storage) */
BlockIO_LagController LagController_B;

/* Continuous states */
ContinuousStates_LagController LagController_X;

/* Block states (auto storage) */
D_Work_LagController LagController_DWork;

/* Real-time model */
RT_MODEL_LagController LagController_M_;
RT_MODEL_LagController *const LagController_M = &LagController_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 2;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  LagController_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void LagController_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_HILReadAnalogTimebase_o2;
  real_T rtb_HILReadEncoder;
  real_T rtb_LagCompensator;
  real_T temp;
  if (rtmIsMajorTimeStep(LagController_M)) {
    /* set solver stop time */
    if (!(LagController_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&LagController_M->solverInfo,
                            ((LagController_M->Timing.clockTickH0 + 1) *
        LagController_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&LagController_M->solverInfo,
                            ((LagController_M->Timing.clockTick0 + 1) *
        LagController_M->Timing.stepSize0 + LagController_M->Timing.clockTickH0 *
        LagController_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(LagController_M)) {
    LagController_M->Timing.t[0] = rtsiGetT(&LagController_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(LagController_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

    /* S-Function Block: LagController/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog
        (LagController_DWork.HILReadAnalogTimebase_Task, 1,
         &LagController_DWork.HILReadAnalogTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
      }

      rtb_HILReadAnalogTimebase_o1 =
        LagController_DWork.HILReadAnalogTimebase_Buffer[0];
      rtb_HILReadAnalogTimebase_o2 =
        LagController_DWork.HILReadAnalogTimebase_Buffer[1];
    }

    /* S-Function (hil_read_encoder_block): '<S1>/HIL Read Encoder' */

    /* S-Function Block: LagController/Subsystem/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(LagController_DWork.HILInitialize_Card,
        &LagController_P.HILReadEncoder_Channels, 1,
        &LagController_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder = LagController_DWork.HILReadEncoder_Buffer;
      }
    }

    /* Gain: '<S1>/Encoder Calibration (deg//count)' */
    LagController_B.EncoderCalibrationdegcount =
      LagController_P.EncoderCalibrationdegcount_Gain * rtb_HILReadEncoder;
  }

  /* SignalGenerator: '<Root>/Signal Generator' */
  temp = LagController_P.SignalGenerator_Frequency * LagController_M->Timing.t[0];
  LagController_B.SignalGenerator = (1.0 - (temp - floor(temp)) * 2.0) *
    LagController_P.SignalGenerator_Amplitude;
  if (rtmIsMajorTimeStep(LagController_M)) {
    /* Gain: '<S1>/Tachometer Calibration (krpm//V)' */
    LagController_B.TachometerCalibrationkrpmV =
      LagController_P.TachometerCalibrationkrpmV_Gain *
      rtb_HILReadAnalogTimebase_o2;

    /* Gain: '<S1>/Potentiometer calibration  (deg//V)' */
    LagController_B.PotentiometercalibrationdegV =
      LagController_P.PotentiometercalibrationdegV_Ga *
      rtb_HILReadAnalogTimebase_o1;
  }

  /* Sum: '<Root>/Sum' incorporates:
   *  Gain: '<Root>/Gain'
   */
  LagController_B.Sum = LagController_P.Gain_Gain *
    LagController_B.SignalGenerator - LagController_B.EncoderCalibrationdegcount;

  /* TransferFcn: '<Root>/PI' */
  LagController_B.PI = LagController_P.PI_D*LagController_B.Sum;
  LagController_B.PI += LagController_P.PI_C*LagController_X.PI_CSTATE;

  /* TransferFcn: '<Root>/Lag Compensator ' */
  rtb_LagCompensator = LagController_P.LagCompensator_D*LagController_B.PI;
  rtb_LagCompensator += LagController_P.LagCompensator_C*
    LagController_X.LagCompensator_CSTATE;

  /* Saturate: '<Root>/Saturation' */
  if (rtb_LagCompensator >= LagController_P.Saturation_UpperSat) {
    temp = LagController_P.Saturation_UpperSat;
  } else if (rtb_LagCompensator <= LagController_P.Saturation_LowerSat) {
    temp = LagController_P.Saturation_LowerSat;
  } else {
    temp = rtb_LagCompensator;
  }

  /* Gain: '<S1>/Motor Calibration (V//V)' incorporates:
   *  Saturate: '<Root>/Saturation'
   */
  LagController_B.MotorCalibrationVV = LagController_P.MotorCalibrationVV_Gain *
    temp;
  if (rtmIsMajorTimeStep(LagController_M)) {
    /* S-Function (hil_write_block): '<S1>/HIL Write' */

    /* S-Function Block: LagController/Subsystem/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(LagController_DWork.HILInitialize_Card,
                         &LagController_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &LagController_B.MotorCalibrationVV,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
      }
    }
  }

  if (rtmIsMajorTimeStep(LagController_M)) {
    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, LagController_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(LagController_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((LagController_M->Timing.clockTick1+
                            LagController_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(LagController_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(LagController_M)!=-1) &&
          !((rtmGetTFinal(LagController_M)-(((LagController_M->Timing.clockTick1
               +LagController_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((LagController_M->Timing.clockTick1+
               LagController_M->Timing.clockTickH1* 4294967296.0)) * 0.002) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(LagController_M, "Simulation finished");
      }

      if (rtmGetStopRequested(LagController_M)) {
        rtmSetErrorStatus(LagController_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&LagController_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++LagController_M->Timing.clockTick0)) {
      ++LagController_M->Timing.clockTickH0;
    }

    LagController_M->Timing.t[0] = rtsiGetSolverStopTime
      (&LagController_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      LagController_M->Timing.clockTick1++;
      if (!LagController_M->Timing.clockTick1) {
        LagController_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void LagController_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/PI' */
  {
    ((StateDerivatives_LagController *) LagController_M->ModelData.derivs)
      ->PI_CSTATE = LagController_B.Sum;
    ((StateDerivatives_LagController *) LagController_M->ModelData.derivs)
      ->PI_CSTATE += (LagController_P.PI_A)*LagController_X.PI_CSTATE;
  }

  /* Derivatives for TransferFcn: '<Root>/Lag Compensator ' */
  {
    ((StateDerivatives_LagController *) LagController_M->ModelData.derivs)
      ->LagCompensator_CSTATE = LagController_B.PI;
    ((StateDerivatives_LagController *) LagController_M->ModelData.derivs)
      ->LagCompensator_CSTATE += (LagController_P.LagCompensator_A)*
      LagController_X.LagCompensator_CSTATE;
  }
}

/* Model initialize function */
void LagController_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)LagController_M, 0,
                sizeof(RT_MODEL_LagController));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&LagController_M->solverInfo,
                          &LagController_M->Timing.simTimeStep);
    rtsiSetTPtr(&LagController_M->solverInfo, &rtmGetTPtr(LagController_M));
    rtsiSetStepSizePtr(&LagController_M->solverInfo,
                       &LagController_M->Timing.stepSize0);
    rtsiSetdXPtr(&LagController_M->solverInfo,
                 &LagController_M->ModelData.derivs);
    rtsiSetContStatesPtr(&LagController_M->solverInfo,
                         &LagController_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&LagController_M->solverInfo,
      &LagController_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&LagController_M->solverInfo, (&rtmGetErrorStatus
      (LagController_M)));
    rtsiSetRTModelPtr(&LagController_M->solverInfo, LagController_M);
  }

  rtsiSetSimTimeStep(&LagController_M->solverInfo, MAJOR_TIME_STEP);
  LagController_M->ModelData.intgData.f[0] = LagController_M->ModelData.odeF[0];
  LagController_M->ModelData.contStates = ((real_T *) &LagController_X);
  rtsiSetSolverData(&LagController_M->solverInfo, (void *)
                    &LagController_M->ModelData.intgData);
  rtsiSetSolverName(&LagController_M->solverInfo,"ode1");
  rtmSetTPtr(LagController_M, &LagController_M->Timing.tArray[0]);
  rtmSetTFinal(LagController_M, 8.0);
  LagController_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  LagController_M->Sizes.checksums[0] = (745639648U);
  LagController_M->Sizes.checksums[1] = (114147794U);
  LagController_M->Sizes.checksums[2] = (2708825034U);
  LagController_M->Sizes.checksums[3] = (1791943064U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    LagController_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(LagController_M->extModeInfo,
      &LagController_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(LagController_M->extModeInfo,
                        LagController_M->Sizes.checksums);
    rteiSetTPtr(LagController_M->extModeInfo, rtmGetTPtr(LagController_M));
  }

  /* block I/O */
  (void) memset(((void *) &LagController_B), 0,
                sizeof(BlockIO_LagController));

  /* states (continuous) */
  {
    (void) memset((void *)&LagController_X, 0,
                  sizeof(ContinuousStates_LagController));
  }

  /* states (dwork) */
  (void) memset((void *)&LagController_DWork, 0,
                sizeof(D_Work_LagController));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    LagController_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: LagController/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &LagController_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(LagController_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options
      (LagController_DWork.HILInitialize_Card,
       "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(LagController_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(LagController_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(LagController_M, _rt_error_message);
      return;
    }

    if ((LagController_P.HILInitialize_AIPStart && !is_switching) ||
        (LagController_P.HILInitialize_AIPEnter && is_switching)) {
      LagController_DWork.HILInitialize_AIMinimums[0] =
        LagController_P.HILInitialize_AILow;
      LagController_DWork.HILInitialize_AIMinimums[1] =
        LagController_P.HILInitialize_AILow;
      LagController_DWork.HILInitialize_AIMaximums[0] =
        LagController_P.HILInitialize_AIHigh;
      LagController_DWork.HILInitialize_AIMaximums[1] =
        LagController_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges
        (LagController_DWork.HILInitialize_Card,
         LagController_P.HILInitialize_AIChannels, 2U,
         &LagController_DWork.HILInitialize_AIMinimums[0],
         &LagController_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
        return;
      }
    }

    if ((LagController_P.HILInitialize_AOPStart && !is_switching) ||
        (LagController_P.HILInitialize_AOPEnter && is_switching)) {
      LagController_DWork.HILInitialize_AOMinimums[0] =
        LagController_P.HILInitialize_AOLow;
      LagController_DWork.HILInitialize_AOMinimums[1] =
        LagController_P.HILInitialize_AOLow;
      LagController_DWork.HILInitialize_AOMaximums[0] =
        LagController_P.HILInitialize_AOHigh;
      LagController_DWork.HILInitialize_AOMaximums[1] =
        LagController_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges
        (LagController_DWork.HILInitialize_Card,
         LagController_P.HILInitialize_AOChannels, 2U,
         &LagController_DWork.HILInitialize_AOMinimums[0],
         &LagController_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
        return;
      }
    }

    if ((LagController_P.HILInitialize_AOStart && !is_switching) ||
        (LagController_P.HILInitialize_AOEnter && is_switching)) {
      LagController_DWork.HILInitialize_AOVoltages[0] =
        LagController_P.HILInitialize_AOInitial;
      LagController_DWork.HILInitialize_AOVoltages[1] =
        LagController_P.HILInitialize_AOInitial;
      result = hil_write_analog(LagController_DWork.HILInitialize_Card,
        LagController_P.HILInitialize_AOChannels, 2U,
        &LagController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
        return;
      }
    }

    if (LagController_P.HILInitialize_AOReset) {
      LagController_DWork.HILInitialize_AOVoltages[0] =
        LagController_P.HILInitialize_AOWatchdog;
      LagController_DWork.HILInitialize_AOVoltages[1] =
        LagController_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (LagController_DWork.HILInitialize_Card,
         LagController_P.HILInitialize_AOChannels, 2U,
         &LagController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
        return;
      }
    }

    if ((LagController_P.HILInitialize_EIPStart && !is_switching) ||
        (LagController_P.HILInitialize_EIPEnter && is_switching)) {
      LagController_DWork.HILInitialize_QuadratureModes[0] =
        LagController_P.HILInitialize_EIQuadrature;
      LagController_DWork.HILInitialize_QuadratureModes[1] =
        LagController_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (LagController_DWork.HILInitialize_Card,
         LagController_P.HILInitialize_EIChannels, 2U,
         (t_encoder_quadrature_mode *)
         &LagController_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
        return;
      }
    }

    if ((LagController_P.HILInitialize_EIStart && !is_switching) ||
        (LagController_P.HILInitialize_EIEnter && is_switching)) {
      LagController_DWork.HILInitialize_InitialEICounts[0] =
        LagController_P.HILInitialize_EIInitial;
      LagController_DWork.HILInitialize_InitialEICounts[1] =
        LagController_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(LagController_DWork.HILInitialize_Card,
        LagController_P.HILInitialize_EIChannels, 2U,
        &LagController_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

  /* S-Function Block: LagController/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader
      (LagController_DWork.HILInitialize_Card,
       LagController_P.HILReadAnalogTimebase_SamplesIn,
       LagController_P.HILReadAnalogTimebase_Channels, 2,
       &LagController_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(LagController_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<Root>/PI' */
  LagController_X.PI_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/Lag Compensator ' */
  LagController_X.LagCompensator_CSTATE = 0.0;
}

/* Model terminate function */
void LagController_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: LagController/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(LagController_DWork.HILInitialize_Card);
    hil_monitor_stop_all(LagController_DWork.HILInitialize_Card);
    is_switching = false;
    if ((LagController_P.HILInitialize_AOTerminate && !is_switching) ||
        (LagController_P.HILInitialize_AOExit && is_switching)) {
      LagController_DWork.HILInitialize_AOVoltages[0] =
        LagController_P.HILInitialize_AOFinal;
      LagController_DWork.HILInitialize_AOVoltages[1] =
        LagController_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(LagController_DWork.HILInitialize_Card,
        LagController_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &LagController_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(LagController_M, _rt_error_message);
      }
    }

    hil_task_delete_all(LagController_DWork.HILInitialize_Card);
    hil_monitor_delete_all(LagController_DWork.HILInitialize_Card);
    hil_close(LagController_DWork.HILInitialize_Card);
    LagController_DWork.HILInitialize_Card = NULL;
  }
}
