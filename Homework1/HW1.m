%% problem 1

%undamped
response(4, 0);

%over-damped
response(4, 2); 

%critically damped
response(4, 1);

%underdamped
response(4, .3);

%% problem 2

%Gs = tf(conv(conv(10, [1 20]), [1 30]), conv(conv([1 0], [1 25]), [1 35]));
Gs = tf(conv(conv(10, [1 20]), [1 30]), conv(conv([1 0 0], [1 25]), [1 35]));

K = 15;

ramp = 0:.1:10;
step = ones(size(ramp));
parab = ramp .* ramp;
t = ramp;

model = feedback(Gs, 1);
[y_step, t] = lsim(model, step * K, t);
[y_ramp, t] = lsim(model, ramp * K, t);
[y_parab, t] = lsim(model, parab * K, t);
plot(t, y_step)
% hold on
% plot(t, y_ramp, 'r')
% plot(t, y_parab, 'g')




%% problem 3
%
% first system:
%   type 0 - Kp = 500*2*5 / (8*10*12) = 125/24 , Kv = 0
%   e_step = 0.1611, e_ramp = inf
% second system:      
%   type 1 - Kp = inf, Kv = (500*2*5*6)/(8*10*12) = 125/4
%   e_step = 0, e_ramp = 4/125
% third system:
%   type 2 - Kp = inf, Kv = inf
%   e_step = 0, e_ramp = 0

%% problem 4
H1 = tf(1.5, [0.025 1]);
H2 = tf(1.5, [0.025 1 0]);

ramp = 0:.1:10;
step = ones(size(ramp));
t = ramp;

model1 = feedback(H1, 1);
model2 = feedback(H2, 1);
[y_step1, t] = lsim(model1, step, t);
[y_ramp1, t] = lsim(model1, ramp, t);
[y_step2, t] = lsim(model2, step, t);
[y_ramp2, t] = lsim(model2, ramp, t);

subplot(1,2,1)
plot(t, y_step1)
hold on
plot(t, y_ramp1)
title('type 0 system')
subplot(1,2,2)
plot(t, y_step2)
hold on
plot(t, y_ramp2)
title('type 1 system')



%% functions for problem 1
function void = response(wn, z)
    Ps = tf(wn^2, [1, 2*z*wn, wn^2]);
    step(Ps, 10)
end

function [OS, Tr, Ts] = timesAndOS(wn, z)
    Ts = 4/(wn*z);
    Tr = (1/(wn * sqrt(1-z^2))) * (pi/2 + arcsin(z));
    OS = exp(-1*z*pi/(sqrt(1-z^2)))*100;
end

function [z, wn] = findSystem(OS, Ts)
    z = -log(OS/100)/(sqrt(pi^2 + (log(OS/100)^2)));
    wn = 4/(Ts * z);
end
