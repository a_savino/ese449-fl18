/*
 * BallBeam.c
 *
 * Code generation for model "BallBeam".
 *
 * Model version              : 1.15
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Dec 03 15:45:58 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "BallBeam.h"
#include "BallBeam_private.h"
#include "BallBeam_dt.h"

/* Block signals (auto storage) */
BlockIO_BallBeam BallBeam_B;

/* Continuous states */
ContinuousStates_BallBeam BallBeam_X;

/* Block states (auto storage) */
D_Work_BallBeam BallBeam_DWork;

/* Real-time model */
RT_MODEL_BallBeam BallBeam_M_;
RT_MODEL_BallBeam *const BallBeam_M = &BallBeam_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 3;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  BallBeam_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void BallBeam_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadAnalogTimebase_o1;
  real_T rtb_HILReadEncoder;
  real_T rtb_HighPassFilter1;
  if (rtmIsMajorTimeStep(BallBeam_M)) {
    /* set solver stop time */
    if (!(BallBeam_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&BallBeam_M->solverInfo,
                            ((BallBeam_M->Timing.clockTickH0 + 1) *
        BallBeam_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&BallBeam_M->solverInfo,
                            ((BallBeam_M->Timing.clockTick0 + 1) *
        BallBeam_M->Timing.stepSize0 + BallBeam_M->Timing.clockTickH0 *
        BallBeam_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(BallBeam_M)) {
    BallBeam_M->Timing.t[0] = rtsiGetT(&BallBeam_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(BallBeam_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

    /* S-Function Block: BallBeam/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog(BallBeam_DWork.HILReadAnalogTimebase_Task, 1,
        &BallBeam_DWork.HILReadAnalogTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
      }

      rtb_HILReadAnalogTimebase_o1 =
        BallBeam_DWork.HILReadAnalogTimebase_Buffer[0];
      rtb_HILReadEncoder = BallBeam_DWork.HILReadAnalogTimebase_Buffer[1];
    }

    /* Gain: '<Root>/Kp1' */
    BallBeam_B.Kp1 = BallBeam_P.Kp1_Gain * rtb_HILReadEncoder;

    /* ManualSwitch: '<Root>/Manual Switch' */
    if (BallBeam_P.ManualSwitch_CurrentSetting == 1) {
      BallBeam_B.ManualSwitch = BallBeam_B.Kp1;
    } else {
      BallBeam_B.ManualSwitch = 0.0;
    }

    /* End of ManualSwitch: '<Root>/Manual Switch' */

    /* Gain: '<S1>/Potentiometer calibration  (deg//V)' */
    BallBeam_B.PotentiometercalibrationdegV =
      BallBeam_P.PotentiometercalibrationdegV_Ga * rtb_HILReadAnalogTimebase_o1;

    /* Gain: '<Root>/Kv2' incorporates:
     *  Sum: '<Root>/Sum3'
     */
    BallBeam_B.Kv2 = (BallBeam_B.ManualSwitch -
                      BallBeam_B.PotentiometercalibrationdegV) *
      BallBeam_P.Kv2_Gain;

    /* Sum: '<Root>/Sum4' incorporates:
     *  Gain: '<Root>/Kv1'
     */
    BallBeam_B.Sum4 = BallBeam_P.Kv1_Gain * BallBeam_B.ManualSwitch -
      BallBeam_B.PotentiometercalibrationdegV;
  }

  /* TransferFcn: '<Root>/High Pass Filter2' */
  rtb_HighPassFilter1 = BallBeam_P.HighPassFilter2_D*BallBeam_B.Sum4;
  rtb_HighPassFilter1 += BallBeam_P.HighPassFilter2_C*
    BallBeam_X.HighPassFilter2_CSTATE;

  /* Gain: '<Root>/Kv3' incorporates:
   *  Sum: '<Root>/Sum5'
   */
  rtb_HighPassFilter1 = (BallBeam_B.Kv2 + rtb_HighPassFilter1) *
    BallBeam_P.Kv3_Gain;

  /* ManualSwitch: '<Root>/Manual Switch1' incorporates:
   *  Constant: '<Root>/Constant'
   */
  if (BallBeam_P.ManualSwitch1_CurrentSetting == 1) {
    BallBeam_B.ManualSwitch1 = rtb_HighPassFilter1;
  } else {
    BallBeam_B.ManualSwitch1 = BallBeam_P.Constant_Value;
  }

  /* End of ManualSwitch: '<Root>/Manual Switch1' */
  if (rtmIsMajorTimeStep(BallBeam_M)) {
    /* S-Function (hil_read_encoder_block): '<S1>/HIL Read Encoder' */

    /* S-Function Block: BallBeam/Subsystem/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(BallBeam_DWork.HILInitialize_Card,
        &BallBeam_P.HILReadEncoder_Channels, 1,
        &BallBeam_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder = BallBeam_DWork.HILReadEncoder_Buffer;
      }
    }

    /* Sum: '<S1>/Sum' incorporates:
     *  Constant: '<S1>/Constant'
     *  Gain: '<S1>/Encoder Calibration (deg//count)'
     *  Gain: '<S2>/Slider Gain'
     */
    BallBeam_B.Sum = BallBeam_P.EncoderCalibrationdegcount_Gain *
      rtb_HILReadEncoder + BallBeam_P.SliderGain_Gain *
      BallBeam_P.Constant_Value_e;
  }

  /* TransferFcn: '<Root>/High Pass Filter1' */
  rtb_HighPassFilter1 = BallBeam_P.HighPassFilter1_C[0]*
    BallBeam_X.HighPassFilter1_CSTATE[0]
    + BallBeam_P.HighPassFilter1_C[1]*BallBeam_X.HighPassFilter1_CSTATE[1];

  /* Gain: '<S1>/Motor Calibration (V//V)' incorporates:
   *  Gain: '<Root>/Kp'
   *  Gain: '<Root>/Kv'
   *  Sum: '<Root>/Sum1'
   *  Sum: '<Root>/Sum2'
   */
  BallBeam_B.MotorCalibrationVV = ((BallBeam_B.ManualSwitch1 - BallBeam_B.Sum) *
    BallBeam_P.Kp_Gain + BallBeam_P.Kv_Gain * rtb_HighPassFilter1) *
    BallBeam_P.MotorCalibrationVV_Gain;
  if (rtmIsMajorTimeStep(BallBeam_M)) {
    /* S-Function (hil_write_block): '<S1>/HIL Write' */

    /* S-Function Block: BallBeam/Subsystem/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(BallBeam_DWork.HILInitialize_Card,
                         &BallBeam_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &BallBeam_B.MotorCalibrationVV,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
      }
    }
  }

  if (rtmIsMajorTimeStep(BallBeam_M)) {
    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, BallBeam_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(BallBeam_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((BallBeam_M->Timing.clockTick1+
                            BallBeam_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(BallBeam_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(BallBeam_M)!=-1) &&
          !((rtmGetTFinal(BallBeam_M)-(((BallBeam_M->Timing.clockTick1+
               BallBeam_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((BallBeam_M->Timing.clockTick1+BallBeam_M->Timing.clockTickH1*
               4294967296.0)) * 0.002) * (DBL_EPSILON))) {
        rtmSetErrorStatus(BallBeam_M, "Simulation finished");
      }

      if (rtmGetStopRequested(BallBeam_M)) {
        rtmSetErrorStatus(BallBeam_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&BallBeam_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++BallBeam_M->Timing.clockTick0)) {
      ++BallBeam_M->Timing.clockTickH0;
    }

    BallBeam_M->Timing.t[0] = rtsiGetSolverStopTime(&BallBeam_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      BallBeam_M->Timing.clockTick1++;
      if (!BallBeam_M->Timing.clockTick1) {
        BallBeam_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void BallBeam_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/High Pass Filter2' */
  {
    ((StateDerivatives_BallBeam *) BallBeam_M->ModelData.derivs)
      ->HighPassFilter2_CSTATE = BallBeam_B.Sum4;
    ((StateDerivatives_BallBeam *) BallBeam_M->ModelData.derivs)
      ->HighPassFilter2_CSTATE += (BallBeam_P.HighPassFilter2_A)*
      BallBeam_X.HighPassFilter2_CSTATE;
  }

  /* Derivatives for TransferFcn: '<Root>/High Pass Filter1' */
  {
    ((StateDerivatives_BallBeam *) BallBeam_M->ModelData.derivs)
      ->HighPassFilter1_CSTATE[0] = BallBeam_B.Sum;
    ((StateDerivatives_BallBeam *) BallBeam_M->ModelData.derivs)
      ->HighPassFilter1_CSTATE[0] += (BallBeam_P.HighPassFilter1_A[0])*
      BallBeam_X.HighPassFilter1_CSTATE[0]
      + (BallBeam_P.HighPassFilter1_A[1])*BallBeam_X.HighPassFilter1_CSTATE[1];
    ((StateDerivatives_BallBeam *) BallBeam_M->ModelData.derivs)
      ->HighPassFilter1_CSTATE[1]= BallBeam_X.HighPassFilter1_CSTATE[0];
  }
}

/* Model initialize function */
void BallBeam_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)BallBeam_M, 0,
                sizeof(RT_MODEL_BallBeam));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&BallBeam_M->solverInfo,
                          &BallBeam_M->Timing.simTimeStep);
    rtsiSetTPtr(&BallBeam_M->solverInfo, &rtmGetTPtr(BallBeam_M));
    rtsiSetStepSizePtr(&BallBeam_M->solverInfo, &BallBeam_M->Timing.stepSize0);
    rtsiSetdXPtr(&BallBeam_M->solverInfo, &BallBeam_M->ModelData.derivs);
    rtsiSetContStatesPtr(&BallBeam_M->solverInfo,
                         &BallBeam_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&BallBeam_M->solverInfo,
      &BallBeam_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&BallBeam_M->solverInfo, (&rtmGetErrorStatus
      (BallBeam_M)));
    rtsiSetRTModelPtr(&BallBeam_M->solverInfo, BallBeam_M);
  }

  rtsiSetSimTimeStep(&BallBeam_M->solverInfo, MAJOR_TIME_STEP);
  BallBeam_M->ModelData.intgData.f[0] = BallBeam_M->ModelData.odeF[0];
  BallBeam_M->ModelData.contStates = ((real_T *) &BallBeam_X);
  rtsiSetSolverData(&BallBeam_M->solverInfo, (void *)
                    &BallBeam_M->ModelData.intgData);
  rtsiSetSolverName(&BallBeam_M->solverInfo,"ode1");
  rtmSetTPtr(BallBeam_M, &BallBeam_M->Timing.tArray[0]);
  rtmSetTFinal(BallBeam_M, -1);
  BallBeam_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  BallBeam_M->Sizes.checksums[0] = (4024864619U);
  BallBeam_M->Sizes.checksums[1] = (1074047285U);
  BallBeam_M->Sizes.checksums[2] = (4006388255U);
  BallBeam_M->Sizes.checksums[3] = (2165778758U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    BallBeam_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(BallBeam_M->extModeInfo,
      &BallBeam_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(BallBeam_M->extModeInfo, BallBeam_M->Sizes.checksums);
    rteiSetTPtr(BallBeam_M->extModeInfo, rtmGetTPtr(BallBeam_M));
  }

  /* block I/O */
  (void) memset(((void *) &BallBeam_B), 0,
                sizeof(BlockIO_BallBeam));

  /* states (continuous) */
  {
    (void) memset((void *)&BallBeam_X, 0,
                  sizeof(ContinuousStates_BallBeam));
  }

  /* states (dwork) */
  (void) memset((void *)&BallBeam_DWork, 0,
                sizeof(D_Work_BallBeam));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    BallBeam_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: BallBeam/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &BallBeam_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(BallBeam_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options(BallBeam_DWork.HILInitialize_Card,
      "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(BallBeam_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(BallBeam_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(BallBeam_M, _rt_error_message);
      return;
    }

    if ((BallBeam_P.HILInitialize_AIPStart && !is_switching) ||
        (BallBeam_P.HILInitialize_AIPEnter && is_switching)) {
      BallBeam_DWork.HILInitialize_AIMinimums[0] =
        BallBeam_P.HILInitialize_AILow;
      BallBeam_DWork.HILInitialize_AIMinimums[1] =
        BallBeam_P.HILInitialize_AILow;
      BallBeam_DWork.HILInitialize_AIMaximums[0] =
        BallBeam_P.HILInitialize_AIHigh;
      BallBeam_DWork.HILInitialize_AIMaximums[1] =
        BallBeam_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(BallBeam_DWork.HILInitialize_Card,
        BallBeam_P.HILInitialize_AIChannels, 2U,
        &BallBeam_DWork.HILInitialize_AIMinimums[0],
        &BallBeam_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
        return;
      }
    }

    if ((BallBeam_P.HILInitialize_AOPStart && !is_switching) ||
        (BallBeam_P.HILInitialize_AOPEnter && is_switching)) {
      BallBeam_DWork.HILInitialize_AOMinimums[0] =
        BallBeam_P.HILInitialize_AOLow;
      BallBeam_DWork.HILInitialize_AOMinimums[1] =
        BallBeam_P.HILInitialize_AOLow;
      BallBeam_DWork.HILInitialize_AOMaximums[0] =
        BallBeam_P.HILInitialize_AOHigh;
      BallBeam_DWork.HILInitialize_AOMaximums[1] =
        BallBeam_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(BallBeam_DWork.HILInitialize_Card,
        BallBeam_P.HILInitialize_AOChannels, 2U,
        &BallBeam_DWork.HILInitialize_AOMinimums[0],
        &BallBeam_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
        return;
      }
    }

    if ((BallBeam_P.HILInitialize_AOStart && !is_switching) ||
        (BallBeam_P.HILInitialize_AOEnter && is_switching)) {
      BallBeam_DWork.HILInitialize_AOVoltages[0] =
        BallBeam_P.HILInitialize_AOInitial;
      BallBeam_DWork.HILInitialize_AOVoltages[1] =
        BallBeam_P.HILInitialize_AOInitial;
      result = hil_write_analog(BallBeam_DWork.HILInitialize_Card,
        BallBeam_P.HILInitialize_AOChannels, 2U,
        &BallBeam_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
        return;
      }
    }

    if (BallBeam_P.HILInitialize_AOReset) {
      BallBeam_DWork.HILInitialize_AOVoltages[0] =
        BallBeam_P.HILInitialize_AOWatchdog;
      BallBeam_DWork.HILInitialize_AOVoltages[1] =
        BallBeam_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (BallBeam_DWork.HILInitialize_Card, BallBeam_P.HILInitialize_AOChannels,
         2U, &BallBeam_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
        return;
      }
    }

    if ((BallBeam_P.HILInitialize_EIPStart && !is_switching) ||
        (BallBeam_P.HILInitialize_EIPEnter && is_switching)) {
      BallBeam_DWork.HILInitialize_QuadratureModes[0] =
        BallBeam_P.HILInitialize_EIQuadrature;
      BallBeam_DWork.HILInitialize_QuadratureModes[1] =
        BallBeam_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode(BallBeam_DWork.HILInitialize_Card,
        BallBeam_P.HILInitialize_EIChannels, 2U, (t_encoder_quadrature_mode *)
        &BallBeam_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
        return;
      }
    }

    if ((BallBeam_P.HILInitialize_EIStart && !is_switching) ||
        (BallBeam_P.HILInitialize_EIEnter && is_switching)) {
      BallBeam_DWork.HILInitialize_InitialEICounts[0] =
        BallBeam_P.HILInitialize_EIInitial;
      BallBeam_DWork.HILInitialize_InitialEICounts[1] =
        BallBeam_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(BallBeam_DWork.HILInitialize_Card,
        BallBeam_P.HILInitialize_EIChannels, 2U,
        &BallBeam_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<S1>/HIL Read Analog Timebase' */

  /* S-Function Block: BallBeam/Subsystem/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader(BallBeam_DWork.HILInitialize_Card,
      BallBeam_P.HILReadAnalogTimebase_SamplesIn,
      BallBeam_P.HILReadAnalogTimebase_Channels, 2,
      &BallBeam_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(BallBeam_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<Root>/High Pass Filter2' */
  BallBeam_X.HighPassFilter2_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/High Pass Filter1' */
  BallBeam_X.HighPassFilter1_CSTATE[0] = 0.0;
  BallBeam_X.HighPassFilter1_CSTATE[1] = 0.0;
}

/* Model terminate function */
void BallBeam_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: BallBeam/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(BallBeam_DWork.HILInitialize_Card);
    hil_monitor_stop_all(BallBeam_DWork.HILInitialize_Card);
    is_switching = false;
    if ((BallBeam_P.HILInitialize_AOTerminate && !is_switching) ||
        (BallBeam_P.HILInitialize_AOExit && is_switching)) {
      BallBeam_DWork.HILInitialize_AOVoltages[0] =
        BallBeam_P.HILInitialize_AOFinal;
      BallBeam_DWork.HILInitialize_AOVoltages[1] =
        BallBeam_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(BallBeam_DWork.HILInitialize_Card,
        BallBeam_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &BallBeam_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(BallBeam_M, _rt_error_message);
      }
    }

    hil_task_delete_all(BallBeam_DWork.HILInitialize_Card);
    hil_monitor_delete_all(BallBeam_DWork.HILInitialize_Card);
    hil_close(BallBeam_DWork.HILInitialize_Card);
    BallBeam_DWork.HILInitialize_Card = NULL;
  }
}
