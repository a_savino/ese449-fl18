OS = 10;
cts = 0.04;
ts = 3.5;
Kbb = .419;
zeta = sqrt( (log(OS/100)^2 / (pi^2 + log(OS/100)^2)));
w_n = 3.2/(zeta * ts);
Kc = 2*zeta*w_n / (Kbb);
z = w_n^2 /(Kbb*Kc);
save('workspace')