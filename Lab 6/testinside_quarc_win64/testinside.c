/*
 * testinside.c
 *
 * Code generation for model "testinside".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Nov 19 16:12:08 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "testinside.h"
#include "testinside_private.h"
#include "testinside_dt.h"

/* Block signals (auto storage) */
BlockIO_testinside testinside_B;

/* Continuous states */
ContinuousStates_testinside testinside_X;

/* Block states (auto storage) */
D_Work_testinside testinside_DWork;

/* Real-time model */
RT_MODEL_testinside testinside_M_;
RT_MODEL_testinside *const testinside_M = &testinside_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 1;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  testinside_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void testinside_step(void)
{
  /* local block i/o variables */
  real_T rtb_Sum4;
  real_T rtb_Gain3;
  if (rtmIsMajorTimeStep(testinside_M)) {
    /* set solver stop time */
    if (!(testinside_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&testinside_M->solverInfo,
                            ((testinside_M->Timing.clockTickH0 + 1) *
        testinside_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&testinside_M->solverInfo,
                            ((testinside_M->Timing.clockTick0 + 1) *
        testinside_M->Timing.stepSize0 + testinside_M->Timing.clockTickH0 *
        testinside_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(testinside_M)) {
    testinside_M->Timing.t[0] = rtsiGetT(&testinside_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(testinside_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

    /* S-Function Block: testinside/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog(testinside_DWork.HILReadAnalogTimebase_Task,
        1, &testinside_DWork.HILReadAnalogTimebase_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
      }

      rtb_Sum4 = testinside_DWork.HILReadAnalogTimebase_Buffer;
    }

    /* Gain: '<Root>/Encoder Calibration (deg//count)' */
    testinside_B.EncoderCalibrationdegcount =
      testinside_P.EncoderCalibrationdegcount_Gain * rtb_Sum4;

    /* Sum: '<Root>/Sum4' incorporates:
     *  Constant: '<Root>/Constant1'
     *  Gain: '<S1>/Slider Gain'
     */
    rtb_Sum4 = testinside_P.SliderGain_Gain * testinside_P.Constant1_Value +
      testinside_B.EncoderCalibrationdegcount;

    /* Gain: '<Root>/Kc1' incorporates:
     *  Constant: '<Root>/Constant'
     *  Sum: '<Root>/Sum3'
     */
    testinside_B.Kc1 = (testinside_P.Constant_Value - rtb_Sum4) *
      testinside_P.Kc1_Gain;

    /* Gain: '<Root>/Gain2' */
    testinside_B.Gain2 = testinside_P.Gain2_Gain * rtb_Sum4;
  }

  /* TransferFcn: '<Root>/Transfer Fcn3' */
  rtb_Gain3 = testinside_P.TransferFcn3_D*testinside_B.Gain2;
  rtb_Gain3 += testinside_P.TransferFcn3_C*testinside_X.TransferFcn3_CSTATE;

  /* Gain: '<Root>/Gain3' incorporates:
   *  Gain: '<Root>/Kc2'
   *  Sum: '<Root>/Sum2'
   */
  rtb_Gain3 = (testinside_P.Kc2_Gain * rtb_Gain3 + testinside_B.Kc1) *
    testinside_P.Gain3_Gain;

  /* Saturate: '<Root>/Saturation' */
  if (rtb_Gain3 >= testinside_P.Saturation_UpperSat) {
    testinside_B.Saturation = testinside_P.Saturation_UpperSat;
  } else if (rtb_Gain3 <= testinside_P.Saturation_LowerSat) {
    testinside_B.Saturation = testinside_P.Saturation_LowerSat;
  } else {
    testinside_B.Saturation = rtb_Gain3;
  }

  /* End of Saturate: '<Root>/Saturation' */
  if (rtmIsMajorTimeStep(testinside_M)) {
    /* S-Function (hil_write_block): '<Root>/HIL Write' */

    /* S-Function Block: testinside/HIL Write (hil_write_block) */
    {
      t_error result;
      result = hil_write(testinside_DWork.HILInitialize_Card,
                         &testinside_P.HILWrite_AnalogChannels, 1U,
                         NULL, 0U,
                         NULL, 0U,
                         NULL, 0U,
                         &testinside_B.Saturation,
                         NULL,
                         NULL,
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
      }
    }
  }

  if (rtmIsMajorTimeStep(testinside_M)) {
    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, testinside_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(testinside_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((testinside_M->Timing.clockTick1+
                            testinside_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(testinside_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(testinside_M)!=-1) &&
          !((rtmGetTFinal(testinside_M)-(((testinside_M->Timing.clockTick1+
               testinside_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((testinside_M->Timing.clockTick1+testinside_M->Timing.clockTickH1*
               4294967296.0)) * 0.002) * (DBL_EPSILON))) {
        rtmSetErrorStatus(testinside_M, "Simulation finished");
      }

      if (rtmGetStopRequested(testinside_M)) {
        rtmSetErrorStatus(testinside_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&testinside_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++testinside_M->Timing.clockTick0)) {
      ++testinside_M->Timing.clockTickH0;
    }

    testinside_M->Timing.t[0] = rtsiGetSolverStopTime(&testinside_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      testinside_M->Timing.clockTick1++;
      if (!testinside_M->Timing.clockTick1) {
        testinside_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void testinside_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn3' */
  {
    ((StateDerivatives_testinside *) testinside_M->ModelData.derivs)
      ->TransferFcn3_CSTATE = testinside_B.Gain2;
    ((StateDerivatives_testinside *) testinside_M->ModelData.derivs)
      ->TransferFcn3_CSTATE += (testinside_P.TransferFcn3_A)*
      testinside_X.TransferFcn3_CSTATE;
  }
}

/* Model initialize function */
void testinside_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)testinside_M, 0,
                sizeof(RT_MODEL_testinside));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&testinside_M->solverInfo,
                          &testinside_M->Timing.simTimeStep);
    rtsiSetTPtr(&testinside_M->solverInfo, &rtmGetTPtr(testinside_M));
    rtsiSetStepSizePtr(&testinside_M->solverInfo,
                       &testinside_M->Timing.stepSize0);
    rtsiSetdXPtr(&testinside_M->solverInfo, &testinside_M->ModelData.derivs);
    rtsiSetContStatesPtr(&testinside_M->solverInfo,
                         &testinside_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&testinside_M->solverInfo,
      &testinside_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&testinside_M->solverInfo, (&rtmGetErrorStatus
      (testinside_M)));
    rtsiSetRTModelPtr(&testinside_M->solverInfo, testinside_M);
  }

  rtsiSetSimTimeStep(&testinside_M->solverInfo, MAJOR_TIME_STEP);
  testinside_M->ModelData.intgData.f[0] = testinside_M->ModelData.odeF[0];
  testinside_M->ModelData.contStates = ((real_T *) &testinside_X);
  rtsiSetSolverData(&testinside_M->solverInfo, (void *)
                    &testinside_M->ModelData.intgData);
  rtsiSetSolverName(&testinside_M->solverInfo,"ode1");
  rtmSetTPtr(testinside_M, &testinside_M->Timing.tArray[0]);
  rtmSetTFinal(testinside_M, -1);
  testinside_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  testinside_M->Sizes.checksums[0] = (3071681570U);
  testinside_M->Sizes.checksums[1] = (569296671U);
  testinside_M->Sizes.checksums[2] = (1750000054U);
  testinside_M->Sizes.checksums[3] = (461422629U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    testinside_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(testinside_M->extModeInfo,
      &testinside_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(testinside_M->extModeInfo, testinside_M->Sizes.checksums);
    rteiSetTPtr(testinside_M->extModeInfo, rtmGetTPtr(testinside_M));
  }

  /* block I/O */
  (void) memset(((void *) &testinside_B), 0,
                sizeof(BlockIO_testinside));

  /* states (continuous) */
  {
    (void) memset((void *)&testinside_X, 0,
                  sizeof(ContinuousStates_testinside));
  }

  /* states (dwork) */
  (void) memset((void *)&testinside_DWork, 0,
                sizeof(D_Work_testinside));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    testinside_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: testinside/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q8_usb", "0", &testinside_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(testinside_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options(testinside_DWork.HILInitialize_Card,
      "update_rate=normal;decimation=1", 32);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(testinside_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(testinside_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(testinside_M, _rt_error_message);
      return;
    }

    if ((testinside_P.HILInitialize_AIPStart && !is_switching) ||
        (testinside_P.HILInitialize_AIPEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_AIMinimums = &testinside_DWork.HILInitialize_AIMinimums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AIMinimums[i1] = testinside_P.HILInitialize_AILow;
        }
      }

      {
        int_T i1;
        real_T *dw_AIMaximums = &testinside_DWork.HILInitialize_AIMaximums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AIMaximums[i1] = testinside_P.HILInitialize_AIHigh;
        }
      }

      result = hil_set_analog_input_ranges(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_AIChannels, 8U,
        &testinside_DWork.HILInitialize_AIMinimums[0],
        &testinside_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if ((testinside_P.HILInitialize_AOPStart && !is_switching) ||
        (testinside_P.HILInitialize_AOPEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_AOMinimums = &testinside_DWork.HILInitialize_AOMinimums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOMinimums[i1] = testinside_P.HILInitialize_AOLow;
        }
      }

      {
        int_T i1;
        real_T *dw_AOMaximums = &testinside_DWork.HILInitialize_AOMaximums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOMaximums[i1] = testinside_P.HILInitialize_AOHigh;
        }
      }

      result = hil_set_analog_output_ranges(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_AOChannels, 8U,
        &testinside_DWork.HILInitialize_AOMinimums[0],
        &testinside_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if ((testinside_P.HILInitialize_AOStart && !is_switching) ||
        (testinside_P.HILInitialize_AOEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_AOVoltages = &testinside_DWork.HILInitialize_AOVoltages[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOVoltages[i1] = testinside_P.HILInitialize_AOInitial;
        }
      }

      result = hil_write_analog(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_AOChannels, 8U,
        &testinside_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if (testinside_P.HILInitialize_AOReset) {
      {
        int_T i1;
        real_T *dw_AOVoltages = &testinside_DWork.HILInitialize_AOVoltages[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOVoltages[i1] = testinside_P.HILInitialize_AOWatchdog;
        }
      }

      result = hil_watchdog_set_analog_expiration_state
        (testinside_DWork.HILInitialize_Card,
         testinside_P.HILInitialize_AOChannels, 8U,
         &testinside_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if ((testinside_P.HILInitialize_EIPStart && !is_switching) ||
        (testinside_P.HILInitialize_EIPEnter && is_switching)) {
      {
        int_T i1;
        int32_T *dw_QuadratureModes =
          &testinside_DWork.HILInitialize_QuadratureModes[0];
        for (i1=0; i1 < 8; i1++) {
          dw_QuadratureModes[i1] = testinside_P.HILInitialize_EIQuadrature;
        }
      }

      result = hil_set_encoder_quadrature_mode
        (testinside_DWork.HILInitialize_Card,
         testinside_P.HILInitialize_EIChannels, 8U, (t_encoder_quadrature_mode *)
         &testinside_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if ((testinside_P.HILInitialize_EIStart && !is_switching) ||
        (testinside_P.HILInitialize_EIEnter && is_switching)) {
      {
        int_T i1;
        int32_T *dw_InitialEICounts =
          &testinside_DWork.HILInitialize_InitialEICounts[0];
        for (i1=0; i1 < 8; i1++) {
          dw_InitialEICounts[i1] = testinside_P.HILInitialize_EIInitial;
        }
      }

      result = hil_set_encoder_counts(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_EIChannels, 8U,
        &testinside_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if ((testinside_P.HILInitialize_POPStart && !is_switching) ||
        (testinside_P.HILInitialize_POPEnter && is_switching)) {
      uint32_T num_duty_cycle_modes = 0;
      uint32_T num_frequency_modes = 0;

      {
        int_T i1;
        int32_T *dw_POModeValues = &testinside_DWork.HILInitialize_POModeValues
          [0];
        for (i1=0; i1 < 8; i1++) {
          dw_POModeValues[i1] = testinside_P.HILInitialize_POModes;
        }
      }

      result = hil_set_pwm_mode(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_POChannels, 8U, (t_pwm_mode *)
        &testinside_DWork.HILInitialize_POModeValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }

      {
        int_T i1;
        const uint32_T *p_HILInitialize_POChannels =
          testinside_P.HILInitialize_POChannels;
        int32_T *dw_POModeValues = &testinside_DWork.HILInitialize_POModeValues
          [0];
        for (i1=0; i1 < 8; i1++) {
          if (dw_POModeValues[i1] == PWM_DUTY_CYCLE_MODE || dw_POModeValues[i1] ==
              PWM_ONE_SHOT_MODE || dw_POModeValues[i1] == PWM_TIME_MODE) {
            testinside_DWork.HILInitialize_POSortedChans[num_duty_cycle_modes] =
              (p_HILInitialize_POChannels[i1]);
            testinside_DWork.HILInitialize_POSortedFreqs[num_duty_cycle_modes] =
              testinside_P.HILInitialize_POFrequency;
            num_duty_cycle_modes++;
          } else {
            testinside_DWork.HILInitialize_POSortedChans[7U -
              num_frequency_modes] = (p_HILInitialize_POChannels[i1]);
            testinside_DWork.HILInitialize_POSortedFreqs[7U -
              num_frequency_modes] = testinside_P.HILInitialize_POFrequency;
            num_frequency_modes++;
          }
        }
      }

      if (num_duty_cycle_modes > 0) {
        result = hil_set_pwm_frequency(testinside_DWork.HILInitialize_Card,
          &testinside_DWork.HILInitialize_POSortedChans[0], num_duty_cycle_modes,
          &testinside_DWork.HILInitialize_POSortedFreqs[0]);
        if (result < 0) {
          msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
            (_rt_error_message));
          rtmSetErrorStatus(testinside_M, _rt_error_message);
          return;
        }
      }

      if (num_frequency_modes > 0) {
        result = hil_set_pwm_duty_cycle(testinside_DWork.HILInitialize_Card,
          &testinside_DWork.HILInitialize_POSortedChans[num_duty_cycle_modes],
          num_frequency_modes,
          &testinside_DWork.HILInitialize_POSortedFreqs[num_duty_cycle_modes]);
        if (result < 0) {
          msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
            (_rt_error_message));
          rtmSetErrorStatus(testinside_M, _rt_error_message);
          return;
        }
      }

      {
        int_T i1;
        int32_T *dw_POModeValues = &testinside_DWork.HILInitialize_POModeValues
          [0];
        for (i1=0; i1 < 8; i1++) {
          dw_POModeValues[i1] = testinside_P.HILInitialize_POConfiguration;
        }
      }

      {
        int_T i1;
        int32_T *dw_POAlignValues =
          &testinside_DWork.HILInitialize_POAlignValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POAlignValues[i1] = testinside_P.HILInitialize_POAlignment;
        }
      }

      {
        int_T i1;
        int32_T *dw_POPolarityVals =
          &testinside_DWork.HILInitialize_POPolarityVals[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POPolarityVals[i1] = testinside_P.HILInitialize_POPolarity;
        }
      }

      result = hil_set_pwm_configuration(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_POChannels, 8U,
        (t_pwm_configuration *) &testinside_DWork.HILInitialize_POModeValues[0],
        (t_pwm_alignment *) &testinside_DWork.HILInitialize_POAlignValues[0],
        (t_pwm_polarity *) &testinside_DWork.HILInitialize_POPolarityVals[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }

      {
        int_T i1;
        real_T *dw_POSortedFreqs =
          &testinside_DWork.HILInitialize_POSortedFreqs[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POSortedFreqs[i1] = testinside_P.HILInitialize_POLeading;
        }
      }

      {
        int_T i1;
        real_T *dw_POValues = &testinside_DWork.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = testinside_P.HILInitialize_POTrailing;
        }
      }

      result = hil_set_pwm_deadband(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_POChannels, 8U,
        &testinside_DWork.HILInitialize_POSortedFreqs[0],
        &testinside_DWork.HILInitialize_POValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if ((testinside_P.HILInitialize_POStart && !is_switching) ||
        (testinside_P.HILInitialize_POEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_POValues = &testinside_DWork.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = testinside_P.HILInitialize_POInitial;
        }
      }

      result = hil_write_pwm(testinside_DWork.HILInitialize_Card,
        testinside_P.HILInitialize_POChannels, 8U,
        &testinside_DWork.HILInitialize_POValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }

    if (testinside_P.HILInitialize_POReset) {
      {
        int_T i1;
        real_T *dw_POValues = &testinside_DWork.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = testinside_P.HILInitialize_POWatchdog;
        }
      }

      result = hil_watchdog_set_pwm_expiration_state
        (testinside_DWork.HILInitialize_Card,
         testinside_P.HILInitialize_POChannels, 8U,
         &testinside_DWork.HILInitialize_POValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(testinside_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

  /* S-Function Block: testinside/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader(testinside_DWork.HILInitialize_Card,
      testinside_P.HILReadAnalogTimebase_SamplesIn,
      &testinside_P.HILReadAnalogTimebase_Channels, 1,
      &testinside_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(testinside_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn3' */
  testinside_X.TransferFcn3_CSTATE = 0.0;
}

/* Model terminate function */
void testinside_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: testinside/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    t_uint32 num_final_pwm_outputs = 0;
    hil_task_stop_all(testinside_DWork.HILInitialize_Card);
    hil_monitor_stop_all(testinside_DWork.HILInitialize_Card);
    is_switching = false;
    if ((testinside_P.HILInitialize_AOTerminate && !is_switching) ||
        (testinside_P.HILInitialize_AOExit && is_switching)) {
      {
        int_T i1;
        real_T *dw_AOVoltages = &testinside_DWork.HILInitialize_AOVoltages[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOVoltages[i1] = testinside_P.HILInitialize_AOFinal;
        }
      }

      num_final_analog_outputs = 8U;
    }

    if ((testinside_P.HILInitialize_POTerminate && !is_switching) ||
        (testinside_P.HILInitialize_POExit && is_switching)) {
      {
        int_T i1;
        real_T *dw_POValues = &testinside_DWork.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = testinside_P.HILInitialize_POFinal;
        }
      }

      num_final_pwm_outputs = 8U;
    }

    if (0
        || num_final_analog_outputs > 0
        || num_final_pwm_outputs > 0
        ) {
      /* Attempt to write the final outputs atomically (due to firmware issue in old Q2-USB). Otherwise write channels individually */
      result = hil_write(testinside_DWork.HILInitialize_Card
                         , testinside_P.HILInitialize_AOChannels,
                         num_final_analog_outputs
                         , testinside_P.HILInitialize_POChannels,
                         num_final_pwm_outputs
                         , NULL, 0
                         , NULL, 0
                         , &testinside_DWork.HILInitialize_AOVoltages[0]
                         , &testinside_DWork.HILInitialize_POValues[0]
                         , (t_boolean *) NULL
                         , NULL
                         );
      if (result == -QERR_HIL_WRITE_NOT_SUPPORTED) {
        t_error local_result;
        result = 0;

        /* The hil_write operation is not supported by this card. Write final outputs for each channel type */
        if (num_final_analog_outputs > 0) {
          local_result = hil_write_analog(testinside_DWork.HILInitialize_Card,
            testinside_P.HILInitialize_AOChannels, num_final_analog_outputs,
            &testinside_DWork.HILInitialize_AOVoltages[0]);
          if (local_result < 0) {
            result = local_result;
          }
        }

        if (num_final_pwm_outputs > 0) {
          local_result = hil_write_pwm(testinside_DWork.HILInitialize_Card,
            testinside_P.HILInitialize_POChannels, num_final_pwm_outputs,
            &testinside_DWork.HILInitialize_POValues[0]);
          if (local_result < 0) {
            result = local_result;
          }
        }

        if (result < 0) {
          msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
            (_rt_error_message));
          rtmSetErrorStatus(testinside_M, _rt_error_message);
        }
      }
    }

    hil_task_delete_all(testinside_DWork.HILInitialize_Card);
    hil_monitor_delete_all(testinside_DWork.HILInitialize_Card);
    hil_close(testinside_DWork.HILInitialize_Card);
    testinside_DWork.HILInitialize_Card = NULL;
  }
}
