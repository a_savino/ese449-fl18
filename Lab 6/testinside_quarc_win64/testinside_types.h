/*
 * testinside_types.h
 *
 * Code generation for model "testinside".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Nov 19 16:12:08 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_testinside_types_h_
#define RTW_HEADER_testinside_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_testinside_ Parameters_testinside;

/* Forward declaration for rtModel */
typedef struct tag_RTM_testinside RT_MODEL_testinside;

#endif                                 /* RTW_HEADER_testinside_types_h_ */
