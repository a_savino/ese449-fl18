/*
 * bb_validation.c
 *
 * Code generation for model "bb_validation".
 *
 * Model version              : 1.25
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Wed Nov 28 16:12:46 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "bb_validation.h"
#include "bb_validation_private.h"
#include "bb_validation_dt.h"

/* Block signals (auto storage) */
BlockIO_bb_validation bb_validation_B;

/* Continuous states */
ContinuousStates_bb_validation bb_validation_X;

/* Block states (auto storage) */
D_Work_bb_validation bb_validation_DWork;

/* Real-time model */
RT_MODEL_bb_validation bb_validation_M_;
RT_MODEL_bb_validation *const bb_validation_M = &bb_validation_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 3;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  bb_validation_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void bb_validation_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadEncoder1;
  real_T rtb_DirectionConversion;
  real_T rtb_Z;
  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* set solver stop time */
    if (!(bb_validation_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&bb_validation_M->solverInfo,
                            ((bb_validation_M->Timing.clockTickH0 + 1) *
        bb_validation_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&bb_validation_M->solverInfo,
                            ((bb_validation_M->Timing.clockTick0 + 1) *
        bb_validation_M->Timing.stepSize0 + bb_validation_M->Timing.clockTickH0 *
        bb_validation_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(bb_validation_M)) {
    bb_validation_M->Timing.t[0] = rtsiGetT(&bb_validation_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

    /* S-Function Block: bb_validation/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
    {
      t_error result;
      result = hil_task_read_analog
        (bb_validation_DWork.HILReadAnalogTimebase_Task, 1,
         &bb_validation_DWork.HILReadAnalogTimebase_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
      }

      rtb_HILReadEncoder1 = bb_validation_DWork.HILReadAnalogTimebase_Buffer;
    }
  }

  /* SignalGenerator: '<Root>/Step ' */
  rtb_Z = bb_validation_P.Step_Frequency * bb_validation_M->Timing.t[0];
  if (rtb_Z - floor(rtb_Z) >= 0.5) {
    rtb_DirectionConversion = bb_validation_P.Step_Amplitude;
  } else {
    rtb_DirectionConversion = -bb_validation_P.Step_Amplitude;
  }

  /* End of SignalGenerator: '<Root>/Step ' */

  /* Gain: '<Root>/Gain3' */
  bb_validation_B.Gain3 = bb_validation_P.Gain3_Gain * rtb_DirectionConversion;
  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* Sum: '<Root>/Sum6' incorporates:
     *  Gain: '<Root>/Ball Beam Calibration (m//V)'
     */
    bb_validation_B.Sum6 = bb_validation_P.BallBeamCalibrationmV_Gain *
      rtb_HILReadEncoder1 + 0.0;
  }

  /* Gain: '<Root>/Z' incorporates:
   *  Sum: '<Root>/Sum5'
   */
  rtb_Z = (bb_validation_B.Gain3 - bb_validation_B.Sum6) *
    bb_validation_P.Z_Gain;
  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* Gain: '<Root>/Gain1' */
    bb_validation_B.Gain1 = bb_validation_P.Gain1_Gain * bb_validation_B.Sum6;
  }

  /* TransferFcn: '<Root>/Transfer Fcn1' */
  rtb_DirectionConversion = bb_validation_P.TransferFcn1_D*bb_validation_B.Gain1;
  rtb_DirectionConversion += bb_validation_P.TransferFcn1_C*
    bb_validation_X.TransferFcn1_CSTATE;

  /* Gain: '<Root>/Kc ' incorporates:
   *  Sum: '<Root>/Sum1'
   */
  rtb_DirectionConversion = (rtb_Z + rtb_DirectionConversion) *
    bb_validation_P.Kc_Gain;

  /* ManualSwitch: '<Root>/Manual Switch' incorporates:
   *  Constant: '<Root>/Constant '
   */
  if (bb_validation_P.ManualSwitch_CurrentSetting == 1) {
    bb_validation_B.ManualSwitch = bb_validation_P.Constant_Value;
  } else {
    bb_validation_B.ManualSwitch = rtb_DirectionConversion;
  }

  /* End of ManualSwitch: '<Root>/Manual Switch' */
  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* S-Function (hil_read_encoder_block): '<Root>/HIL Read Encoder1' */

    /* S-Function Block: bb_validation/HIL Read Encoder1 (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(bb_validation_DWork.HILInitialize_Card,
        &bb_validation_P.HILReadEncoder1_Channels, 1,
        &bb_validation_DWork.HILReadEncoder1_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
      } else {
        rtb_HILReadEncoder1 = bb_validation_DWork.HILReadEncoder1_Buffer;
      }
    }

    /* Gain: '<Root>/Gain' */
    bb_validation_B.Gain = bb_validation_P.Gain_Gain * rtb_HILReadEncoder1;

    /* Sum: '<Root>/Sum4' incorporates:
     *  Constant: '<Root>/Constant'
     *  Gain: '<S1>/Slider Gain'
     */
    bb_validation_B.Sum4 = bb_validation_P.SliderGain_Gain *
      bb_validation_P.Constant_Value_f + bb_validation_B.Gain;
  }

  /* Gain: '<Root>/Proportional Gain' incorporates:
   *  Sum: '<Root>/Sum3'
   */
  rtb_Z = (bb_validation_B.ManualSwitch - bb_validation_B.Sum4) *
    bb_validation_P.ProportionalGain_Gain;

  /* TransferFcn: '<Root>/High Pass filter ' */
  rtb_DirectionConversion = bb_validation_P.HighPassfilter_C[0]*
    bb_validation_X.HighPassfilter_CSTATE[0]
    + bb_validation_P.HighPassfilter_C[1]*bb_validation_X.HighPassfilter_CSTATE
    [1];

  /* Gain: '<Root>/Direction Conversion ' incorporates:
   *  Gain: '<Root>/Velocity Gain'
   *  Sum: '<Root>/Sum2'
   */
  rtb_DirectionConversion = (bb_validation_P.VelocityGain_Gain *
    rtb_DirectionConversion + rtb_Z) * bb_validation_P.DirectionConversion_Gain;

  /* Saturate: '<Root>/Saturation' */
  if (rtb_DirectionConversion >= bb_validation_P.Saturation_UpperSat) {
    bb_validation_B.Saturation = bb_validation_P.Saturation_UpperSat;
  } else if (rtb_DirectionConversion <= bb_validation_P.Saturation_LowerSat) {
    bb_validation_B.Saturation = bb_validation_P.Saturation_LowerSat;
  } else {
    bb_validation_B.Saturation = rtb_DirectionConversion;
  }

  /* End of Saturate: '<Root>/Saturation' */
  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* S-Function (hil_write_analog_block): '<Root>/HIL Write Analog' */

    /* S-Function Block: bb_validation/HIL Write Analog (hil_write_analog_block) */
    {
      t_error result;
      result = hil_write_analog(bb_validation_DWork.HILInitialize_Card,
        &bb_validation_P.HILWriteAnalog_Channels, 1, &bb_validation_B.Saturation);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
      }
    }

    /* Gain: '<Root>/Gain2' */
    bb_validation_B.Gain2 = bb_validation_P.Gain2_Gain * bb_validation_B.Sum4;
  }

  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, bb_validation_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(bb_validation_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((bb_validation_M->Timing.clockTick1+
                            bb_validation_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(bb_validation_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(bb_validation_M)!=-1) &&
          !((rtmGetTFinal(bb_validation_M)-(((bb_validation_M->Timing.clockTick1
               +bb_validation_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((bb_validation_M->Timing.clockTick1+
               bb_validation_M->Timing.clockTickH1* 4294967296.0)) * 0.002) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(bb_validation_M, "Simulation finished");
      }

      if (rtmGetStopRequested(bb_validation_M)) {
        rtmSetErrorStatus(bb_validation_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&bb_validation_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++bb_validation_M->Timing.clockTick0)) {
      ++bb_validation_M->Timing.clockTickH0;
    }

    bb_validation_M->Timing.t[0] = rtsiGetSolverStopTime
      (&bb_validation_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      bb_validation_M->Timing.clockTick1++;
      if (!bb_validation_M->Timing.clockTick1) {
        bb_validation_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void bb_validation_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn1' */
  {
    ((StateDerivatives_bb_validation *) bb_validation_M->ModelData.derivs)
      ->TransferFcn1_CSTATE = bb_validation_B.Gain1;
    ((StateDerivatives_bb_validation *) bb_validation_M->ModelData.derivs)
      ->TransferFcn1_CSTATE += (bb_validation_P.TransferFcn1_A)*
      bb_validation_X.TransferFcn1_CSTATE;
  }

  /* Derivatives for TransferFcn: '<Root>/High Pass filter ' */
  {
    ((StateDerivatives_bb_validation *) bb_validation_M->ModelData.derivs)
      ->HighPassfilter_CSTATE[0] = bb_validation_B.Gain2;
    ((StateDerivatives_bb_validation *) bb_validation_M->ModelData.derivs)
      ->HighPassfilter_CSTATE[0] += (bb_validation_P.HighPassfilter_A[0])*
      bb_validation_X.HighPassfilter_CSTATE[0]
      + (bb_validation_P.HighPassfilter_A[1])*
      bb_validation_X.HighPassfilter_CSTATE[1];
    ((StateDerivatives_bb_validation *) bb_validation_M->ModelData.derivs)
      ->HighPassfilter_CSTATE[1]= bb_validation_X.HighPassfilter_CSTATE[0];
  }
}

/* Model initialize function */
void bb_validation_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)bb_validation_M, 0,
                sizeof(RT_MODEL_bb_validation));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&bb_validation_M->solverInfo,
                          &bb_validation_M->Timing.simTimeStep);
    rtsiSetTPtr(&bb_validation_M->solverInfo, &rtmGetTPtr(bb_validation_M));
    rtsiSetStepSizePtr(&bb_validation_M->solverInfo,
                       &bb_validation_M->Timing.stepSize0);
    rtsiSetdXPtr(&bb_validation_M->solverInfo,
                 &bb_validation_M->ModelData.derivs);
    rtsiSetContStatesPtr(&bb_validation_M->solverInfo,
                         &bb_validation_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&bb_validation_M->solverInfo,
      &bb_validation_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&bb_validation_M->solverInfo, (&rtmGetErrorStatus
      (bb_validation_M)));
    rtsiSetRTModelPtr(&bb_validation_M->solverInfo, bb_validation_M);
  }

  rtsiSetSimTimeStep(&bb_validation_M->solverInfo, MAJOR_TIME_STEP);
  bb_validation_M->ModelData.intgData.f[0] = bb_validation_M->ModelData.odeF[0];
  bb_validation_M->ModelData.contStates = ((real_T *) &bb_validation_X);
  rtsiSetSolverData(&bb_validation_M->solverInfo, (void *)
                    &bb_validation_M->ModelData.intgData);
  rtsiSetSolverName(&bb_validation_M->solverInfo,"ode1");
  rtmSetTPtr(bb_validation_M, &bb_validation_M->Timing.tArray[0]);
  rtmSetTFinal(bb_validation_M, -1);
  bb_validation_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  bb_validation_M->Sizes.checksums[0] = (560436361U);
  bb_validation_M->Sizes.checksums[1] = (4155323148U);
  bb_validation_M->Sizes.checksums[2] = (2355963104U);
  bb_validation_M->Sizes.checksums[3] = (3371087540U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    bb_validation_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(bb_validation_M->extModeInfo,
      &bb_validation_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(bb_validation_M->extModeInfo,
                        bb_validation_M->Sizes.checksums);
    rteiSetTPtr(bb_validation_M->extModeInfo, rtmGetTPtr(bb_validation_M));
  }

  /* block I/O */
  (void) memset(((void *) &bb_validation_B), 0,
                sizeof(BlockIO_bb_validation));

  /* states (continuous) */
  {
    (void) memset((void *)&bb_validation_X, 0,
                  sizeof(ContinuousStates_bb_validation));
  }

  /* states (dwork) */
  (void) memset((void *)&bb_validation_DWork, 0,
                sizeof(D_Work_bb_validation));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    bb_validation_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: bb_validation/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &bb_validation_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(bb_validation_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options
      (bb_validation_DWork.HILInitialize_Card,
       "d0=digital;d1=digital;led=auto;update_rate=normal;decimation=1", 63);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(bb_validation_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(bb_validation_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(bb_validation_M, _rt_error_message);
      return;
    }

    if ((bb_validation_P.HILInitialize_AIPStart && !is_switching) ||
        (bb_validation_P.HILInitialize_AIPEnter && is_switching)) {
      bb_validation_DWork.HILInitialize_AIMinimums[0] =
        bb_validation_P.HILInitialize_AILow;
      bb_validation_DWork.HILInitialize_AIMinimums[1] =
        bb_validation_P.HILInitialize_AILow;
      bb_validation_DWork.HILInitialize_AIMaximums[0] =
        bb_validation_P.HILInitialize_AIHigh;
      bb_validation_DWork.HILInitialize_AIMaximums[1] =
        bb_validation_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges
        (bb_validation_DWork.HILInitialize_Card,
         bb_validation_P.HILInitialize_AIChannels, 2U,
         &bb_validation_DWork.HILInitialize_AIMinimums[0],
         &bb_validation_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
        return;
      }
    }

    if ((bb_validation_P.HILInitialize_AOPStart && !is_switching) ||
        (bb_validation_P.HILInitialize_AOPEnter && is_switching)) {
      bb_validation_DWork.HILInitialize_AOMinimums[0] =
        bb_validation_P.HILInitialize_AOLow;
      bb_validation_DWork.HILInitialize_AOMinimums[1] =
        bb_validation_P.HILInitialize_AOLow;
      bb_validation_DWork.HILInitialize_AOMaximums[0] =
        bb_validation_P.HILInitialize_AOHigh;
      bb_validation_DWork.HILInitialize_AOMaximums[1] =
        bb_validation_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges
        (bb_validation_DWork.HILInitialize_Card,
         bb_validation_P.HILInitialize_AOChannels, 2U,
         &bb_validation_DWork.HILInitialize_AOMinimums[0],
         &bb_validation_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
        return;
      }
    }

    if ((bb_validation_P.HILInitialize_AOStart && !is_switching) ||
        (bb_validation_P.HILInitialize_AOEnter && is_switching)) {
      bb_validation_DWork.HILInitialize_AOVoltages[0] =
        bb_validation_P.HILInitialize_AOInitial;
      bb_validation_DWork.HILInitialize_AOVoltages[1] =
        bb_validation_P.HILInitialize_AOInitial;
      result = hil_write_analog(bb_validation_DWork.HILInitialize_Card,
        bb_validation_P.HILInitialize_AOChannels, 2U,
        &bb_validation_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
        return;
      }
    }

    if (bb_validation_P.HILInitialize_AOReset) {
      bb_validation_DWork.HILInitialize_AOVoltages[0] =
        bb_validation_P.HILInitialize_AOWatchdog;
      bb_validation_DWork.HILInitialize_AOVoltages[1] =
        bb_validation_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (bb_validation_DWork.HILInitialize_Card,
         bb_validation_P.HILInitialize_AOChannels, 2U,
         &bb_validation_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
        return;
      }
    }

    if ((bb_validation_P.HILInitialize_EIPStart && !is_switching) ||
        (bb_validation_P.HILInitialize_EIPEnter && is_switching)) {
      bb_validation_DWork.HILInitialize_QuadratureModes[0] =
        bb_validation_P.HILInitialize_EIQuadrature;
      bb_validation_DWork.HILInitialize_QuadratureModes[1] =
        bb_validation_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (bb_validation_DWork.HILInitialize_Card,
         bb_validation_P.HILInitialize_EIChannels, 2U,
         (t_encoder_quadrature_mode *)
         &bb_validation_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
        return;
      }
    }

    if ((bb_validation_P.HILInitialize_EIStart && !is_switching) ||
        (bb_validation_P.HILInitialize_EIEnter && is_switching)) {
      bb_validation_DWork.HILInitialize_InitialEICounts[0] =
        bb_validation_P.HILInitialize_EIInitial;
      bb_validation_DWork.HILInitialize_InitialEICounts[1] =
        bb_validation_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(bb_validation_DWork.HILInitialize_Card,
        bb_validation_P.HILInitialize_EIChannels, 2U,
        &bb_validation_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_analog_timebase_block): '<Root>/HIL Read Analog Timebase' */

  /* S-Function Block: bb_validation/HIL Read Analog Timebase (hil_read_analog_timebase_block) */
  {
    t_error result;
    result = hil_task_create_analog_reader
      (bb_validation_DWork.HILInitialize_Card,
       bb_validation_P.HILReadAnalogTimebase_SamplesIn,
       &bb_validation_P.HILReadAnalogTimebase_Channels, 1,
       &bb_validation_DWork.HILReadAnalogTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(bb_validation_M, _rt_error_message);
    }
  }

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn1' */
  bb_validation_X.TransferFcn1_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/High Pass filter ' */
  bb_validation_X.HighPassfilter_CSTATE[0] = 0.0;
  bb_validation_X.HighPassfilter_CSTATE[1] = 0.0;
}

/* Model terminate function */
void bb_validation_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: bb_validation/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(bb_validation_DWork.HILInitialize_Card);
    hil_monitor_stop_all(bb_validation_DWork.HILInitialize_Card);
    is_switching = false;
    if ((bb_validation_P.HILInitialize_AOTerminate && !is_switching) ||
        (bb_validation_P.HILInitialize_AOExit && is_switching)) {
      bb_validation_DWork.HILInitialize_AOVoltages[0] =
        bb_validation_P.HILInitialize_AOFinal;
      bb_validation_DWork.HILInitialize_AOVoltages[1] =
        bb_validation_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(bb_validation_DWork.HILInitialize_Card,
        bb_validation_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &bb_validation_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(bb_validation_M, _rt_error_message);
      }
    }

    hil_task_delete_all(bb_validation_DWork.HILInitialize_Card);
    hil_monitor_delete_all(bb_validation_DWork.HILInitialize_Card);
    hil_close(bb_validation_DWork.HILInitialize_Card);
    bb_validation_DWork.HILInitialize_Card = NULL;
  }
}
