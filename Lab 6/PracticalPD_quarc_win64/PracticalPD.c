/*
 * PracticalPD.c
 *
 * Code generation for model "PracticalPD".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Nov 12 16:19:14 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "PracticalPD.h"
#include "PracticalPD_private.h"
#include "PracticalPD_dt.h"

/* Block signals (auto storage) */
BlockIO_PracticalPD PracticalPD_B;

/* Continuous states */
ContinuousStates_PracticalPD PracticalPD_X;

/* Block states (auto storage) */
D_Work_PracticalPD PracticalPD_DWork;

/* Real-time model */
RT_MODEL_PracticalPD PracticalPD_M_;
RT_MODEL_PracticalPD *const PracticalPD_M = &PracticalPD_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 5;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  PracticalPD_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void PracticalPD_step(void)
{
  /* local block i/o variables */
  real_T rtb_TransferFcn;
  real_T rtb_Derivative;
  real_T rtb_Z;
  if (rtmIsMajorTimeStep(PracticalPD_M)) {
    /* set solver stop time */
    if (!(PracticalPD_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&PracticalPD_M->solverInfo,
                            ((PracticalPD_M->Timing.clockTickH0 + 1) *
        PracticalPD_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&PracticalPD_M->solverInfo,
                            ((PracticalPD_M->Timing.clockTick0 + 1) *
        PracticalPD_M->Timing.stepSize0 + PracticalPD_M->Timing.clockTickH0 *
        PracticalPD_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(PracticalPD_M)) {
    PracticalPD_M->Timing.t[0] = rtsiGetT(&PracticalPD_M->solverInfo);
  }

  /* SignalGenerator: '<Root>/Signal Generator' */
  rtb_Z = PracticalPD_P.SignalGenerator_Frequency * PracticalPD_M->Timing.t[0];
  if (rtb_Z - floor(rtb_Z) >= 0.5) {
    PracticalPD_B.SignalGenerator = PracticalPD_P.SignalGenerator_Amplitude;
  } else {
    PracticalPD_B.SignalGenerator = -PracticalPD_P.SignalGenerator_Amplitude;
  }

  /* End of SignalGenerator: '<Root>/Signal Generator' */

  /* TransferFcn: '<Root>/Transfer Fcn' */
  PracticalPD_B.TransferFcn = PracticalPD_P.TransferFcn_C[0]*
    PracticalPD_X.TransferFcn_CSTATE[0]
    + PracticalPD_P.TransferFcn_C[1]*PracticalPD_X.TransferFcn_CSTATE[1];
  if (rtmIsMajorTimeStep(PracticalPD_M)) {
  }

  /* Gain: '<Root>/Z' incorporates:
   *  Sum: '<Root>/Sum'
   */
  rtb_Z = (PracticalPD_B.SignalGenerator - PracticalPD_B.TransferFcn) *
    PracticalPD_P.Z_Gain;

  /* Sum: '<Root>/Sum2' incorporates:
   *  Gain: '<Root>/bsp  '
   */
  PracticalPD_B.Sum2 = PracticalPD_P.bsp_Gain * PracticalPD_B.TransferFcn -
    PracticalPD_B.TransferFcn;

  /* TransferFcn: '<Root>/Transfer Fcn1' */
  rtb_Derivative = PracticalPD_P.TransferFcn1_D*PracticalPD_B.Sum2;
  rtb_Derivative += PracticalPD_P.TransferFcn1_C*
    PracticalPD_X.TransferFcn1_CSTATE;

  /* Gain: '<Root>/Kc' incorporates:
   *  Sum: '<Root>/Sum1'
   */
  PracticalPD_B.Kc = (rtb_Z + rtb_Derivative) * PracticalPD_P.Kc_Gain;

  /* TransferFcn: '<S1>/Transfer Fcn' */
  rtb_TransferFcn = PracticalPD_P.TransferFcn_C_d[0]*
    PracticalPD_X.TransferFcn_CSTATE_c[0]
    + PracticalPD_P.TransferFcn_C_d[1]*PracticalPD_X.TransferFcn_CSTATE_c[1];

  /* Derivative: '<S1>/Derivative' */
  {
    real_T t = PracticalPD_M->Timing.t[0];
    real_T timeStampA = PracticalPD_DWork.Derivative_RWORK.TimeStampA;
    real_T timeStampB = PracticalPD_DWork.Derivative_RWORK.TimeStampB;
    real_T *lastU = &PracticalPD_DWork.Derivative_RWORK.LastUAtTimeA;
    if (timeStampA >= t && timeStampB >= t) {
      rtb_Derivative = 0.0;
    } else {
      real_T deltaT;
      real_T lastTime = timeStampA;
      if (timeStampA < timeStampB) {
        if (timeStampB < t) {
          lastTime = timeStampB;
          lastU = &PracticalPD_DWork.Derivative_RWORK.LastUAtTimeB;
        }
      } else if (timeStampA >= t) {
        lastTime = timeStampB;
        lastU = &PracticalPD_DWork.Derivative_RWORK.LastUAtTimeB;
      }

      deltaT = t - lastTime;
      rtb_Derivative = (rtb_TransferFcn - *lastU++) / deltaT;
    }
  }

  /* Sum: '<S1>/Sum1' incorporates:
   *  Gain: '<S1>/Kp'
   *  Gain: '<S1>/Kv'
   *  Gain: '<S1>/Kv1'
   *  Sum: '<S1>/Sum'
   */
  PracticalPD_B.Sum1 = (0.0 - rtb_TransferFcn) * PracticalPD_P.Kp_Gain +
    PracticalPD_P.Kv1_Gain * rtb_Derivative * PracticalPD_P.Kv_Gain;
  if (rtmIsMajorTimeStep(PracticalPD_M)) {
    /* Update for Derivative: '<S1>/Derivative' */
    {
      real_T timeStampA = PracticalPD_DWork.Derivative_RWORK.TimeStampA;
      real_T timeStampB = PracticalPD_DWork.Derivative_RWORK.TimeStampB;
      real_T* lastTime = &PracticalPD_DWork.Derivative_RWORK.TimeStampA;
      real_T* lastU = &PracticalPD_DWork.Derivative_RWORK.LastUAtTimeA;
      if (timeStampA != rtInf) {
        if (timeStampB == rtInf) {
          lastTime = &PracticalPD_DWork.Derivative_RWORK.TimeStampB;
          lastU = &PracticalPD_DWork.Derivative_RWORK.LastUAtTimeB;
        } else if (timeStampA >= timeStampB) {
          lastTime = &PracticalPD_DWork.Derivative_RWORK.TimeStampB;
          lastU = &PracticalPD_DWork.Derivative_RWORK.LastUAtTimeB;
        }
      }

      *lastTime = PracticalPD_M->Timing.t[0];
      *lastU++ = rtb_TransferFcn;
    }

    /* External mode */
    rtExtModeUploadCheckTrigger(2);

    {                                  /* Sample time: [0.0s, 0.0s] */
      rtExtModeUpload(0, PracticalPD_M->Timing.t[0]);
    }

    if (rtmIsMajorTimeStep(PracticalPD_M)) {/* Sample time: [0.002s, 0.0s] */
      rtExtModeUpload(1, (((PracticalPD_M->Timing.clockTick1+
                            PracticalPD_M->Timing.clockTickH1* 4294967296.0)) *
                          0.002));
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(PracticalPD_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(PracticalPD_M)!=-1) &&
          !((rtmGetTFinal(PracticalPD_M)-(((PracticalPD_M->Timing.clockTick1+
               PracticalPD_M->Timing.clockTickH1* 4294967296.0)) * 0.002)) >
            (((PracticalPD_M->Timing.clockTick1+
               PracticalPD_M->Timing.clockTickH1* 4294967296.0)) * 0.002) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(PracticalPD_M, "Simulation finished");
      }

      if (rtmGetStopRequested(PracticalPD_M)) {
        rtmSetErrorStatus(PracticalPD_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&PracticalPD_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++PracticalPD_M->Timing.clockTick0)) {
      ++PracticalPD_M->Timing.clockTickH0;
    }

    PracticalPD_M->Timing.t[0] = rtsiGetSolverStopTime
      (&PracticalPD_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.002s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.002, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      PracticalPD_M->Timing.clockTick1++;
      if (!PracticalPD_M->Timing.clockTick1) {
        PracticalPD_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void PracticalPD_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn' */
  {
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn_CSTATE[0] = PracticalPD_B.Kc;
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn_CSTATE[0] += (PracticalPD_P.TransferFcn_A[0])*
      PracticalPD_X.TransferFcn_CSTATE[0]
      + (PracticalPD_P.TransferFcn_A[1])*PracticalPD_X.TransferFcn_CSTATE[1];
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn_CSTATE[1]= PracticalPD_X.TransferFcn_CSTATE[0];
  }

  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn1' */
  {
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn1_CSTATE = PracticalPD_B.Sum2;
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn1_CSTATE += (PracticalPD_P.TransferFcn1_A)*
      PracticalPD_X.TransferFcn1_CSTATE;
  }

  /* Derivatives for TransferFcn: '<S1>/Transfer Fcn' */
  {
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn_CSTATE_c[0] = PracticalPD_B.Sum1;
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn_CSTATE_c[0] += (PracticalPD_P.TransferFcn_A_b[0])*
      PracticalPD_X.TransferFcn_CSTATE_c[0]
      + (PracticalPD_P.TransferFcn_A_b[1])*PracticalPD_X.TransferFcn_CSTATE_c[1];
    ((StateDerivatives_PracticalPD *) PracticalPD_M->ModelData.derivs)
      ->TransferFcn_CSTATE_c[1]= PracticalPD_X.TransferFcn_CSTATE_c[0];
  }
}

/* Model initialize function */
void PracticalPD_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)PracticalPD_M, 0,
                sizeof(RT_MODEL_PracticalPD));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&PracticalPD_M->solverInfo,
                          &PracticalPD_M->Timing.simTimeStep);
    rtsiSetTPtr(&PracticalPD_M->solverInfo, &rtmGetTPtr(PracticalPD_M));
    rtsiSetStepSizePtr(&PracticalPD_M->solverInfo,
                       &PracticalPD_M->Timing.stepSize0);
    rtsiSetdXPtr(&PracticalPD_M->solverInfo, &PracticalPD_M->ModelData.derivs);
    rtsiSetContStatesPtr(&PracticalPD_M->solverInfo,
                         &PracticalPD_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&PracticalPD_M->solverInfo,
      &PracticalPD_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&PracticalPD_M->solverInfo, (&rtmGetErrorStatus
      (PracticalPD_M)));
    rtsiSetRTModelPtr(&PracticalPD_M->solverInfo, PracticalPD_M);
  }

  rtsiSetSimTimeStep(&PracticalPD_M->solverInfo, MAJOR_TIME_STEP);
  PracticalPD_M->ModelData.intgData.f[0] = PracticalPD_M->ModelData.odeF[0];
  PracticalPD_M->ModelData.contStates = ((real_T *) &PracticalPD_X);
  rtsiSetSolverData(&PracticalPD_M->solverInfo, (void *)
                    &PracticalPD_M->ModelData.intgData);
  rtsiSetSolverName(&PracticalPD_M->solverInfo,"ode1");
  rtmSetTPtr(PracticalPD_M, &PracticalPD_M->Timing.tArray[0]);
  rtmSetTFinal(PracticalPD_M, 20.0);
  PracticalPD_M->Timing.stepSize0 = 0.002;

  /* External mode info */
  PracticalPD_M->Sizes.checksums[0] = (1951937936U);
  PracticalPD_M->Sizes.checksums[1] = (3042832915U);
  PracticalPD_M->Sizes.checksums[2] = (230028904U);
  PracticalPD_M->Sizes.checksums[3] = (2670087241U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    PracticalPD_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(PracticalPD_M->extModeInfo,
      &PracticalPD_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(PracticalPD_M->extModeInfo,
                        PracticalPD_M->Sizes.checksums);
    rteiSetTPtr(PracticalPD_M->extModeInfo, rtmGetTPtr(PracticalPD_M));
  }

  /* block I/O */
  (void) memset(((void *) &PracticalPD_B), 0,
                sizeof(BlockIO_PracticalPD));

  /* states (continuous) */
  {
    (void) memset((void *)&PracticalPD_X, 0,
                  sizeof(ContinuousStates_PracticalPD));
  }

  /* states (dwork) */
  (void) memset((void *)&PracticalPD_DWork, 0,
                sizeof(D_Work_PracticalPD));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    PracticalPD_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn' */
  PracticalPD_X.TransferFcn_CSTATE[0] = 0.0;
  PracticalPD_X.TransferFcn_CSTATE[1] = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn1' */
  PracticalPD_X.TransferFcn1_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<S1>/Transfer Fcn' */
  PracticalPD_X.TransferFcn_CSTATE_c[0] = 0.0;
  PracticalPD_X.TransferFcn_CSTATE_c[1] = 0.0;

  /* InitializeConditions for Derivative: '<S1>/Derivative' */
  PracticalPD_DWork.Derivative_RWORK.TimeStampA = rtInf;
  PracticalPD_DWork.Derivative_RWORK.TimeStampB = rtInf;
}

/* Model terminate function */
void PracticalPD_terminate(void)
{
}
