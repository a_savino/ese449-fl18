/*
 * PracticalPD_data.c
 *
 * Code generation for model "PracticalPD".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Nov 12 16:19:14 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "PracticalPD.h"
#include "PracticalPD_private.h"

/* Block parameters (auto storage) */
Parameters_PracticalPD PracticalPD_P = {
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<Root>/Signal Generator'
                                        */
  0.05,                                /* Expression: .05
                                        * Referenced by: '<Root>/Signal Generator'
                                        */

  /*  Computed Parameter: TransferFcn_A
   * Referenced by: '<Root>/Transfer Fcn'
   */
  { -0.0, -0.0 },

  /*  Computed Parameter: TransferFcn_C
   * Referenced by: '<Root>/Transfer Fcn'
   */
  { 0.0, 0.419 },
  1.216,                               /* Expression: 1.216
                                        * Referenced by: '<Root>/Z'
                                        */
  1.0,                                 /* Expression: 1
                                        * Referenced by: '<Root>/bsp  '
                                        */
  -6.2831853071795862,                 /* Computed Parameter: TransferFcn1_A
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  -39.478417604357432,                 /* Computed Parameter: TransferFcn1_C
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  6.2831853071795862,                  /* Computed Parameter: TransferFcn1_D
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  3.37,                                /* Expression: 3.37
                                        * Referenced by: '<Root>/Kc'
                                        */

  /*  Computed Parameter: TransferFcn_A_b
   * Referenced by: '<S1>/Transfer Fcn'
   */
  { -33.333333333333336, -0.0 },

  /*  Computed Parameter: TransferFcn_C_d
   * Referenced by: '<S1>/Transfer Fcn'
   */
  { 0.0, 54.226666666666667 },
  8.6864,                              /* Expression: 8.6864
                                        * Referenced by: '<S1>/Kp'
                                        */
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<S1>/Kv1'
                                        */
  -0.0625                              /* Expression: -.0625
                                        * Referenced by: '<S1>/Kv'
                                        */
};
