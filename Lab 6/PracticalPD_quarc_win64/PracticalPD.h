/*
 * PracticalPD.h
 *
 * Code generation for model "PracticalPD".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.3 (R2012b) 20-Jul-2012
 * C source code generated on : Mon Nov 12 16:19:14 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_PracticalPD_h_
#define RTW_HEADER_PracticalPD_h_
#ifndef PracticalPD_COMMON_INCLUDES_
# define PracticalPD_COMMON_INCLUDES_
#include <float.h>
#include <math.h>
#include <string.h>
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "dt_info.h"
#include "ext_work.h"
#include "rt_nonfinite.h"
#endif                                 /* PracticalPD_COMMON_INCLUDES_ */

#include "PracticalPD_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T SignalGenerator;              /* '<Root>/Signal Generator' */
  real_T TransferFcn;                  /* '<Root>/Transfer Fcn' */
  real_T Sum2;                         /* '<Root>/Sum2' */
  real_T Kc;                           /* '<Root>/Kc' */
  real_T Sum1;                         /* '<S1>/Sum1' */
} BlockIO_PracticalPD;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  struct {
    real_T TimeStampA;
    real_T LastUAtTimeA;
    real_T TimeStampB;
    real_T LastUAtTimeB;
  } Derivative_RWORK;                  /* '<S1>/Derivative' */

  struct {
    void *LoggedData;
  } Scope_PWORK;                       /* '<Root>/Scope' */
} D_Work_PracticalPD;

/* Continuous states (auto storage) */
typedef struct {
  real_T TransferFcn_CSTATE[2];        /* '<Root>/Transfer Fcn' */
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
  real_T TransferFcn_CSTATE_c[2];      /* '<S1>/Transfer Fcn' */
} ContinuousStates_PracticalPD;

/* State derivatives (auto storage) */
typedef struct {
  real_T TransferFcn_CSTATE[2];        /* '<Root>/Transfer Fcn' */
  real_T TransferFcn1_CSTATE;          /* '<Root>/Transfer Fcn1' */
  real_T TransferFcn_CSTATE_c[2];      /* '<S1>/Transfer Fcn' */
} StateDerivatives_PracticalPD;

/* State disabled  */
typedef struct {
  boolean_T TransferFcn_CSTATE[2];     /* '<Root>/Transfer Fcn' */
  boolean_T TransferFcn1_CSTATE;       /* '<Root>/Transfer Fcn1' */
  boolean_T TransferFcn_CSTATE_c[2];   /* '<S1>/Transfer Fcn' */
} StateDisabled_PracticalPD;

#ifndef ODE1_INTG
#define ODE1_INTG

/* ODE1 Integration Data */
typedef struct {
  real_T *f[1];                        /* derivatives */
} ODE1_IntgData;

#endif

/* Parameters (auto storage) */
struct Parameters_PracticalPD_ {
  real_T SignalGenerator_Amplitude;    /* Expression: -1
                                        * Referenced by: '<Root>/Signal Generator'
                                        */
  real_T SignalGenerator_Frequency;    /* Expression: .05
                                        * Referenced by: '<Root>/Signal Generator'
                                        */
  real_T TransferFcn_A[2];             /* Computed Parameter: TransferFcn_A
                                        * Referenced by: '<Root>/Transfer Fcn'
                                        */
  real_T TransferFcn_C[2];             /* Computed Parameter: TransferFcn_C
                                        * Referenced by: '<Root>/Transfer Fcn'
                                        */
  real_T Z_Gain;                       /* Expression: 1.216
                                        * Referenced by: '<Root>/Z'
                                        */
  real_T bsp_Gain;                     /* Expression: 1
                                        * Referenced by: '<Root>/bsp  '
                                        */
  real_T TransferFcn1_A;               /* Computed Parameter: TransferFcn1_A
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T TransferFcn1_C;               /* Computed Parameter: TransferFcn1_C
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T TransferFcn1_D;               /* Computed Parameter: TransferFcn1_D
                                        * Referenced by: '<Root>/Transfer Fcn1'
                                        */
  real_T Kc_Gain;                      /* Expression: 3.37
                                        * Referenced by: '<Root>/Kc'
                                        */
  real_T TransferFcn_A_b[2];           /* Computed Parameter: TransferFcn_A_b
                                        * Referenced by: '<S1>/Transfer Fcn'
                                        */
  real_T TransferFcn_C_d[2];           /* Computed Parameter: TransferFcn_C_d
                                        * Referenced by: '<S1>/Transfer Fcn'
                                        */
  real_T Kp_Gain;                      /* Expression: 8.6864
                                        * Referenced by: '<S1>/Kp'
                                        */
  real_T Kv1_Gain;                     /* Expression: -1
                                        * Referenced by: '<S1>/Kv1'
                                        */
  real_T Kv_Gain;                      /* Expression: -.0625
                                        * Referenced by: '<S1>/Kv'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_PracticalPD {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;
  RTWSolverInfo solverInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    real_T *contStates;
    real_T *derivs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    real_T odeF[1][5];
    ODE1_IntgData intgData;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
    int_T numContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T tFinal;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block parameters (auto storage) */
extern Parameters_PracticalPD PracticalPD_P;

/* Block signals (auto storage) */
extern BlockIO_PracticalPD PracticalPD_B;

/* Continuous states (auto storage) */
extern ContinuousStates_PracticalPD PracticalPD_X;

/* Block states (auto storage) */
extern D_Work_PracticalPD PracticalPD_DWork;

/* Model entry point functions */
extern void PracticalPD_initialize(void);
extern void PracticalPD_step(void);
extern void PracticalPD_terminate(void);

/* Real-time Model object */
extern RT_MODEL_PracticalPD *const PracticalPD_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'PracticalPD'
 * '<S1>'   : 'PracticalPD/PV Controller '
 */
#endif                                 /* RTW_HEADER_PracticalPD_h_ */
